

//LIKE PAGE
function addLike(id){
	
    var likeform = $.find("#addLikeForm"+id);

    $.post( $(likeform).attr("action"), null, "json" )
        .done(function( json ) {
            if(json.success){
				var likenum = $(likeform).find("#likenum"+id);
				$(likenum).html(json.total);
            }else{
				alert(json.message);
            }
          });
}


//ADD COMMENT TO PAGE
function addComment(id){
	var cform = $.find("#addComForm"+id);	
	if ($("#comment").val() == ""){
		return false;
	}
	
    $.post( $(cform).attr("action"), $( cform ).serialize(), null, "json" )
        .done(function( json ) {
			if(json.success){
				var szurl = baseurl +"/cms/pages/" + id +"/comments?depth=2&likes=1&user=1";
				listcomments(id, szurl, 1);
				$( "#comment").val("");
			}
          });

}

//LIKE COMMENT
function likeComment(id){
	$.post( baseurl +"/cms/comments/" + id +"/like", null, "json" )
		.done(function( json ) {
			if(json.success){
				$("#likecomnum"+id).html(json.total);
			}else{
				alert(json.message);
			}
	});	
}

//ADD COMMENT TO COMMENT
function comment2comment(id,pageid,cform){
	
	if ($(cform).find("#comment").val() == ""){
		return false;
	}

    $.post( $(cform).attr("action"), $( cform ).serialize(), null, "json" )
        .done(function( json ) {
			if(json.success){
				var szurl = baseurl +"/cms/pages/" + pageid +"/comments?likes=1&user=1&depth=2";
				listcomments(pageid, szurl, 1);
				$(cform).find("#comment").val("");
			}
          });
	
	

}

//LIKE COMMENT 2 COMMENT
function likeComment2c(id){
	$.post( baseurl +"/cms/comments/" + id +"/like", null, "json" )
		.done(function( json ) {
			if(json.success){
				$("#likecom2cnum"+id).html(json.total);
			}else{
				alert(json.message);
			}
	});		
}

//LIST COMMENTS TO PAGE
function listcomments(id,szurl,level){
	$.getJSON( szurl, function( json ) { 
		$(".pagecomments").html("");
			
		$.each( json.data, function( i, comment ) {
			
			var antlikes = 0;
			
			if(!(jQuery.isEmptyObject(comment.likes))){
				antlikes = comment.likes.length;			
			}
			
			var child_antcomm = 0;
			if(!(jQuery.isEmptyObject(comment.children))){
				child_antcomm = comment.children.length;			
			}		
			
			$("#numpagecomments").html(json.data.length);
			
			var list = '<div class="col-md-12" id="comment'+comment.id+'">'+
					'<div class="">'+
  						'<div class="h3">'+
							'<div class="badge"><span class="glyphicon glyphicon-comment"></span>&nbsp;'+child_antcomm+'</div>&nbsp;'+
							comment.user.firstname + " " + comment.user.lastname + " &nbsp; "+
							comment.created_at+							
							'<button class="btn btn-danger btn-xs pull-right" type="button" onclick="likeComment('+comment.id+')">Like '+
								'<span class="glyphicon glyphicon-thumbs-up"></span>&nbsp;  '+
								'<span class="badge pull-right likenum" id="likecomnum'+comment.id+'" data-commentid="'+comment.id+'">'+
								antlikes +'</span>'+
							'</button>'+
							
						'</div>'+
  						'<div class=""><p>'+comment.comment.replace(/(?:\r\n|\r|\n)/g, '<br />')+'</p>'+
							'<form class="show-grid" role="form" method="post" action="'+baseurl+'/cms/comments/'+comment.id+'/comments" onsubmit="comment2comment('+comment.id+','+id+',this); return false;">'+
								'<div class="form-group col-md-9">'+
                    				'<textarea class="form-control" rows="2" name="comment" id="comment" placeholder="Kommentar"></textarea>'+
                				'</div>'+
								'<button type="submit" class="btn btn-sm">Send inn din kommentar</button>'+
							'</form>'+
						
							'<div class="commentcomments" id="listc2c'+comment.id+'">';
								$.each(comment.children,function(i, child){
									var antlikes = 0;
			
									if(!(jQuery.isEmptyObject(child.likes))){
										antlikes = child.likes.length;			
									}
									list = list + 
									'<div class="panel panel-default">'+
										'<div class="panel-heading">'+child.user.firstname + " " + child.user.lastname + " &nbsp; "+
											child.created_at+
											'<button class="btn btn-xs pull-right" type="button" onclick="likeComment2c('+child.id+')">Like '+
												'<span class="glyphicon glyphicon-thumbs-up"></span>&nbsp;  '+
												'<span class="badge pull-right likenum" id="likecom2cnum'+child.id+'" data-commentid="'+child.id+'">'+
												antlikes +'</span>'+
											'</button>'+
										'</div>'+
										'<div class="panel-body"><p>'+child.comment.replace(/(?:\r\n|\r|\n)/g, '<br />')+'</p>'+
									
										'</div>'+
									'</div>';
								});
							list = list +
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
				$(".pagecomments").append(list);
		});
	});
}




$(document).ready(function(){
	
//BX-SLIDERS
	$('.birthdaySlider').bxSlider({
		mode: 'fade',
		auto: true,
		controls: false,
		captions: true,
		randomStart: false,
		adaptiveHeight: false,
		pager: false,
		pause:6000
	});
	
$('.carousel').carousel();	
	
/*	
	$('.gallery').bxSlider({
		auto: false,
  		autoControls: false,
		randomStart: true,
		adaptiveHeight: true,
		pager: true, 
		pause: 5000
	});
*/	
	
//COUNT LIKES TO PAGES
    $(".likenum").each(function(){
        var span = $(this);
        var id = $(this).attr("data-pageid");
        var szurl = baseurl +"/cms/pages/" + id + "/likes";

        $.getJSON( szurl, function( json ) {  
		    if(!(jQuery.isEmptyObject(json.data))){  
            	$( span).html(json.data.length);
			}
       });
    });

//LIST COMMENTS TO PAGE
    $(".pagecomments").each(function(){
        var id = $(this).attr("data-pageid");
		var szurl = baseurl +"/cms/pages/" + id +"/comments?depth=2&likes=1&user=1";
		listcomments(id, szurl, 1);
	});

//NEW GROUP
	$("#newGroupBtn").on('click',function(){
		$("#newGroup").toggle('slow');
	});
	
//BUTTON ADD MEMBER 2 GROUP
	$("#add2group").on('click', function(){
		if($("#add2group").children('span').hasClass('glyphicon-star-empty')){
			var pageid = $("#add2group").attr('data-pageid');
			$.ajax({
				url: baseurl +"/ngi/member/add/" + pageid,
				type: 'POST',
				dataType:"json",
				success: function(json) {
					if(json.success){
						$("#add2group").html('<span class="glyphicon glyphicon-star"></span> Fjern fra mine grupper');
					}
				}
			});
			/*
			$.post( baseurl +"/ngi/member/add/" + pageid , null, "json" )
				.done(function( json ) {
					console.log(json.success);
					console.log($("#add2group").html());
					if(json.success){
						$("#add2group").html('<span class="glyphicon glyphicon-star"></span> Fjern fra mine grupper');
					}
			});	
			*/	
			
		}
	});
	
//BUTTON Remove MEMBER from GROUP
	$("#add2group").on('click', function(){
		if($("#add2group").children('span').hasClass('glyphicon-star')){
			var pageid = $("#add2group").attr('data-pageid');
			$.ajax({
				url: baseurl +"/ngi/member/remove/" + pageid,
				type: 'DELETE',
				dataType:"json",
				success: function(json) {
					if(json.success){
						$("#add2group").html('<span class="glyphicon glyphicon-star-empty"></span> Legg til i mine grupper');
					}
				}
			});
			
			
		}
	});
	
//Admin Remove MEMBER from GROUP
	$(".remove-member").on('click', function(){
			var userid = $(this).next("input[name='userid']").val();
			var pageid = $("#pageid").val();
			$.ajax({
				url: baseurl +"/ngi/member/remove/" + pageid + "/" + userid,
				type: 'DELETE',
				dataType:"json",
				success: function(json) {
					if(json.success){
						$("#member"+userid).remove();
					}
				}
			});

	});
	
//NEW PAGE
	$(".newPageBtn").on('click',function(){
		var parentid = $(this).parent().find(".list_parentid").val();
		$(this).parent().find("input[name='parent_id']").val(parentid);	
		$(this).next(".newPostForm").toggle('slow');

	});

//SMS
	$("#btn-sms").on("click",function(){
		$("#message").val("");
		$('select option').attr('selected', false);
	
		$("#msg").addClass("alert alert-success").append("SMS er sendt").show();
	});
	
	$("#sms-receivers").on("change",function(){
		var ant = $("#sms-receivers option:selected");
		$("#btn-sms .badge").text(ant.length);
		$("#btn-sms .badge").show();
	});
	
//DROPDOWN HURTIGLENKER
	$('.dropdown').mouseenter(function(){
   	 	if(!$('.navbar-toggle').is(':visible')) { // disable for mobile view
        	if(!$(this).hasClass('open')) { // Keeps it open when hover it again
            	$('.dropdown-toggle', this).trigger('click');
        	}
		}
	});	
	
//FILTER på nyheter
	$("#filter input[name='filter']").on('change', function(){
		var tags = "";
		$("#filter input[name='filter']:checked").each(function(){
			
			tags = tags  + $(this).val()+ ",";
		});
		
		
		if(tags){
			var szurl = $("#filter #pageurl").text() + "?tags=" + tags;
		}else{
			var szurl = $("#filter #pageurl").text() + "?tags=alle";
		}
		$( "#result" ).load( szurl+" #result" );
	});
	
//SUBSCRIPTION
	$('.subscription').on('click', function(){
		var tag = $(this);
		var id = $(tag).attr("id");
		if($(tag).hasClass("label-default")){
			//Subscribe
			$.ajax({
				url: baseurl +"/ngi/tag/" + id,
				type: 'POST',
				dataType:"json",
				success: function(json) {
					if(json.success){
						$(tag).removeClass("label-default").addClass("label-primary");
					}
				}
			});
			
		}else{
			//Unsubscribe
			$.ajax({
				url: baseurl +"/ngi/tag/" + id,
				type: 'DELETE',
				dataType:"json",
				success: function(json) {
					if(json.success){
						$(tag).removeClass("label-primary").addClass("label-default");
					}
				}
			});
		}
	});
	
//COMMENTS Les mer
	$(".read-full-comm").on("click",function(){
		
		var comment = $(this).parent();
		var h = $(comment).find('.content').innerHeight();
		$(comment).animate({
			height: h,		
  		}, "slow");
		$(this).remove();
	});
	
});
