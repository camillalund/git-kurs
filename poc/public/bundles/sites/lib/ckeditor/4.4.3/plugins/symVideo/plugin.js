CKEDITOR.plugins.add('symVideo', {

	icons: 'symVideoEmbed',

	init: function(editor) {
		editor.addCommand('symVideoEmbed', new CKEDITOR.dialogCommand('symVideoEmbedDialog'));
		editor.ui.addButton('SymVideoEmbed', {
			label: 'Embed Video',
			command: 'symVideoEmbed',
			toolbar: 'insert'
		});

		//-----------------------------------------------------------------------------------------------
		// Patch the DTD to allow meta tags as children to div's
		CKEDITOR.dtd['div']['meta'] = 1;

		//-----------------------------------------------------------------------------------------------
		// Parse Vippy video meta-data
		function ParseVippyMetaData(element) {
			var result = {
				thumbnail: null,
				width: null,
				height: null,
				valid: function() {
					return !!this.thumbnail && !!this.width && !!this.height;
				},
				apply: function(element) {
					var attributes;
					if (this.valid()) {
						attributes = {
							src: this.thumbnail,
							width: this.width,
							height: this.height,
							alt: 'Vippy Video'
						};
					}
					else {
						attributes = {
							src: $ymfoni.cms.asset('/admin/resources/images/symfoni/unknown_video.png'),
							alt: 'Vippy Video'
						};
					}
					if (element instanceof CKEDITOR.dom.element) {
						element.setAttributes(attributes);
					}
					else {
						for(var k in attributes) {
							element.attributes[k] = attributes[k];
						}
					}
				}
			};

			element.forEach(function(node) {
				if (node.name == 'meta') {
					switch (node.attributes.itemprop) {
						case 'thumbnailURL': 	result.thumbnail = node.attributes.content; break;
						case 'width': 			result.width = node.attributes.content; break;
						case 'height': 			result.height = node.attributes.content; break;
					}
				}
			}, CKEDITOR.NODE_ELEMENT, false);

			return result;
		}

		//-----------------------------------------------------------------------------------------------
		// Setup a writer that's used by the data filter...
		var writer = new CKEDITOR.htmlParser.basicWriter();

		//-----------------------------------------------------------------------------------------------
		// Add filter to convert TO source from placeholder
		editor.dataProcessor.htmlFilter.addRules({
			elements: {
				img: function(element) {
					if (element.hasClass('vippy-placeholder') && ('data-embedcode' in element.attributes)) {
						var fragment = CKEDITOR.htmlParser.fragment.fromHtml(Ext.util.Format.htmlDecode(element.attributes['data-embedcode'])),
							replace = fragment.children[0];
						if (!replace.hasClass('vippy-video')) replace.addClass('vippy-video');
						replace.parent = element.parent;
						replace.next = element.next;
						replace.previous = element.previous;
						//element.replaceWith(replace);
						return replace;
					}
					return element;
				}
			}
		});

		//-----------------------------------------------------------------------------------------------
		// Add filter to convert FROM source to placeholder
		editor.dataProcessor.dataFilter.addRules({
			elements: {
				div: function(element) {
					if (element.hasClass('vippy-video')) {
						var meta = ParseVippyMetaData(element);
						element.writeHtml(writer);
						var replace = new CKEDITOR.htmlParser.element('img', {
							'class': 'vippy-placeholder',
							'data-embedcode': writer.getHtml(true)
						});
						meta.apply(replace);
						replace.parent = element.parent;
						replace.next = element.next;
						replace.previous = element.previous;
						//element.replaceWith(replace);
						return replace;
					}
					return element;
				}
			}
		});


		//-----------------------------------------------------------------------------------------------
		// Dialog definitions
		// We're defining the dialog in-line here to share the funcitonality above..

		CKEDITOR.dialog.add('symVideoEmbedDialog', function(editor) {
			return {
				title: 'Embed Video',
				minWidth: 600,
				minHeight: 400,
				resizable: CKEDITOR.DIALOG_RESIZE_BOTH,

				contents: [
					{
						id: 'tab-basic',
						label: 'Basic Settings',
						elements: [ ]
					}
				],

				onLoad: function() {
					// Add resize listener...
					this.on('resize', this.definition.onResize, this);

					// Let's fix the styles to play along with Ext JS...
					var footer = this.parts.footer.getParent(),
						contents = footer.getAscendant('table');
					footer.addClass('cke_reset_all');
					footer.setStyles({
						margin: 0,
						border: 0,
						padding: 0
					});
					contents.setStyles({
						margin: 0,
						border: 0,
						padding: 0
					});
					this.getElement().removeClass('cke_reset_all');

					// Render the Ext JS view into the dialog...
					this._.symfoni = this._.symfoni || {};
					this._.symfoni.grid = Ext.create('Sym.view.video.StreamsGrid', {
						height: 500,
						renderTo: this.parts.contents.$
					});
				},

				onShow: function(evt) {
					this._.symfoni.grid.updateLayout();
				},

				onResize: function(evt) {
					this._.symfoni.grid.setSize(evt.data.width, evt.data.height);
				},

				onOk: function() {
					var selection = this._.symfoni.grid.getSelectionModel().getSelection();
					if (selection.length > 0) {
						var record = selection[0],
							embedCode = record.get('embed_code'),
							editor = this.getParentEditor();
						var insertVideoPlaceholder = function(embedCode) {
							var meta = ParseVippyMetaData(CKEDITOR.htmlParser.fragment.fromHtml(embedCode)),
								element = editor.document.createElement('img');
							element.addClass('vippy-placeholder');
							element.setAttribute('data-embedcode', embedCode); // CK automatically encodes the content..
							meta.apply(element);
							editor.insertElement(element);
						};

						if (embedCode) {
							insertVideoPlaceholder(embedCode);
						}
						else {
							record.reload().then(function(record) {
								insertVideoPlaceholder(record.get('embed_code'));
							});
						}
					}
				}
			};
		});
	}
});