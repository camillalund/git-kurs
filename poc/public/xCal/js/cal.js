$(document).ready(function(){

    // ++++++++++++++++ GLOBAL VARIABLES +++++++++++++
	
    var users_list_url = "../xCal/listinfo.php";
    var user_inf_url = "../xCal/getcalendarinfo.php?openagent";
    //var user_inf_url = "getcalendarinfo";	
	
	var sort_by = $("#cal_sort").val().toLowerCase();


    var nowDate = new Date();
    var weekEnd = new Date(new Date(nowDate).setDate(nowDate.getDate() + 6 - nowDate.getDay()));
    var weekStart = new Date(new Date(weekEnd).setDate(weekEnd.getDate() - 6));

    var usersList = [];

    var weekday=new Array(7);
    weekday[0]="Sunday";
    weekday[1]="Monday";
    weekday[2]="Tuesday";
    weekday[3]="Wednesday";
    weekday[4]="Thursday";
    weekday[5]="Friday";
    weekday[6]="Saturday";

    // ++++++++++++++++ DOCUMENT LOAD ++++++++++++++++

    //LoadUsers();

	LoadGroups();
	//LoadDepartments();
	//$('#calendar').fullCalendar( {height: 'auto', defaultView: 'basicWeek'} );

    // ++++++++++++++++ EVENT HANDLERS ++++++++++++++++

    // wait cursor for ajax requests
    $(document).ajaxStart(function () {
        $('body').addClass('wait');
    }).ajaxComplete(function () {
                $('body').removeClass('wait');			
				$("#calendar").show();
            });

    // filter for users
    $("#filter_text").keyup(function(){
		var sQuery = $(this).val();
        searchUser(sQuery);
    });

    // show user calendar form
    $('#selectable').on({
        click : UserItemClick
    }, 'li');

    $('#accordion').on({
        click : UserItemClickFullCal
    }, 'li');
	
    $('#filter-result').on({
        click : UserItemClickFullCal
    }, 'li');
	
    $('#department').on({
        click : function(){
			$("#cal_sort").val("Department");
			sort_by = "department";
			LoadGroups();
		}
	});
	
    $('#country').on({
        click : function(){
			$("#cal_sort").val("Country");
			sort_by = "country";
			LoadGroups();
		}
	});	
    $('#responsibility').on({
        click : function(){
			$("#cal_sort").val("Responsibility");
			sort_by = "responsibility";
			LoadGroups();
		}
	});		



    // user information table
    $('#dialog-selectable').on({
        click : function(){
            var start = $("#currentDateInverval").text().split(" - ")[0].replace('.', '').replace('.', '');
            var end = $("#currentDateInverval").text().split(" - ")[1].replace('.', '').replace('.', '');

            var elementId = this.id;

            $.ajax({
                type : 'GET',
                contentType: "application/json; charset=utf-8",
                //url : user_inf_url + "?openagent&FromDate=" + start.trim() + "&ToDate=" + end.trim() + "&Username=" + ($("#current_user_cn").attr("value")).trim(),
                url : user_inf_url,				
                dataType : 'json',
                headers : {Accept : "application/json", "Access-Control-Allow-Origin" : "*"},
                crossDomain : true,
                success : function(data) {
                    $.each(data.data, function (index, value) {
                        if (index == elementId) {
                            var subject = ("" + value[0]).split(',')[1];
                            var location = ("" + value[1]).split(',')[1];
                            var startdate = ("" + value[2]).split(',')[1];
                            var enddate = ("" + value[3]).split(',')[1];
                            var chair = ("" + value[4]).split(',')[1];
                            var required = ("" + value[5]).split(',')[1];
                            var optional = ("" + value[6]).split(',')[1];
                            var body = ("" + value[7]).split(',')[1];
                            var room = ("" + value[8]).split(',')[1];
                            var resources = ("" + value[9]).split(',')[1];

                            $("#dialog-details").dialog( "open" );
                            $("#dialog-details").dialog('option', 'title', subject);

                            $("#calendar_inf_table tr").remove();

                            $("#calendar_inf_table").append(
                                    "<tr><td class='smalltext'>Date: </td><td class='smalltext'>" + startdate + " - " + enddate + "</td></tr>"
                                            + "<tr><td class='smalltext'>Location: </td><td class='smalltext'>" + location + "</td></tr>"
                                            + "<tr><td class='smalltext'>Chair: </td><td class='smalltext'>" + chair + "</td></tr>"
                                            + "<tr><td class='smalltext'>Required: </td><td class='smalltext'>" + required + "</td></tr>"
                                            + "<tr><td class='smalltext'>Optional: </td><td class='smalltext'>" + optional + "</td></tr>"
                                            + "<tr><td class='smalltext'>Room: </td><td class='smalltext'>" + room + "</td></tr>"
                                            + "<tr><td class='smalltext'>Body: </td><td class='smalltext'>" + body + "</td></tr>"
                            );
                        }
                    })
                },
                error : function(data, textStatus, errorThrown) {
                    //console.log("error" + ' ' + JSON.stringify(data) + textStatus  + errorThrown);}
                    //console.log("error" + ' ' + textStatus  + errorThrown);
					}
            });
        }
    }, 'div');

    // switch user-group view
    $("#switch_to").click(function(){
        if ($("#switch_to").text() == 'Switch to group view') {
            $("#switch_to").text("Switch to user view");
            $("#selectable li").remove();
            $("#accordion div").remove();
            $("#accordion h3").remove();
            LoadGroups();
        } else {
            $("#switch_to").text("Switch to group view");
            $("#selectable li").remove();
            $("#accordion div").remove();
            $("#accordion h3").remove();
            LoadUsers();
        }
    });

    $(function() {
        $( "#accordion" ).accordion({
            collapsible: true,
            heightStyle: "content"
        });
    });

    // ++++++++++++++++ DIALOGUES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    $( "#dialog-form" ).dialog({
        autoOpen: false,
        height: 600,
        width: 700,
        modal: true
    });

    $( "#dialog-details" ).dialog({
        autoOpen: false,
        height: 300,
        width: 500,
        modal: true
    });

    // ++++++++++++++++ FUNCTIONS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	function UserItemClickFullCal(){
		
			//mobile nav
		    $('.row-offcanvas').toggleClass('active')
			 
			$( "#accordion ol > li" ).removeClass( "active" );
			$(this).addClass( "active" );
			$( "#filter-result ul > li" ).removeClass( "active" );
			$("#user_info").show();
			$("#meeting_info").hide();
			
			var headingUser = $(this).html();
			var selectedUser = $(this).attr("id");
			var user = {};
			
			$.each(usersList, function(index, value){
				if (selectedUser == value.id) {
					user = value;
				}
			});	
			
			$('#calendar').fullCalendar( 'destroy' );
			
			$('#calendar').fullCalendar({
				
				header: {
					left: 'prev,next today',
					center: 'title',
					timezone: 'Europe/Oslo',
					right: 'month,basicWeek,basicDay',
				},
				
				//defaultDate: '2014-12-1',			
				editable: false,
				eventLimit: true, // allow "more" link when too many events
				height: 'auto',
				eventColor: '#75787B',
				defaultView: 'basicWeek',
				columnFormat: 'ddd D',
				timeFormat: 'H(:mm)', // uppercase H for 24-hour clock
				//defaultView: 'agendaWeek',				
					
		
				events: function(start, end, timezone, callback) {
					
					var MyEndDate = new Date(end);
					var endDateString = '';
					MyEndDate.setDate(MyEndDate.getDate());
					var endMonth = (MyEndDate.getMonth()+1);
					var endDate = (MyEndDate.getDate());
					if (endMonth < 10) endMonth = '0' + endMonth;
					if (endDate < 10) endDate = '0' + endDate;	
					endDateString = MyEndDate.getFullYear() +''+ endMonth +''+ endDate;
				 
					var MyStartDate = new Date(start);
					var startDateString = '';
					MyStartDate.setDate(MyStartDate.getDate());
					var startMonth = (MyStartDate.getMonth()+1);
					var startDate = (MyStartDate.getDate());
					if (startMonth < 10) startMonth = '0' + startMonth;
					if (startDate < 10) startDate = '0' + startDate;	
					startDateString = MyStartDate.getFullYear() +''+ startMonth +''+ startDate;
					
					$.ajax({
						url: user_inf_url,
						dataType: 'json',
						"Access-Control-Allow-Origin" : "*",
						data: {
							// our hypothetical feed requires UNIX timestamps
							FromDate: startDateString,
							ToDate: endDateString,
							Username: user.id
						},
							
						success: function(doc) {
						var elements = [];
					
							$.each(doc.data, function (index, value) {
								var subject = ("" + value[0]).split(',')[1];
								var startdate = ("" + value[2]).split(',')[1];
								var enddate = ("" + value[3]).split(',')[1];
								var location = ("" + value[1]).split(',')[1];
								var chair = ("" + value[4]).split(',')[1];
								var required = ("" + value[5]).split(',')[1];
 								var optional = ("" + value[6]).split(',')[1];
                            	var body = ("" + value[7]).split(',')[1];
                            	var room = ("" + value[8]).split(',')[1];							
			
								var date = new Date(parseInt(startdate.split(' ')[0].split('.')[2]),
								parseInt(startdate.split(' ')[0].split('.')[1]) - 1,
								parseInt(startdate.split(' ')[0].split('.')[0]),
								parseInt(startdate.split(' ')[1].split(':')[0]),
								parseInt(startdate.split(' ')[1].split(':')[1]),
								parseInt(startdate.split(' ')[1].split(':')[2], 0));
			
								var obj = {
									"title": subject,
									"startdate":startdate,
									"enddate":enddate,
									"start": date,
									"id": index,
									"loc": location,
									"chair": chair,
									"required": required,
									"optional": optional,
									"body": body,
									"room": room
									
								};
			
								elements.push(obj);
										
							
							});
							
						callback(elements);		
						
						}
						
					});
					
				},				
				
				
				eventClick: function(calEvent, jsEvent, view) {
					
					$("#meeting_info").show();
            		$("#calendar_inf_table tr").remove();
					$("#meeting_info_heading").html(calEvent.title);
            		$("#calendar_inf_table").append(
                	 	"<tr><td class='smalltext'>Date: </td><td class='smalltext'>" + calEvent.startdate + "</td></tr>"
                	 	+ "<tr><td class='smalltext'>Location: </td><td class='smalltext'>" + calEvent.loc + "</td></tr>"
                	 	+ "<tr><td class='smalltext'>Chair: </td><td class='smalltext'>" + calEvent.chair + "</td></tr>"
                	 	+ "<tr><td class='smalltext'>Required: </td><td class='smalltext'>" + calEvent.required + "</td></tr>"
                	 	+ "<tr><td class='smalltext'>Optional: </td><td class='smalltext'>" + calEvent.optional + "</td></tr>"
                	 	+ "<tr><td class='smalltext'>Room: </td><td class='smalltext'>" + calEvent.room + "</td></tr>"
                	 	+ "<tr><td class='smalltext'>Body: </td><td class='smalltext'>" + calEvent.body + "</td></tr>"
            	 );					

				}
				
				
		
			});	
	
			$("#user-information-table tr").remove();	
			
			$("#user-information-heading").html(headingUser);
			
			// add if responsibilities
			if(user.responsibility == ""){
				var resp = "";
				}else{
				var resp = "<tr><td>Responsibility</td>" + "<td>" + user.responsibility + "</td>" + "<td></td></tr>";
			}
				
			$("#user-information-table").append(
					"<tr><td>Company</td>" + "<td>" + user.company + "</td>" + "<td></td></tr>" +
							"<tr><td>Country</td>" + "<td>" + user.country + "</td>" + "<td></td></tr>" +
							"<tr><td>City</td>" + "<td>" + user.city + "</td>" + "<td></td></tr>" +
							"<tr><td>Position</td>" + "<td>" + user.jobTitle + "</td>" + "<td></td></tr>" +
							"<tr><td>Department</td>" + "<td>" + user.department + "</td>" + "<td></td></tr>" +
							"<tr><td>Phone number</td>" + "<td>" + user.phone + "</td>" + "<td></td></tr>" +
							"<tr><td>Mail</td>" + "<td>" + "<a href='mailto:" + user.mail + "'>" + user.mail + "</a>" + "</td>" + "<td></td></tr>"
							+ resp
			);	
			
	}
	
	function searchUser(d){
        var text = d.toLowerCase();
		var tmp = "";
        $('ol li').each(function(){								 
            if($(this).text().toLowerCase().indexOf(text) != -1){
                tmp = tmp + $(this).clone().wrap('<li>').parent().html();			
			}
        });
		
		if(tmp.indexOf("li") == -1){
			tmp = "Ingen treff";
			}
		if(text == ""){
		$("#filter-result ul").html("");
		$("#filter-result").hide();
		}else{
		$("#filter-result ul").html(tmp);
		$("#filter-result").show();
		}
		
	}
		

    function UserItemClick() {
        var start = FormatDate(weekStart, "");
        var end = FormatDate(weekEnd, "");
        var user = {};
        var selectedUser = $(this).attr("id");
		
		$( "#accordion ol > li" ).removeClass( "active" );
		$(this).addClass( "active" );
		$( "#filter-result ul > li" ).removeClass( "active" );		

        $("#user-information-table tr").remove();

        $.each(usersList, function(index, value){
            if (selectedUser == value.id) {
                user = value;
            }
        });

        $("#user-information-table").append(
                "<tr><td>Company</td>" + "<td>" + user.company + "</td>" + "<td></td></tr>" +
                        "<tr><td>Country</td>" + "<td>" + user.country + "</td>" + "<td></td></tr>" +
                        "<tr><td>City</td>" + "<td>" + user.city + "</td>" + "<td></td></tr>" +
                        "<tr><td>Position</td>" + "<td>" + user.jobTitle + "</td>" + "<td></td></tr>" +
                        "<tr><td>Department</td>" + "<td>" + user.department + "</td>" + "<td></td></tr>" +
                        "<tr><td>Phone number</td>" + "<td>" + user.phone + "</td>" + "<td></td></tr>" +
                        "<tr><td>Mail</td>" + "<td>" + "<a href='mailto:" + user.mail + "'>" + user.mail + "</a>" + "</td>" + "<td></td></tr>"
        );


        $("#current_user_cn").attr("value", user.id);
        $("#dialog-form").dialog('option', 'title', user.fullName);
		$(".panel-heading").text(user.fullName);

        $.ajax({
            type : 'GET',
            contentType: "application/json; charset=utf-8",
            //url : user_inf_url + "?openagent&FromDate=" + start.trim() + "&ToDate=" + end.trim() + "&Username=" + user.id,
            url : user_inf_url + start.trim() + "&ToDate=" + end.trim() + "&Username=" + user.id,			
            dataType : 'json',
            headers : {Accept : "application/json", "Access-Control-Allow-Origin" : "*"},
            crossDomain : true,
            success : UserSucceedFunc,
            error : function(data, textStatus, errorThrown) {
                //console.log("error" + ' ' + JSON.stringify(data) + textStatus  + errorThrown);}
                //console.log("error" + ' ' + textStatus  + errorThrown);
				}
        });
    }

    function LoadUsers() {
        $.ajax({
            type : 'GET',
            contentType: "application/json; charset=utf-8",
            url : users_list_url,
            dataType : 'json',
            headers : {Accept : "application/json", "Access-Control-Allow-Origin" : "*"},
            crossDomain : true,
            success : SucceedFunc,
            error : function(data, textStatus, errorThrown) {
                //console.log("error" + ' ' + JSON.stringify(data) + textStatus  + errorThrown);}
                //console.log("error" + ' ' + textStatus  + errorThrown);
				}
        });
    }

    function SucceedFunc(data) {
        var list = $("#selectable");
        $.each(data.viewentry, function (index, value) {

            var jobTitle;
            try {
                jobTitle = value.entrydata[12].text[0];
            } catch (err) {
                // ignore
            }

            var userData = {
                "id": value.entrydata[0].text[0],
                "fullName": value.entrydata[1].text[0] + " " + value.entrydata[2].text[0] + " " + value.entrydata[3].text[0],
                "company": value.entrydata[8].text[0],
                "country": value.entrydata[9].text[0],
                "city": value.entrydata[7].text[0],
                "jobTitle": jobTitle,
                "department": value.entrydata[11].text[0],
                "phone": value.entrydata[6].text[0],
                "mail": value.entrydata[10].text[0]
            };

            usersList.push(userData);

            list.append("<li class='ui-widget-content' id='" + userData.id + "'>" + userData.fullName + "</li>");
        });
    }

    function UserSucceedFunc(data) {
		
        $("#dialog-selectable .ui-widget-content").remove();
        var elements = [];
		
        $.each(data.data, function (index, value) {
            var subject = ("" + value[0]).split(',')[1];
            var startdate = ("" + value[2]).split(',')[1];

            var date = new Date(parseInt(startdate.split(' ')[0].split('.')[2]),
                    parseInt(startdate.split(' ')[0].split('.')[1]) - 1,
                    parseInt(startdate.split(' ')[0].split('.')[0]),
                    parseInt(startdate.split(' ')[1].split(':')[0]),
                    parseInt(startdate.split(' ')[1].split(':')[1]),
                    parseInt(startdate.split(' ')[1].split(':')[2], 0));

            var obj = {
                "subject": subject,
                "date": date,
                "id": index
            };

            elements.push(obj);
        });

        elements.sort(function(b, a) {
            return b.date.getTime() - a.date.getTime();
        });

        var insertDay = -1;
        for (var i = 0; i < elements.length; i++) {
            if (insertDay != elements[i].date.getDay()) {
                $("#dialog-selectable").append("<h4 style='font-size: small' class='ui-widget-content'>"
                        + weekday[elements[i].date.getDay()] + " (" + FormatDate(elements[i].date, '.') + ")" + "</h4>");
            }

            insertDay = elements[i].date.getDay();

            $("#dialog-selectable").append("<div class='ui-widget-content' time='" + elements[i].date
                    + "' style='font-size: small' id='"
                    + elements[i].id + "'>" + FormatTime(elements[i].date, ":") + " " + elements[i].subject + "</div>");
        }

        var start = FormatDate(weekStart, ".");
        var end = FormatDate(weekEnd, ".");

        var prevStart = new Date(new Date(weekStart).setDate(weekStart.getDate() - 7));
        var prevEnd = new Date(new Date(weekEnd).setDate(weekEnd.getDate() - 7));
        var prevInterval = FormatDate(prevStart, ".") + " " + FormatDate(prevEnd, ".");

        var nextStart = new Date(new Date(weekStart).setDate(weekStart.getDate() + 7));
        var nextEnd = new Date(new Date(weekEnd).setDate(weekEnd.getDate() + 7));
        var nextInterval = FormatDate(nextStart, ".") + " " + FormatDate(nextEnd, ".");

        $('#buttonPrev').attr("name", prevInterval);
        $('#buttonNext').attr("name", nextInterval);
        $("#dialog-form").dialog( "open" );
        $("#currentDateInverval").text(" " + start + " - " + end + " ");
    }

    function UserSucceedFuncPrev(data) {
        $("#dialog-selectable .ui-widget-content").remove();
        var elements = [];

        $.each(data.data, function (index, value) {
            var subject = ("" + value[0]).split(',')[1];
            var startdate = ("" + value[2]).split(',')[1];

            var date = new Date(parseInt(startdate.split(' ')[0].split('.')[2]),
                    parseInt(startdate.split(' ')[0].split('.')[1]) - 1,
                    parseInt(startdate.split(' ')[0].split('.')[0]),
                    parseInt(startdate.split(' ')[1].split(':')[0]),
                    parseInt(startdate.split(' ')[1].split(':')[1]),
                    parseInt(startdate.split(' ')[1].split(':')[2], 0));

            var obj = {
                "subject": subject,
                "date": date,
                "id": index
            };

            elements.push(obj);
        });

        elements.sort(function(b, a) {
            return b.date.getTime() - a.date.getTime();
        });

        var insertDay = -1;
        for (var i = 0; i < elements.length; i++) {
            if (insertDay != elements[i].date.getDay()) {
                $("#dialog-selectable").append("<h4 style='font-size: small' class='ui-widget-content'>"
                        + weekday[elements[i].date.getDay()] + " (" + FormatDate(elements[i].date, '.') + ")" + "</h4>");
            }

            insertDay = elements[i].date.getDay();

            $("#dialog-selectable").append("<div class='ui-widget-content' time='" + elements[i].date
                    + "' style='font-size: small' id='"
                    + elements[i].id + "'>" + FormatTime(elements[i].date, ":") + " " + elements[i].subject + "</div>");
        }

        var startStrSplited = $("#buttonPrev").attr("name").split(" ")[0].split('.');
        var endStrSplited = $("#buttonPrev").attr("name").split(" ")[1].split('.');

        var start = new Date();
        start.setFullYear(parseInt(startStrSplited[0]));
        start.setMonth(parseInt(startStrSplited[1]) - 1);
        start.setDate(parseInt(startStrSplited[2]));

        var end = new Date();
        end.setFullYear(parseInt(endStrSplited[0]));
        end.setMonth(parseInt(endStrSplited[1]) - 1);
        end.setDate(parseInt(endStrSplited[2]));

        var prevStart = new Date(new Date(start).setDate(start.getDate() - 7));
        var prevEnd = new Date(new Date(end).setDate(end.getDate() - 7));
        var prevInterval = FormatDate(prevStart, ".") + " " + FormatDate(prevEnd, ".");

        var nextStart = new Date(new Date(start).setDate(start.getDate() + 7));
        var nextEnd = new Date(new Date(end).setDate(end.getDate() + 7));
        var nextInterval = FormatDate(nextStart, ".") + " " + FormatDate(nextEnd, ".");

        $('#buttonPrev').attr("name", prevInterval);
        $('#buttonNext').attr("name", nextInterval);

        $("#dialog-form").dialog( "open" );

        $("#currentDateInverval").text(" " + FormatDate(start, '.') + " - " + FormatDate(end, '.') + " ");
    }

    function UserSucceedFuncNext(data) {
        $("#dialog-selectable .ui-widget-content").remove();
        var elements = [];

        $.each(data.data, function (index, value) {
            var subject = ("" + value[0]).split(',')[1];
            var startdate = ("" + value[2]).split(',')[1];

            var date = new Date(parseInt(startdate.split(' ')[0].split('.')[2]),
                    parseInt(startdate.split(' ')[0].split('.')[1]) - 1,
                    parseInt(startdate.split(' ')[0].split('.')[0]),
                    parseInt(startdate.split(' ')[1].split(':')[0]),
                    parseInt(startdate.split(' ')[1].split(':')[1]),
                    parseInt(startdate.split(' ')[1].split(':')[2], 0));

            var obj = {
                "subject": subject,
                "date": date,
                "id": index
            };

            elements.push(obj);
        });

        elements.sort(function(b, a) {
            return b.date.getTime() - a.date.getTime();
        });

        var insertDay = -1;
        for (var i = 0; i < elements.length; i++) {
            if (insertDay != elements[i].date.getDay()) {
                $("#dialog-selectable").append("<h4 style='font-size: small' class='ui-widget-content'>"
                        + weekday[elements[i].date.getDay()] + " (" + FormatDate(elements[i].date, '.') + ")" + "</h4>");
            }

            insertDay = elements[i].date.getDay();

            $("#dialog-selectable").append("<div class='ui-widget-content' time='" + elements[i].date
                    + "' style='font-size: small' id='"
                    + elements[i].id + "'>" + FormatTime(elements[i].date, ":") + " " + elements[i].subject + "</div>");
        }

        var startStrSplited = $("#buttonNext").attr("name").split(" ")[0].split('.');
        var endStrSplited = $("#buttonNext").attr("name").split(" ")[1].split('.');

        var start = new Date();
        start.setFullYear(parseInt(startStrSplited[0]));
        start.setMonth(parseInt(startStrSplited[1]) - 1);
        start.setDate(parseInt(startStrSplited[2]));

        var end = new Date();
        end.setFullYear(parseInt(endStrSplited[0]));
        end.setMonth(parseInt(endStrSplited[1]) - 1);
        end.setDate(parseInt(endStrSplited[2]));

        var prevStart = new Date(new Date(start).setDate(start.getDate() - 7));
        var prevEnd = new Date(new Date(end).setDate(end.getDate() - 7));
        var prevInterval = FormatDate(prevStart, ".") + " " + FormatDate(prevEnd, ".");

        var nextStart = new Date(new Date(start).setDate(start.getDate() + 7));
        var nextEnd = new Date(new Date(end).setDate(end.getDate() + 7));
        var nextInterval = FormatDate(nextStart, ".") + " " + FormatDate(nextEnd, ".");

        $('#buttonPrev').attr("name", prevInterval);
        $('#buttonNext').attr("name", nextInterval);

        $("#dialog-form").dialog( "open" );

        $("#currentDateInverval").text(" " + FormatDate(start, '.') + " - " + FormatDate(end, '.') + " ");
    }

    /**
     * @return {string}
     */
    function FormatDate(date, delimiter) {
        var year = date.getFullYear();
        var month = "" + (date.getMonth() + 1);
        if (month.length == 1) {
            month = ("0" + month);
        }
        var day = "" + date.getDate();
        if (day.length == 1) {
            day = ("0" + day);
        }
        return (year + delimiter + month + delimiter + day);
    }

    /**
     * @return {string}
     */
    function FormatTime(date, delimiter) {
        var hours = "" + (date.getHours());
        if (hours.length == 1) {
            hours = ("0" + hours);
        }
        var min = "" + date.getMinutes();
        if (min.length == 1) {
            min = (min + "0");
        }
        return (hours + delimiter + min);
    }

    function LoadGroups() {	
		
        $.ajax({
            type : 'GET',
            contentType: "application/json; charset=utf-8",
            url : users_list_url,
            dataType : 'json',
            headers : {Accept : "application/json", "Access-Control-Allow-Origin" : "*"},
            crossDomain : true,
            success : BuildGroupsFunc ,
            error : function(data, textStatus, errorThrown) {
                //console.log("error" + ' ' + JSON.stringify(data) + textStatus  + errorThrown);}
                //console.log("error" + ' ' + textStatus  + errorThrown);
				}
        });
    }

    function BuildGroupsFunc(data) {
		
		$("#accordion").html("");
        var accordion = $("#accordion");
        var sortKeys = [];
        usersList = [];

        $.each(data.viewentry, function (index, value) {

            var jobTitle;
            try {
                jobTitle = value.entrydata[12].text[0];
            } catch (err) {
                // ignore
            }

            var userData = {
                "id": value.entrydata[0].text[0],
                "fullName": value.entrydata[1].text[0] + " " + value.entrydata[2].text[0] + " " + value.entrydata[3].text[0],
                "company": value.entrydata[8].text[0],
                "country": value.entrydata[9].text[0],
                "city": value.entrydata[7].text[0],
                "jobTitle": jobTitle,
                "department": value.entrydata[11].text[0],
                "phone": value.entrydata[6].text[0],
                "mail": value.entrydata[10].text[0],
				"responsibility": value.entrydata[13].text[0]
            };

            usersList.push(userData);
			
			
			if(sort_by == "country"){
				var sortKey = value.entrydata[9].text[0];
			}else if(sort_by == "department"){
				var sortKey = value.entrydata[11].text[0];
			}else{
				var sortKey = value.entrydata[13].text[0];
			}


            if (sortKey == undefined || sortKey.trim().length == 0) {
                sortKey = "Other";
            }

            if ($.inArray(sortKey, sortKeys) == -1) {
                sortKeys.push(sortKey);
            }
        });

        sortKeys.sort(function(a, b) {
            if (a == "Other") return 1;
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        });

        $.each(sortKeys, function (index, sortKey) {

            var appendList = "";
			if(sortKey != "Other"){
            accordion.append("<h3 class='h3 list-group-item' style='font-size: small'>" + sortKey + "</h3>");
			}
            $.each(usersList, function (index, user) {
										
                if (sortKey == "Other") {
                    if (user[sort_by] == undefined || user[sort_by].trim().length == 0) {
                        //appendList += "<li style='padding: 0; margin: 0' class='ui-widget-content' id='" + user.id + "'>" + user.fullName + "</li>";
                    }
                } else {
                    if (user[sort_by] == sortKey) {
                        appendList += "<li style='padding: 0; margin: 0' class='ui-widget-content' id='" + user.id + "'>" + user.fullName + "</li>";
                    }
                }
            });
			if(sortKey != "Other"){
            accordion.append("<div style='padding: 0; margin: 0; font-size: small; border: 0'><ol style='list-style-type: none;'>" + appendList + "</ol></div>");
			}
        });

        accordion.accordion('destroy').accordion({
            collapsible: true,
            heightStyle: "content"
        });
		
		// Top search
		var sQuery = $('#sQuery').val();
		if(sQuery != ""){
			$("#filter_text").val(sQuery);
			searchUser(sQuery);
		}		
		
    }

});