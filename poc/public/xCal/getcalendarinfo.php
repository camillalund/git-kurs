<?php

/*
	//https://xcal.symfoni.com/symfoni/xcal.nsf/getcalendarinfo?openagent&FromDate=20140324&ToDate=20140330&Username=CN=Anders Bache-Mathisen/OU=NO/O=SYM
*/

	$output = new stdClass();	
	$events = array();
	
	$fromDate = $_GET["FromDate"];
	$toDate = $_GET["ToDate"];
	//$name = $_GET["name"];

	$meetings = array("meeting 1", "meeting 2", "meeting 3");
	$locations = array("HQ", "Oslo", "Stavanger");
	$chairs = array("Wade Munoz", "Melanie Black", "Allen Logan", "Genevieve Drake", "Darrin Silva", "Duane Bell", "Esther Wagner", "Joe Wilson", "Devin Wells", "Shirley Sutton");
	$requireds = array("Brendan Wagner", "Cindy Lane", "Tasha Lyons", "Henrietta Vasquez", "Joyce Knight", "Rosemary Bryant", "Rachel Poole", "Randal Jimenez", "Shelley Hubbard", "Cesar Little", "Betsy Page", "Julius Douglas", "Jody Santos", "Rebecca Moody", "Tiffany Malone", "Megan Sutton", "Miranda Leonard", "Alexander Allison", "Austin Fox", "Rudy Spencer", "Caleb Luna", "Sara Brooks", "Boyd Griffin", "Allan Sherman", "Cary Garrett");
	$optionals = array("Brendan Wagner", "Cindy Lane", "Tasha Lyons", "Henrietta Vasquez", "Joyce Knight", "Rosemary Bryant", "Rachel Poole", "Randal Jimenez", "Shelley Hubbard", "Cesar Little", "Betsy Page", "Julius Douglas", "Jody Santos", "Rebecca Moody", "Tiffany Malone", "Megan Sutton", "Miranda Leonard", "Alexander Allison", "Austin Fox", "Rudy Spencer", "Caleb Luna", "Sara Brooks", "Boyd Griffin", "Allan Sherman", "Cary Garrett");
	$body = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.";
	$rooms = array("The Conference Room", "The Auditorium", "The Mac Lab", "The Networking Lab", "The CAD Lab", "Engineering (electrical)", "The Fab", "Mechanical Engineering", "The Mezanine", "The Elevator");
	$resources = array("resource 1");
	
	function rand_date($min_date, $max_date) {

    	$min_epoch = strtotime($min_date);
    	$max_epoch = strtotime($max_date);

    	$rand_epoch = rand($min_epoch, $max_epoch);

		//24.03.2014 10:00:00 CET
    	return date('d.m.Y H:i:s', $rand_epoch);
	}
	

	for ($i = 1; $i<15; $i++) {
		$event = array();
		$event[0] = array("subject", $meetings[array_rand($meetings)]);
		$event[1] = array("location", $locations[array_rand($locations)]);
		$event[2] = array("startdate", rand_date($fromDate, $toDate)." CET");
		$event[3] = array("enddate", rand_date($fromDate, $toDate)." CET");
		$event[4] = array("chair", $chairs[array_rand($chairs)]);
		$event[5] = array("required", $requireds[array_rand($requireds)].", ".$requireds[array_rand($requireds)].", ".$requireds[array_rand($requireds)]);
		$event[6] = array("optional", $optionals[array_rand($optionals)]);
		$event[7] = array("body", $body);
		$event[8] = array("room", $rooms[array_rand($rooms)]);
		$event[9] = array("resources", $resources[array_rand($resources)]);	
		
		if (strpos($event[2][1],'13.05') !== false) {
    		continue;
		}	

		$events[] = $event;
	}
	
	//Adding competition event Rebecca McQuillan
	if ($fromDate == "20140512") {
		$event = array();
		$event[0] = array("subject", "Making unique Parrot");
		$event[1] = array("location", "the quarrelsome Amazon");
		$event[2] = array("startdate", date('d.m.Y H:i:s', strtotime('05/13/2014 08:00'))." CET");
		$event[3] = array("enddate", date('d.m.Y H:i:s', strtotime('05/12/2014 16:00'))." CET");
		$event[4] = array("chair", "Louis Richardson");
		$event[5] = array("required", "G. Aratoo <g,aratoo@nonsense.es>");
		$event[6] = array("optional", "");
		$event[7] = array("body", "mixing Microglossini and Pezoporini, in cooperation with Goliath Aratoo");
		$event[8] = array("room", "");
		$event[9] = array("resources", "");		
		$events[] = $event;
	}
	
	//End adding competition events
	
	$output->success = TRUE;
	$output->data = $events;
	
	header('Content-Type: application/json');
	echo json_encode($output);

?>
