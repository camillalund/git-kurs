<?php
	ini_set('display_startup_errors',1);
	ini_set('display_errors',1);
	error_reporting(-1);
?>

<?php
/*
	//https://xcal.symfoni.com/symfoni/xcal.nsf/listinfo?readviewentries&count=20000&outputFormat=JSON
*/

	$firstNames = array("Carla", "Kathryn", "Crystal", "Hope", "Chester", "Marty", "Laurence", "Kimberly", "Roberta", "Vicki", "Elaine", "Sheldon", "Kurt", "Mitchell", "Mae", "Michelle", "Nathan", "Hugh", "Tom", "Damon", "Jeannie", "Dallas", "Gilberto", "Beatrice", "Ann");
	$middleNames = array("Teriann", "Ephrayim", "Anet", "Abdul", "Corrie", "Moe", "Conni", "Gerrie", "Daffy", "Les", "Natty", "Irvin", "Wylma", "Granger", "Tandy", "Patton", "Vanni", "Mathias", "Ardisj", "Paul", "Alice");
	$lastNames = array("Rovida", "Snow", "Lin", "Pedulla", "Balzac", "Rothberg", "Eales", "Filman", "Fletcher", "Cusatelli", "Nardone", "Quinn", "Ducharme", "Labunka", "Morvay", "Davisson", "Messere", "Tootell", "Tennyson", "Bolafka", "Huber");
	$countries = array("Belgium", "Denmark", "Netherlands", "Norway", "Sweden");
	$departments = array("Marketing", "Accounting", "Administration");
	$cities = array("Brussels", "Copenhagen", "Amsterdam", "Oslo", "Stockholm");
	$phoneNumbers = array("(855) 730-8361", "(899) 917-5812", "(811) 539-2822", "(811) 467-8761", "(833) 913-3854", "(822) 723-6455", "(811) 952-7756", "(855) 240-0015", "(899) 394-1589", "(844) 039-5954", "(811) 076-7329", "(833) 705-8218", "(833) 137-7170", "(855) 107-0528", "(833) 043-3739", "(844) 442-7613", "(822) 415-6859", "(811) 670-2088", "(899) 324-1536", "(899) 897-5003", "(833) 182-0310", "(899) 902-9910", "(855) 344-9809", "(822) 962-0755", "(811) 388-7087");
	$companyNames = array("xCalendar");
	$jobTitles = array("Consultant", "System Administrator", "Developer", "Sales responsible");
	$responsibilities = array("", "", "", "", "", "", "","","","","","", "Kitchen", "Library", "Training", "Social events");

	$output = new stdClass();	
	$viewentry = array();
	
	for ($i = 1; $i<101; $i++) {
		$entryData = array();
		
		$entry0 = new stdClass();
		$entry0->text[0] = "Common name";
		$entryData[] = $entry0;
		
		$entry1 = new stdClass();
		$entry1->text[0] = $firstNames[array_rand($firstNames)];
		$entryData[] = $entry1;
		
		$entry2 = new stdClass();
		$entry2->text[0] = $middleNames[array_rand($middleNames)];
		$entryData[] = $entry2;
		
		$entry3 = new stdClass();
		$entry3->text[0] = $lastNames[array_rand($lastNames)];
		$entryData[] = $entry3;
		
		$entryData[] = "";
		$entryData[] = "";
		
		$entry6 = new stdClass();
		$entry6->text[0] = $phoneNumbers[array_rand($phoneNumbers)];
		$entryData[] = $entry6;
		
		$entry7 = new stdClass();
		$entry7->text[0] = $cities[array_rand($cities)];
		$entryData[] = $entry7;
		
		$entry8 = new stdClass();
		$entry8->text[0] = $companyNames[array_rand($companyNames)];
		$entryData[] = $entry8;
		
		$entry9 = new stdClass();
		$entry9->text[0] = $countries[array_rand($countries)];
		$entryData[] = $entry9;
		
		$entry10 = new stdClass();
		$entry10->text[0] = $entry1->text[0].".".$entry3->text[0]."@".$entry8->text[0].".com";
		$entryData[] = $entry10;
		
		$entry11 = new stdClass();
		$entry11->text[0] = $departments[array_rand($departments)];
		$entryData[] = $entry11;
		
		$entry12 = new stdClass();
		$entry12->text[0] = $jobTitles[array_rand($jobTitles)];
		$entryData[] = $entry12;
		
		$entry13 = new stdClass();
		$entry13->text[0] = $responsibilities[array_rand($responsibilities)];
		$entryData[] = $entry13;	
		
		
		$obj = new stdClass();
		$obj->entrydata = $entryData;
		$viewentry[] = $obj;
	}
	
	//Adding competiotion user
	$entryData = array();
		
		$entry0 = new stdClass();
		$entry0->text[0] = "louis";
		$entryData[] = $entry0;
		
		$entry1 = new stdClass();
		$entry1->text[0] = "Louis";
		$entryData[] = $entry1;
		
		$entry2 = new stdClass();
		$entry2->text[0] = "";
		$entryData[] = $entry2;
		
		$entry3 = new stdClass();
		$entry3->text[0] = "Richardson";
		$entryData[] = $entry3;
		
		$entryData[] = "";
		$entryData[] = "";
		
		$entry6 = new stdClass();
		$entry6->text[0] = "(456) 417-48-21";
		$entryData[] = $entry6;
		
		$entry7 = new stdClass();
		$entry7->text[0] = "Oslo";
		$entryData[] = $entry7;
		
		$entry8 = new stdClass();
		$entry8->text[0] = "IBM";
		$entryData[] = $entry8;
		
		$entry9 = new stdClass();
		$entry9->text[0] = "Norway";
		$entryData[] = $entry9;
		
		$entry10 = new stdClass();
		$entry10->text[0] = $entry1->text[0].".".$entry3->text[0]."@".$entry8->text[0].".com";
		$entryData[] = $entry10;
		
		$entry11 = new stdClass();
		$entry11->text[0] = "Enthusiasts";
		$entryData[] = $entry11;
		
		$entry12 = new stdClass();
		$entry12->text[0] = "Worldwide Storyteller & Enthusiast";
		$entryData[] = $entry12;
		
		$entry13 = new stdClass();
		$entry13->text[0] = "Intranet";
		$entryData[] = $entry13;
		
		
		$obj = new stdClass();
		$obj->entrydata = $entryData;
		$viewentry[] = $obj;
	
	//End competiotion user
	
	function sortByOrder($a, $b) {
    	return strcmp($a->entrydata[1]->text[0], $b->entrydata[1]->text[0]);
	}

	usort($viewentry, 'sortByOrder');
	


	$output->viewentry = $viewentry;
	$output->toplevelentries = 100;
	
	header('Content-Type: application/json');
	echo json_encode($output);
?>