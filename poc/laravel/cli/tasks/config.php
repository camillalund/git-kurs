<?php namespace Laravel\CLI\Tasks;

use Laravel\Bundle;
use Laravel\File;

class Config extends Task {

	/**
	 * Install the config files of the given bundles into the application's config directory.
	 *
	 * @param  array  $bundles
	 * @return void
	 */
	public function publish($bundles)
	{
		foreach ($bundles as $bundle) {
			echo "Publishing [$bundle] config files...";

			$source = Bundle::path($bundle).'config';
			if (File::exists($source)) {
				File::cpdir($source, path('app')."config/bundles/$bundle");
				echo "  Config files published.".PHP_EOL;
			}
			else echo "  No config files to publish, skipping...".PHP_EOL;
		}
	}
}