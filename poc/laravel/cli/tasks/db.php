<?php namespace Laravel\CLI\Tasks;

use Laravel\IoC;
use Laravel\Str;
use Laravel\Bundle;

class Db extends Task {
	
	/**
	 * Seed the database with default data.
	 *
	 * @param  string  $seed
	 * @return void
	 */
	public function seed($seed)
	{
		if (empty($seed)) $seed = array( 'Database' );
		list($bundle, $seed) = Bundle::parse($seed[0]);
		
		// If the task exists within a bundle, we will start the bundle so that any
		// dependencies can be registered in the application IoC container. If the
		// task is registered in the container,  we'll resolve it.
		if (Bundle::exists($bundle))
		{
			Bundle::start($bundle);
		}

		$seed = static::resolve($bundle, $seed);

		// Once the bundle has been resolved, we'll make sure we could actually
		// find that seed, and then verify that the run method exists on the seed
		// so we can successfully call it without a problem.
		if (is_null($seed))
		{
			throw new \Exception("Sorry, I can't find that seed.");
		}

		if (is_callable(array($seed, 'run')))
		{
			$seed->run();
		}
		else
		{
			throw new \Exception("Sorry, that seed doesn't have a run() method!");
		}
	}

	protected static function resolve($bundle, $seed)
	{
		$identifier = Bundle::identifier($bundle, $seed);

		// First we'll check to see if the seed has been registered in the
		// application IoC container. This allows all dependencies to be
		// injected into seeds for more flexible testability.
		if (IoC::registered("seed: {$identifier}"))
		{
			return IoC::resolve("seed: {$identifier}");
		}
		
		// If the seed file exists, we'll format the bundle and seed name
		// into a seed class name and resolve an instance of the class so that
		// the requested method may be executed.
		if (file_exists($path = Bundle::path($bundle).'seeds/'.$seed.EXT))
		{
			require_once $path;

			$seed = static::format($bundle, $seed);

			return new $seed;
		}
	}

	/**
	 * Format a bundle and seed into a seed class name.
	 *
	 * @param  string  $bundle
	 * @param  string  $seed
	 * @return string
	 */
	protected static function format($bundle, $seed)
	{
		$prefix = Bundle::class_prefix($bundle);

		return '\\'.$prefix.Str::classify($seed).'_Seeder';
	}
}