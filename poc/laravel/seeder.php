<?php namespace Laravel;

use Laravel\CLI\Tasks\Db;

abstract class Seeder {

	public function call($seed) {
		$db = new Db;
		$db->seed(array($seed));
	}
}