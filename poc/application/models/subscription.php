<?php


class Subscription extends Eloquent {
	public static $table = 'custom.subscriptions';

	public function tags()
	{
			return $this->belongs_to('cms.keywords','tag_id');
	}
	
	public function users()
	{
			return $this->belongs_to('cms.users');
	}
	
	public function employee() //funkar inte
	{
		return $this->has_many_and_belongs_to('Employee', 'custom.employees', 'tag_id', 'user_id');	
	}

      

}
