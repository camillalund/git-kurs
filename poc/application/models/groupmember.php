<?php


class Groupmember extends Eloquent {
	public static $table = 'custom.groupmembers';

	public function pages()
	{
			return $this->belongs_to('cms.pages');
	}
	
	public function users()
	{
			return $this->belongs_to('cms.users');
	}

      

}
