
<!DOCTYPE html> 
<html lang="en"> 
  <head> 
    <meta charset="utf-8"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    {{ CMS::title() }} 
    {{ CMS::description() }} 
    {{ CMS::author() }} 
    {{ CMS::keywords() }} 

	<script type="text/javascript">
		var baseurl = "{{URL::Base()}}/no";
    </script>
    <!-- Placed at the end of the document so the pages load faster --> 
	{{ HTML::script('/js/jquery-1.11.1.min.js') }}
	{{ HTML::script('/js/jquery.bxslider.min.js') }} 
	{{ HTML::script('/js/sym.js') }}
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> -->

	<!-- Bootstrap core JavaScript 
    ================================================== --> 
	{{ Bootstrap::script() }}

    <link rel="shortcut icon" href="favicon.ico"> 

    <!-- Bootstrap core CSS --> 
    {{ Bootstrap::style() }} 

	{{ HTML::style('/css/jquery.bxslider.css') }}  

	{{ HTML::style('css/eventCalendar_theme_responsive.css') }}
    {{ HTML::style('css/eventCalendar.css') }}
 	{{ HTML::style('/css/symfoni.css') }}

	<!-- Fonts -->
	<script src="//use.typekit.net/lax3wry.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>         

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    {{ Bootstrap::html5shim() }} 

    <style type="text/css">
    
    	.goog-te-banner-frame.skiptranslate {
        	display: none !important;
        } 
    	body {
        	top: 0px !important; 
        }

  		.h3 {
    		/*padding: 0.2em 0.3em;
    		margin-bottom: 0.5em;
    		position: relative;*/
  		}


  		.portlet-placeholder {
    		border: 2px dashed black;
    		margin: 0 1em 1em 0;
    		height: 50px;
    		background-color: #e3e3e3;
  		}

    </style>  
    
    <script>
  		$(function() {
    		$( ".col-md-4" ).sortable({
      			connectWith: ".col-md-4",
      			handle: ".widget .h3",
      			cancel: ".portlet-toggle",
      			placeholder: "portlet-placeholder ui-corner-all"
    		});

 			
    		$( ".widget" )
      			.addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
      			.find( ".widget .h3" )
        		//.addClass( "ui-widget-header ui-corner-all" );
        		//.prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

  		});
  </script>
    

<meta name="google-translate-customization" content="f8c9be81c71f51-bb44f125309c2de1-gbd3874a0ceede0ca-12"></meta> 


  </head> 
<?php
$dato = new Datetime("today");
$idag = $dato->format('d.m.Y');
?>
  <body> 
    <div class="navbar navbar-inverse navbar-static-top top-admin" role="navigation"> 
      <div class="container"> 
        <div class="navbar-header"> 
          <a class="navbar-brand" href="/">{{ Current::$config->site_title }}</a> 
        </div> 
          <div class="ngiadm"> 
            <ul class="nav navbar-nav navbar-right">
              @if (Security::isAllowed('general.admin'))
              <li><a href="{{ URL::to_secure('cms/admin') }}" target="_blank">Administrasjon <span class="glyphicon glyphicon-cog"></span></a></li>
              @endif
              @if (Security::isAllowed('general.admin'))
              <li><a href="{{ URL::to_secure('/admin-sms') }}">SMS <span class="glyphicon glyphicon-phone"></span></a></li>
              @endif
              <li><a href="{{ URL::to_secure('my-info') }}">Velkommen {{Auth::user()->firstname}} <!--{{Auth::user()->middlename}} {{Auth::user()->lastname}}--> <span class="glyphicon glyphicon-user"></span> &nbsp; {{$idag}}</a> </li>
            </ul> 
          </div><!--/.nav-collapse --> 
        </div><!--/.container-fluid --> 
      </div> 
    </div>


<div id="logo"> 
<div id="logo_wrap">                  
	<div class="container">
		<div class="row">
       	 <div class="col-md-12">
                  <a href="/"><img class="logo-img" src="/img/symfoni_logo.png" alt="Symfoni Software" /></a>

			<form action="/no/xcal" class="navbar-form navbar-right last">
				<div class="form-group">
      				<input type="text" class="form-control" placeholder="Ansatte" name="q" id="a">
   				</div><!-- /form-group -->
                <button class="btn btn-default" type="submit">Søk</button>
          	</form> 

			<form action="/no/soek" class="navbar-form navbar-right"  id="sann-sok">
				<div class="form-group">
      				<input type="text" class="form-control" placeholder="Hele intranettet" name="q" id="s">
   				</div><!-- /form-group -->
                <button class="btn btn-default" type="submit">Søk</button>
          	</form> 

        <div class="navbar-header"> 
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".mob_search, .sym-menu"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
          </button> 
        </div> 


         </div>
		</div> 
	</div> 
</div> 
</div>                   




    <div class="navbar-collapse sym-menu collapse" role="navigation" aria-expanded="true"> 
      <div class="container"> 


<!-- MOB SEARCH -->
   <div id="mob_search">
   <form action="/no/soek" class="input-group">
      <input type="text" class="form-control" placeholder="Alt innhold" name="q" id="s" >
      <span class="input-group-btn">
        <button class="btn gray" type="button">Søk</button>
      </span>
   </form>

   <form action="/no/xcal" class="input-group">
      <input type="text" class="form-control" placeholder="Ansatte" name="q" id="a" >
      <span class="input-group-btn">
        <button class="btn gray" type="button">Søk</button>
      </span>
   </form>
   </div>                  




<!-- Hurtiglenker -->
<div class="nav navbar-right dropdown ngilinks">
  <button type="button" class="btn btn-col dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    Hurtiglenker <span class="caret"></span>
  </button>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="/no/sannkassen/lenke-1">Maconomy</a></li>
    <li><a href="/no/sannkassen/lenke-1">HR systemet</a></li>
    <li><a href="/no/sannkassen/lenke-1">Brukerstøtte</a></li>
    <li><a href="#" target="_blank" style="border-top: solid 1px #ccc; margin-top: 10px">Oppslagstavle</a></li>
    <li><a href="http://www.tek.no" target="_blank">TEK.no (webside)</a></li>
  </ul>
</div>


            @menu('main', 2) 

        </div><!--/.container --> 
      </div> 
    </div> 







    @yield('content') 

<footer class="footer">                  
    <div class="container"> 


        <p>&copy; {{ Current::$config->customer_name }} 2015</p> 
        <div style="float:right"><a href="https://devsites01.symfoni.com:8443/devngi/logout?service=https%3A%2F%2Fdevngi.symfoni.com%2Fno">Logg ut</a></div>
<div id="google_translate_element"></div>
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'no', includedLanguages: 'de,en,fi,no,sv', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    </div> <!-- /container --> 
</footer>



  </body> 
</html> 
