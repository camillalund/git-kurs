
<!DOCTYPE html> 
<html lang="en"> 
  <head> 
    <meta charset="utf-8"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>Intranett - ISS</title>
    {{ CMS::description() }} 
    {{ CMS::author() }} 
    {{ CMS::keywords() }} 

	<script type="text/javascript">
		var baseurl = "{{URL::Base()}}/no";
    </script>
    <!-- Placed at the end of the document so the pages load faster --> 
	{{ HTML::script('/js/jquery-1.11.1.min.js') }}
	{{ HTML::script('/js/jquery.bxslider.min.js') }} 
	{{ HTML::script('/js/ngi.js') }}
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css"> -->

	<!-- Bootstrap core JavaScript 
    ================================================== --> 
	{{ Bootstrap::script() }}

    <link rel="shortcut icon" href="favicon.ico"> 

    <!-- Bootstrap core CSS --> 
    {{ Bootstrap::style() }} 

	{{ HTML::style('/css/jquery.bxslider.css') }}  

	{{ HTML::style('css/eventCalendar_theme_responsive.css') }}
    {{ HTML::style('css/eventCalendar.css') }}
 	{{ HTML::style('/css/iss.css') }}

	<!-- Fonts -->
	<script src="//use.typekit.net/dub6omz.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>        

    <!-- Custom styles for this template --> 
    <style type="text/css"> 
      /* Move down content because we have a fixed navbar that is 50px tall */ 
      body { 
        padding-top: 50px; 
        padding-bottom: 20px; 
      } 
    </style>  

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    {{ Bootstrap::html5shim() }} 

    <style type="text/css">
    
    	.goog-te-banner-frame.skiptranslate {
        	display: none !important;
        } 
    	body {
        	top: 0px !important; 
        }

  		.h3 {
    		/*padding: 0.2em 0.3em;
    		margin-bottom: 0.5em;
    		position: relative;*/
  		}


  		.portlet-placeholder {
    		border: 2px dashed black;
    		margin: 0 1em 1em 0;
    		height: 50px;
    		background-color: #e3e3e3;
  		}
    
    </style>  
    
    
    
    
    
       
    <meta name="google-translate-customization" content="33b08132d782d896-634491fefeb03e63-g9a5ee2c3310d5257-12"></meta>

  </head> 
<?php
$dato = new Datetime("today");
$idag = $dato->format('d.m.Y');
?>
  <body> 
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
      <div class="container"> 
        <div class="navbar-header"> 
    <!--
          <a class="navbar-brand" href="/">{{ Current::$config->site_title }}</a> 
              -->
        </div> 
          <div class="navbar-collapse collapse"> 
            <ul class="nav navbar-nav navbar-right">
              @if (Security::isAllowed('general.admin'))
              <li><a href="{{ URL::to_secure('cms/admin') }}" target="_blank">Administrasjon <span class="glyphicon glyphicon-cog"></span></a></li>
              @endif
              @if (Security::isAllowed('admin.read'))
              <li><a href="{{ URL::to_secure('/admin-sms') }}">SMS <span class="glyphicon glyphicon-phone"></span></a></li>
              @endif
              <li><a href="{{ URL::to_secure('my-info') }}">Velkommen {{Auth::user()->firstname}} <!--{{Auth::user()->middlename}} {{Auth::user()->lastname}}--> <span class="glyphicon glyphicon-user"></span> &nbsp; {{$idag}}</a> </li>
            </ul> 
          </div><!--/.nav-collapse --> 
        </div><!--/.container-fluid --> 
      </div> 
    </div>

<div id="logo">                  
	<div class="container">
		<div class="row">
       	 <div class="col-md-12">
                  <a href="/"><img class="logo-img" src="/cms/images/24/340/340/iss-header-logo.png?_dc=1421249688000" alt="NGI" /></a>

			<form action="/no/xcal" class="navbar-form navbar-right last">
				<div class="form-group">
      				<input type="text" class="form-control" placeholder="Ansatte" name="q" id="a">
   				</div><!-- /form-group -->
                <button class="btn btn-default btn-black" type="submit">Søk</button>
          	</form> 

			<form action="/no/soek" class="navbar-form navbar-right"  id="sann-sok">
				<div class="form-group">
      				<input type="text" class="form-control" placeholder="Intranett" name="q" id="s">
   				</div><!-- /form-group -->
                <button class="btn btn-default btn-black" type="submit">Søk</button>
          	</form>                 

         </div>
		</div> 
	</div> 
</div> 

    <div class="navbar ngimenu" role="navigation"> 
      <div class="container"> 
        <div class="navbar-header"> 
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
          </button> 
        </div> 
          <div class="navbar-collapse collapse"> 
            @menu('main', 2) 

<!-- Hurtiglenker -->
<div class="btn-group pull-right dropdown">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    Hurtiglenker <span class="caret"></span>
  </button>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="/no/sannkassen/lenke-1">Lenke 1</a></li>
    <li><a href="http://www.symfoni.com" target="_blank">Symfoni Software</a></li>
  </ul>
</div>

          </div><!--/.nav-collapse --> 
        </div><!--/.container-fluid --> 
      </div> 
    </div> 

    @yield('content') 
 
    <div class="container"> 
      <hr> 
 
      <footer> 
        <p>&copy; ISS 2015</p> 
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'no', includedLanguages: 'en,no', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
      </footer> 
    </div> <!-- /container --> 
 



  </body> 
</html>
