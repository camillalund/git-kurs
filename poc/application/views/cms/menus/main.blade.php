<ul class="nav navbar-nav"> 
  @foreach ($menuitems as $item) 
    @if (!isset($item->children)) 
      @if ($item->id == Current::$page->id) 
        <li class="active"> 
      @else 
        <li> 
      @endif 
      {{ CMS::pageLink($item) }}</li> 
    @else 
      <li class="dropdown"> 
        <a href="{{ CMS::pageUrl($item) }}" class="dropdown-toggle" data-toggle="dropdown">{{ $item->subject }} <b class="caret"></b></a> 
        <ul class="dropdown-menu"> 
          @foreach ($item->children as $item) 
            <li>{{ CMS::pageLink($item) }}</li> 
          @endforeach 
        </ul> 
      </li> 
    @endif 
  @endforeach 
</ul> 
