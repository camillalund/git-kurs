@layout($layout) 
 
@section('content') 
	<div class="container"> 
		<h1>{{ $subject }}</h1> 
		<div class="row"> 
			<div class="col-sm-8 blog-main"> 
				 

<div id="chart" style="height: 500px;">
@element('bikechart')
</div> 



	</div> 


			<div class="col-sm-3 col-sm-offset-1"> 

					<div class="col-sm-10 pull-right">
   						<a class="btn btn-danger" href="?edit=1"><span class="glyphicon glyphicon-pencil"></span> Legg til sykkeldata</a>
					</div>

				@foreach(Current::$page->related_pages as $related) 
					<div class="sidebar-module sidebar-module-inset"> 
						<h4>{{ $related->subject }}</h4> 
						<p> {{ @isset($related->summary) }}</p> 
						<p><a class="btn btn-default" href="{{ $related->url() }}" role="button">View details &raquo;</a></p> 
					</div> 
				@endforeach 
			</div> 
		</div> 
	</div> 



@endsection 

                              