@layout($layout) 
 
@section('content') 
	<!-- Main jumbotron for a primary marketing message or call to action --> 
	<div class="jumbotron"> 
      <div class="container"> 
        <h1>{{ $subject }}</h1> 
        <p>{{ $preamble }}</p> 
      </div> 
	</div> 
    <div class="container"> 
        <div class="row">
			<div class="col-md-12">{{Current::$content->body}}</div>
        </div>
        <div class="row">
            <div class="col-md-8">
            <!-- KOLONNE 2 -->
				@element('groups_all')

            </div>

			<div class="col-md-4">
            <!-- KOLONNE 1 -->
                <button type="button" id="newGroupBtn" class="btn btn-danger btn-lg show-grid"><span class="glyphicon glyphicon-plus-sign"></span> &nbsp;Opprett ny gruppe</button>
  		        <div class="panel panel-default" id="newGroup">
					<div class="panel-heading">Ny gruppe</div>
    				<div class="panel-body">
						<form role="form" method="post" action="{{URL::base().'/ngi/group/'.Current::$page->id}}">
                        	<div class="form-group">
                            	<label for="subject">Gruppe tittel</label>
                            	<input type="text" class="form-control" id="subject" name="subject" placeholder="Tittel på gruppen">
                          	</div>
                            <div class="form-group">
                            	<label for="preamble">Kort info</label>
                            	<textarea class="form-control" rows="5" name="preamble" id="preamble"></textarea>
                          	</div>
							<div class="checkbox">
    							<label>
      								<input type="checkbox" name="closed_group" id="closed_group"> Lukket gruppe
    							</label>
  							</div>
  							<button type="submit" class="btn btn-default">Opprett gruppe</button>
                            <input type="hidden" name="pageuri" value="{{URL::base().Current::$page->uri}}">
                            <input type="hidden" name="template_id" value="{{Current::$content->templateid}}">
						</form>
                    </div>
                </div>

                <div class="box">
                @element('groups_mine')
				</div>
            </div>


            

		</div>
    </div>

@endsection 