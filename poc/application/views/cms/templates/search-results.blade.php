@layout($layout) 
 
@section('content') 


<style type="text/css">
 #google-search * {
   box-sizing: content-box;

}
.cse .gsc-control-cse, .gsc-control-cse {
    padding: inherit !important;
    width: auto;
}

.gs-webResult.gs-result a.gs-title:link, .gs-webResult.gs-result a.gs-title:link b, .gs-imageResult a.gs-title:link, .gs-imageResult a.gs-title:link b {
color: #225A97 !important;
text-decoration: underline !important;
}

.gs-result .gs-title, .gs-result .gs-title * {
color: #225A97 !important;
text-decoration: underline !important;
    }

.gs-webResult div.gs-visibleUrl, .gs-imageResult div.gs-visibleUrl {
color: #676767 !important;
    }

.gs-webResult .gs-snippet, .gs-imageResult .gs-snippet, .gs-fileFormatType {
padding-left: 1%;
    }

.gsc-results .gsc-cursor-box .gsc-cursor-page {
    color: #225A97;
}

.gs-title, .gs-title:link, .gs-title:visited , .gs-title:hover, .gs-title:active {
color: #225A97 !important;
text-decoration: underline !important;
    }

.gs-image {
margin-left: 1%;
    float: left;
    }

.gs-image-box.gs-web-image-box.gs-web-image-box-portrait, .gs-image-box.gs-web-image-box.gs-web-image-box-landscape {
    visibility: hidden;
    width: 1px;
    height: 1px;
}
</style>

	<div class="container"> 
    	<h1>{{ $subject }}</h1> 
     		<div class="row"> 
			<div class="col-md-offset-2"> 

<div id="google-search">
<script>
  (function() {
    var cx = '005801327050111265129:0yziojuqeim';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>
</div>


		</div> 
		</div> 
</div> 

@endsection 