@layout($layout) 
 
@section('content') 
	<!-- Main jumbotron for a primary marketing message or call to action --> 
	<div class="jumbotron"> 
      <div class="container"> 
        <h1>{{ $subject }}</h1> 
        <p>{{ $preamble }}</p>
        @element('group_add_mylist')
      </div> 
	</div> 
    <div class="container"> 
        <div class="row">
			<div class="col-md-12 show-grid">{{Current::$content->body}}</div>
        </div>
        <div class="row">
            <div class="col-md-8">
            <!-- KOLONNE 2 -->

                @element('comments')

            </div>

			<div class="col-md-4">
            <!-- KOLONNE 1 -->
				<button type="button" class="btn btn-danger btn-lg"><span class="glyphicon glyphicon-book"></span> Wiki</button>
                <div class="box">
                <h3 class="h3">Filer</h3>

                	@if(isset(Current::$content->filefolder))


                        @if ($image1 = Current::$content->filefolder)
                        <ul class="list">
                        <?php
                           $media = Folder::find($image1);
                        ?>

                        @foreach($media->media as $file)
                            <li>
                            <div class="row show-grid"> 
                                <div class="col-md-6">
                                    <a href="{{$file->fileUrl()}}" target="_new">{{$file->filename}}</a>
                                </div>
                                <div class="col-md-6 show-grid">
                                    {{$file->updated_at}}
                                </div>
                            </div>
                            </li>

                        @endforeach
                        </ul>
                        @endif                                    

						<!--<button type="button" id="addfile" class="btn btn-danger btn-sm" data-pageid="{{Current::$page->id}}">Legg til fil</button>-->

                        <div class="row show-grid">
                        <div class="col-md-6">                                    

                        <h4> Legg til fil</h4>

						<form accept-charset="UTF-8" action="{{URL::to('ngi/media')}}" method="POST" id="addfile" enctype="multipart/form-data">
							{{Form::file('fil')}}
							<button style="margin-top: 10px" type="submit" class="btn btn-danger btn-sm" data-pageid="{{Current::$page->id}}"><span class="glyphicon glyphicon-open"></span> Last opp</button>

                            <input type="hidden" name="folder_id" value="{{Current::$content->filefolder}}">
                                <input type="hidden" name="pageuri" value="{{URL::base().Current::$page->uri}}">
                        </form>
                        </div>
                        </div>


 					@endif




				</div> <!-- End box -->



                @if(Current::$content->closed_group)
                	@element('groupmembers')
                	@element('members_add')
                @endif

                <div class="box">
                @element('groups_mine')
                </div>

				</div>                                
            </div>


            

		</div>
    </div>

@endsection 
