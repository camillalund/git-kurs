@layout($layout) 
 
@section('content') 

<?php

	$dato = date('d.m.Y',strtotime($publish_date));
	$bilde = "";

	if($end_date){
		$sluttdato = date('d.m.Y',strtotime($end_date));
        $dato = $dato ." - ". $sluttdato;
    }
?>


<div class="jumbotron">
	<div class="container"> 
		<h1>{{ $subject }}</h1> 
            <p><b>Dato:</b> {{$dato}}</p>
		<div class="row"> 
			<div class="col-sm-8 blog-main"> 
				<p class="lead">{{ $preamble }}</p> 
				<p>{{ $content }}</p> 
			</div> 
			<div class="col-sm-3 col-sm-offset-1"> 
				@foreach(Current::$page->related_pages as $related) 
					<div class="sidebar-module sidebar-module-inset"> 
						<h4>{{ $related->subject }}</h4> 
						<p> {{ @isset($related->summary) }}</p> 
						<p><a class="btn btn-default" href="{{ $related->url() }}" role="button">View details &raquo;</a></p> 
					</div> 
				@endforeach 
			</div> 
		</div> 
	</div> 
</div> 
 
@endsection 