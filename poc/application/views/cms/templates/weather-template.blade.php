@layout($layout) 
 
@section('content') 

<div class="jumbotron"> 
	<div class="container"> 
        <h1>{{ $subject }}</h1> 
        <p>{{ $preamble }}</p>


    </div> 
</div> 
<div class="container"> 
    <div class="row">


<script src="http://www.yr.no/sted/Norge/Oslo/Oslo/Karenslyst/ekstern_boks_tre_dager.js"></script>
<noscript><a href="http://www.yr.no/sted/Norge/Oslo/Oslo/Karenslyst/">yr.no: Værvarsel for Karenslyst</a></noscript>


<script src="http://www.yr.no/sted/Norge/Oslo/Oslo/Karenslyst/ekstern_boks_liten.js"></script>
<noscript><a href="http://www.yr.no/sted/Norge/Oslo/Oslo/Karenslyst/">yr.no: Værvarsel for Karenslyst</a></noscript>

		<div class="col-md-12">

			<p>{{ $body }}</p>
            <div class="clearfix">
<script src="http://www.yr.no/sted/Norge/Oslo/Oslo/Karenslyst/ekstern_boks_stripe.js"></script>
 <noscript><a href="http://www.yr.no/sted/Norge/Oslo/Oslo/Karenslyst/">yr.no: Værvarsel for Karenslyst</a></noscript>
                </div>

        </div>

            <div class="col-md-4">

            </div>

	</div> 
</div> 