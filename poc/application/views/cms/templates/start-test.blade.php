@layout($layout) 
 
@section('content') 
	<!-- Main jumbotron for a primary marketing message or call to action --> 
	<div class="jumbotron"> 
      <div class="container"> 
        <h1>{{ $subject }}</h1> 
        <p>{{ $preamble }}</p> 
      </div> 
	</div> 
    <div class="container"> 
        <div class="row">
			<div class="col-md-4">
            <!-- KOLONNE 1 -->
				@element('weather')
            </div>

            <div class="col-md-4">
            <!-- KOLONNE 2 -->
            @element('calendartwo')


            </div>
            
            <div class="col-md-4">
            <!-- KOLONNE 3 OM-->
            @element('bikechart')

				

            </div>
		</div>
    </div>

@endsection 