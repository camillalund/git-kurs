@layout($layout) 
 
@section('content') 
	<div class="container"> 
		<h1>{{ $subject }}</h1> 
		<div class="row"> 
			<div class="col-sm-8 blog-main"> 
				<p class="lead">{{ $preamble }}</p> 

                    <div id="chart" style="height: 500px;">
                        <ul class="chartslide">
                             <li>
                             <h3>Leder sammenlagt</h3> 
                                <div id="chart_leader" style="width: 100%; height: 480px;"></div>
                            </li>
                            <li>
                            <h3>Leder uke 1</h3>   
                                <div id="chart_week" style="width: 100%; height: 480px;"></div>
                            </li>
                            <li>
                            <h3>Leder proffgruppen</h3> 
                                <div id="chart_pro" style="width: 100%; height: 480px;"></div>
                            </li>
                        </ul>
                    </div> 

	</div> 


			<div class="col-sm-3 col-sm-offset-1"> 

					<div class="col-sm-10 pull-right">
   						<a class="btn btn-danger" href="?edit=1"><span class="glyphicon glyphicon-pencil"></span>Redigere sykkeldata</a>
					</div>

				@foreach(Current::$page->related_pages as $related) 
					<div class="sidebar-module sidebar-module-inset"> 
						<h4>{{ $related->subject }}</h4> 
						<p> {{ @isset($related->summary) }}</p> 
						<p><a class="btn btn-default" href="{{ $related->url() }}" role="button">View details &raquo;</a></p> 
					</div> 
				@endforeach 
			</div> 
		</div> 
	</div> 



@endsection 

<script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
		  ['Element', 'Kilometer', { role: 'style' }],
          //['Navn', 'Kilometer'],
          ['Kari',  127, '#ab2326'],
          ['Ola',  113, '#666'],
          ['Lise',  110, 'color: #666'],
		  ['Per',  103, '#666'],
          ['Anne',  96, 'color: #666'],
		  ['Ole',  88, '#666'],
          ['Lars',  76, 'color: #666'],
		  ['Trine',  73, 'color: #666'],
		  ['Knut',  60, '#666'],
          ['Arne',  10, 'color: #666'],
        ]);



        var options = {
          title: 'Kilometer',
          legend: { position: 'none' },
          bar: {groupWidth: '40%'},
          vAxis: {title: '',  titleTextStyle: {color: 'red'}
                 }
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_leader'));

        chart.draw(data, options);
      }
    </script> 


    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
		  ['Element', 'Kilometer', { role: 'style' }],
          //['Navn', 'Kilometer'],
		  ['Per',  10.3, '#ab2326'],
          ['Anne',  9.6, 'color: #666'],
		  ['Ole',  8.8, '#666'],
          ['Lars',  7.6, 'color: #666'],
		  ['Trine',  7.3, 'color: #666'],
		  ['Knut',  6.0, '#666'],
          ['Arne',  1.0, 'color: #666'],
        ]);



        var options = {
          title: 'Kilometer',
          legend: { position: 'none' },
          bar: {groupWidth: '40%'},
          vAxis: {title: '',  titleTextStyle: {color: 'red'}
                 }
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_week'));

        chart.draw(data, options);
      }
    </script>


    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
		  ['Element', 'Kilometer', { role: 'style' }],
          //['Navn', 'Kilometer'],
		  ['Per',  103, '#ab2326'],
          ['Anne',  96, 'color: #666'],
		  ['Ole',  88, '#666'],
          ['Lars',  76, 'color: #666'],
        ]);



        var options = {
          title: 'Kilometer',
          legend: { position: 'none' },
          bar: {groupWidth: '40%'},
          vAxis: {title: '',  titleTextStyle: {color: 'red'}
                 }
        };

        var chart = new google.visualization.BarChart(document.getElementById('chart_pro'));

        chart.draw(data, options);
      }
    </script>

<script type="text/javascript">
    window.onload = function(){
    $('.chartslide').bxSlider({
 	//mode: 'fade',
  	adaptiveHeight: true,
  	speed: 800,
  	auto: false,
  	pager: true
	});
	}

</script>               
