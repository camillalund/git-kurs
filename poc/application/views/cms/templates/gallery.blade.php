@layout($layout) 
 
@section('content') 
	<!-- Main jumbotron for a primary marketing message or call to action --> 
	<div class="jumbotron"> 
      <div class="container"> 
        <h1>{{ $subject }}</h1> 
        <p>{{ $preamble }}</p> 
      </div> 
	</div> 
    <div class="container"> 

			@element('gallery')

    </div>

@endsection 
