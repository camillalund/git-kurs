@layout($layout) 
 
@section('content') 


	<?php
        $relatedLinks = Current::$page->related_links()->get();
        $relatedPages = Current::$page->related_pages()->get();
        $relatedMedia = Current::$page->related_media()->get();
    ?>
	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
      <div class="container">
        <h1>{{ $subject }}</h1>
        <p>{{ $preamble }}</p>
		@element('filter')

      </div> 
	</div> 
    <div class="container"> 
        <div class="row">
			<div class="col-md-12 show-grid">{{Current::$content->body}}</div>
        </div>
        <div class="row">
            <div class="col-md-8">
            <!-- KOLONNE 2 -->

                @element('newslist')

            </div>

			<div class="col-md-4">
            <!-- KOLONNE 1 -->

        @if($relatedLinks || $relatedPages || $relatedMedia)
        <div class="col-md-12">
			<h3>Relatert informasjon</h3>
            <ul class="list-unstyled">
            @foreach($relatedPages as $pagelink)
				<li><a href="{{$pagelink->uri}}"><span class="glyphicon glyphicon-file"></span> {{$pagelink->subject}}</a></li>
    	    @endforeach

            @foreach($relatedMedia as $media)
				<li><a href="/cms/files/{{$media->id}}/{{$media->filename}}" target="_blank"><span class="glyphicon glyphicon-paperclip"></span> {{$media->title}}</a></li>
    	    @endforeach

        	@foreach($relatedLinks as $link)
            <?php 
            	if($link->new_window==1){
					$target="_blank";
                }else{
					$target="top";
                }
			?>
				<li><a href="{{$link->url}}" target="{{$target}}"><span class="glyphicon glyphicon-link"></span> {{$link->title}}</a></li>
    	    @endforeach
            </ul>
        </div>
    @endif
            </div>


            

		</div>
    </div>

@endsection 

