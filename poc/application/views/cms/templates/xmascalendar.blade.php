@layout($layout) 
 
@section('content') 
	<!-- Main jumbotron for a primary marketing message or call to action --> 
	<div class="jumbotron"> 
      <div class="container"> 
        <h1>{{ $subject }}</h1> 
        <p>{{ $preamble }}</p> 
      </div> 
	</div> 
    <div class="container"> 
        <div class="row">
			<div class="col-md-12">{{Current::$content->body}}</div>
        </div>
        <div class="row">
			<div class="col-md-4">
            <!-- KOLONNE 1 -->
    			@element('xmascal')

            </div>

            <div class="col-md-8">
            <!-- KOLONNE 2 -->
				@element('xmaswindow')

            </div>
            

		</div>
    </div>

@endsection 
