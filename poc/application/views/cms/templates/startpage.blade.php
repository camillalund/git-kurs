@layout($layout) 
 
@section('content') 
	<!-- Main jumbotron for a primary marketing message or call to action --> 

    <div class="container" id="startpage"> 
        <div class="row">
			<div class="importantMessage">
				@element('important_message')
			</div>
			<div class="col-md-4">
            <!-- KOLONNE 1 -->

                    <div class="box news widget">
                        <h3 class="h3">{{CMS::pageLink(Current::$content->pagepath_articles, 'Nyheter')}}</h3>
                        @element('post_add')
                        @element('news_shortlist')
                    </div>
                    <div class="box widget">
						<h3 class="h3" style="background-color:#f5f5f5">Nyheter fra internett</h3>
                        @element('rssngi')
                    </div>

                    <div class="box widget">
						<h3 class="h3">Meltwater nyheter</h3>
                        @element('rssmeltwater')
                    </div>
					<div class="box widget">
                        <h3 class="h3" style="margin-bottom:0;">Social Media</h3>
                        @element('tweets')
                    </div>
               
            </div>

            <div class="col-md-4">
            <!-- KOLONNE 2 -->
                <div class="posts box widget">
                    <h3 class="h3">{{CMS::pageLink(Current::$content->path_posts, 'Åpen post')}}</h3>
                    @element('post_add')
                    @element('posts_shortlist')
                </div>
                <div class="box widget">
                    <h3 class="h3">Driftsmeldinger</h3>
                    @element('post_add')
	                @element('driftsmeldinger')
                </div>
                <div class="box widget show-grid">
                    <h3 class="h3" style="background-color:#f5f5f5">Kantine</h3>
                	<div class="row show-grid">
    					<div class="col-md-12">
                   			<p>Dagens varmrett: Biff med bearnaise</p>
                    	</div>
                	</div>
                </div>
                <div class="box widget">
					@element('birthdays')
                </div>
                <div class="box widget show-grid">
                        @element('gallery')
                </div>
                <div class="box widget">
            		@element('comments_from_my_groups')
            		<div class="clearfix"></div>
                </div>
            </div>

            <div class="col-md-4">
            <!-- KOLONNE 3 -->

                    <div class="box widget">
                    <h3 class="h3">Neste arrangement</h3>
                        @element('calendar_add')
                	@element('calendarstartpage')
                </div>
                <div class="box widget">
	                <h3 class="h3">{{CMS::pageLink(Current::$content->path_bikechart, 'Vi sykler')}}</h3>
    	            @element('bikechart_startpage')
                </div>
				<div class="box widget">	
            		@element('myincidents')
                </div>
               	<div class="box widget">
	                @element('knowledge_base')
				</div>
               	<div class="box widget">
                	@element('mymessages')
                </div>
               	<div class="box widget">
                	@element('groups_mine')
                </div>
               	<div class="box widget">
					@element('weather')
                </div>

		</div>
    </div>


 <script>
  		$(function() {
    		$( ".col-md-4" ).sortable({
      			connectWith: ".col-md-4",
      			handle: ".h3",
      			cancel: ".portlet-toggle",
      			placeholder: "portlet-placeholder ui-corner-all"
    		});

 			
    		$( ".widget" )
      			.addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
      			.find( ".h3" )
        		//.addClass( "ui-widget-header ui-corner-all" );
        		//.prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

  		});
  </script>                       

@endsection 
