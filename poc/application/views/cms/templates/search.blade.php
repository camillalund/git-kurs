@layout($layout) 
 
@query('search') 
	return array( 
		'query' => Search::getQuery(), 
		'results' => Search::find(), 
		'type' => Search::getType(), 
	); 
@endquery 
 
@section('content') 
	<div class="container" id="search">  
		<h1>{{ $subject }}</h1> 
		<div class="row"> 
			<div class="col-md-offset-2"> 
				<form class="form-horizontal" role="form"> 
					<div class="form-group has-feedback col-md-8 col-xs-10"> 
						<input type="text" name="q" class="form-control" placeholder="Search" value="{{ @isset($search['query']) }}"> 
						<input type="hidden" name="type" value="{{ @isset($search['type']) }}"> 
						<span class="glyphicon glyphicon-search form-control-feedback"></span> 
					</div> 
					<div class="col-md-2 col-xs-2"> 
					<button type="submit" class="btn btn-default">Submit</button> 
					</div> 
				</form> 
			</div> 
		</div> 
 
		@if ($search['results'] !== false) 
			<div class="col-md-offset-1"> 
				@if($search['results']) 
					@if ($search['type'] === 'media') 
						@foreach($search['results'] as $result) 
							<div class="row col-md-10"> 
								<h4>{{ CMS::fileLink($result) }}</h4> 
								<p>{{ $result->description }}</p> 
							</div> 
						@endforeach 
					@else 
						@foreach($search['results'] as $result) 
							<div class="row col-md-10"> 
								<h4>{{ CMS::pageLink($result) }}</h4> 
								<p>{{ $result->summary }}</p> 
							</div> 
						@endforeach 
					@endif 
				@else 
					<div class="row col-md-10"> 
						No results found... 
					</div> 
				@endif 
			</div> 
		@endif 
	</div> 
 
@endsection 
