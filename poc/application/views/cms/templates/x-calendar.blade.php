@layout($layout)

@section('content')

    {{ HTML::style('xCal/css/offcanvas.css') }}
	{{ HTML::script('xCal/fullcalendar-2.1.1/lib/moment.min.js') }}
	{{ HTML::script('js/jquery-ui.js') }}


	{{ HTML::style('xCal/fullcalendar-2.1.1/fullcalendar.css') }}

	{{ HTML::script('xCal/fullcalendar-2.1.1/fullcalendar.min.js') }}
	{{ HTML::script('xCal/js/cal.js') }}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<style>
        /*body { font-size: 62.5%; }*/
html,
body {
  overflow-x: visible;
}		
#xcal{
  margin-top:20px; 
  min-height:700px;
}
    label, input { display:block; }
    input.text { margin-bottom:8px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    .ui-dialog .ui-state-error { padding: .3em; }
    #buttonNext { padding: .5em 1em; text-decoration: none; }
    #buttonPrev { padding: .5em 1em; text-decoration: none; }
    #switch_to { padding: .5em 1em; text-decoration: none; width: 150px; text-align: center; cursor: pointer;}
    #selectable {cursor: pointer;}
    #sort-nav>li.active>a {z-index:1;}
	#accordion {margin-top:-2px;}
	#accordion div {background:#fff;}
	#accordion h3:focus{outline:none;}	
	#accordion h3{cursor:pointer;}	
	#accordion .list-group-item {margin-top:0;}
    #accordion ol {margin-bottom:0; border-right: 1px solid #ddd; border-left: 1px solid #ddd; padding: 10px 0 10px 35px; overflow:auto; max-height:350px;}
    #accordion li {cursor: pointer; color:#AB2326;}
	#accordion li.active{color:#333;}
    #dialog-selectable div {cursor: pointer;}
    .smalltext {font-size: small;}
    table {font-size: small}
    .ui-widget-content {padding:0}

    body.wait *, body.wait {cursor: progress !important;}	
	#filter-result{
		padding:10px; 
		margin-top:20px; 
		display:none;
		background: #fff;
		}	
	#filter-result li{cursor: pointer; color:#AB2326;}
	#sort-nav{margin-top:30px;}
    .fc-event{cursor: pointer;}
	.fc-view-container{background:#fff;}

</style>


<div class="container" id="xcal">

      <div class="row row-offcanvas row-offcanvas-right active">

        <div class="col-xs-12 col-sm-9">
          <p class="pull-right visible-xs">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
          </p>


<!-- USER INFO -->
<div class="panel panel-default" id="user_info" style="display:none; margin-bottom:50px;">
	<!-- Default panel contents -->
	<div id="user-information-heading" class="h3 panel-heading" style="font-weight:bold; margin-bottom:0;"></div>
	<div class="table-responsive">
   	 <table id="user-information-table" class="table table-striped"></table>
	</div>	
</div>
<input type="hidden" id="current_user_cn">

<!-- CALENDAR -->		  
<div id='calendar'></div>
<ol id="selectable" style="list-style-type: none; border: 0; margin-left: -30px"></ol>


<!-- MEETING INFO -->    
<div class="panel panel-default" id="meeting_info" style="display:none; margin-top:30px;">
  <div class="h3 panel-heading" id="meeting_info_heading" style="font-weight:bold; margin-bottom:0;"></div>          
	<div class="table-responsive">
		<table id="calendar_inf_table" class="table table-striped"></table> 
	</div>

</div>    

		  
</div><!--/.col-xs-12.col-sm-9-->


<!-- RIGHT COLUMN -->
        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
		
		<div class="input-group">
		  <input type="text" id="filter_text" class="form-control" placeholder="Name" />
    	  <input id="sQuery" type="hidden" value="<?php if(isset($_GET['q'])){echo $_GET['q'];} ?>" />
		  <span class="input-group-btn">
             <button class="btn btn-success" type="button">Search</button>
          </span>		  
		</div><!-- /input-group -->
        
        <div id="filter-result" class="navbar navbar-default"><ul class="nav nav-pills nav-stacked"></ul></div> 

<div class="input-group" style="margin:30px 0;">
  <input type="text" class="form-control" aria-label="..." id="cal_sort" value="Department" readonly>
  <div class="input-group-btn">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Sort by <span class="caret"></span></button>
        <ul class="dropdown-menu dropdown-menu-right" role="menu">
          <li id="department"><a href="#">Department</a></li>
          <li id="country"><a href="#">Country</a></li>
          <li id="responsibility"><a href="#">Responsibility</a></li>
        </ul>
  </div>
</div>  


          <div id="accordion" class="list-group">

          </div>

        </div><!--/.sidebar-offcanvas-->

      </div><!--/row-->

    </div>
	<!--/.container-->

	{{ HTML::script('xCal/js/offcanvas.js') }}   




@endsection 