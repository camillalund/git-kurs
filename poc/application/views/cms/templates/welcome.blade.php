@layout($layout) 
 
@section('content') 
	<!-- Main jumbotron for a primary marketing message or call to action --> 
	<div class="jumbotron"> 
      <div class="container"> 
        <h1>{{ $welcome }}</h1> 
        <p>{{ $intro }}</p> 
      </div> 
	</div> 
   <div class="container"> 
      <p class="lead">{{ $content }}</p> 
	</div> 
 
   @element('newsflash') 
 
@endsection 
