@layout($layout) 
 
@section('content') 
	<!-- Main jumbotron for a primary marketing message or call to action --> 
	<div class="jumbotron"> 
      <div class="container"> 
        <h1>{{ $subject }}</h1> 
        <p>{{ $preamble }}</p>
        @element('group_add_mylist')
      </div> 
	</div> 
    <div class="container"> 
        <div class="row">
            @if(isset(Current::$content->image1))
			<div class="col-md-8 show-grid">
            @else
			<div class="col-md-12 show-grid">
            @endif
        		{{Current::$content->body}}

			</div>
            @if(isset(Current::$content->image1))
        	@if ($image1 = Current::$content->image1)
    	    <?php
               $media = Media::find($image1);
            ?>
            <div class="col-md-4 show-grid ">
                 {{ CMS::image($media, 800, 800, $subject, array('class' => 'img-responsive')) }}
            </div>
       		 @endif
	@endif
        </div>
        <div class="row">
            <div class="col-md-8">
            <!-- KOLONNE 2 -->

                @element('comments')

            </div>

			<div class="col-md-4">
            <!-- KOLONNE 1 -->
                

                @if(Current::$content->closed_group)
                	@element('groupmembers')
                	@element('users')
                @endif

                @element('groups_mine')
            </div>


            

		</div>
    </div>

@endsection 

