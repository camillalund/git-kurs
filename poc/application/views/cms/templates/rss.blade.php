@query('pages') 
	return Page::order_by('updated_at', 'desc')->take(20)->get(); 
@endquery 
 
<?php 
 
	//Create a feed & configure the details... 
	$feed = RSS::writer($subject); 
	$feed->author()->name($feed_author)->email($feed_email)->up(); 
	$feed->description()->add('text', $feed_description)->up(); 
 
	//Add the pages... 
	foreach($pages as $page) { 
		$entry = $feed->page($page); 
		if (isset($page->summary)) { 
			$entry->content()->add('text', $page->summary); 
		} 
	} 
 
	//Output the feed in the selected format... 
	$feed->$feed_format(); 
?> 
