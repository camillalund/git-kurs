@layout($layout) 
 
@section('content') 

    <div class="container"> 
        <div class="row-fluid">
			<div class="col-md-8">
            <!-- KOLONNE 1 -->
                <h1>{{$subject}}</h1>
                <p><b>{{$preamble}}</b></p>
                {{$body}}
   			</div>
    		<div class="col-md-4">
            <!-- KOLONNE 2 -->
                <div id="msg" style="display: none"></div>
                <h3>Send SMS</h3>
               	<div class="form-group">
                	<label>Melding</label>
                    <textarea class="form-control" rows="3" name="message" id="message"></textarea>
                </div>
                <div class="form-group">
                	<label>Mottakere</label>
                <div id="sms-receivers">
                    @element('people')
                </div>
                	<button type="submit" class="btn btn-default" id="btn-sms">Send SMS <span class="badge" style="display:none">0</span></button>
                </div>

   			</div>
    	</div>
    </div>
@endsection
<?php
/*
/bundles/sites/seeds/defaultsitepermissions.php


if (Security::isAllowed('general.admin')) {
	
	//författare

}

if (Security::isAllowed('admin.read')) {
	//manager
}


//SMS controllern
// req SMS bundle

public function post_send() {
	try {
		Security::assert('admin.read'); //if not - throws
		
		SMS::send($destination, $text)

	}
	catch(Exception $e) {
		//handle error
	}
}
*/
                ?>