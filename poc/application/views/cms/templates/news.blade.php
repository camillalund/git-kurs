@layout($layout) 
 
@section('content') 
<?php

    $dato = new Datetime(Current::$page->created_at);
	$opprettet = $dato->format('d.m.Y');
	$hide_comments = 0;
	if(isset(Current::$content->hide_comments)){
		$hide_comments = Current::$content->hide_comments;
    }

?>
<div class="jumbotron"> 
	<div class="container"> 


        @if(Current::$page->tags)
            @foreach(Current::$page->tags as $tag)
            	<span class="text-info"><i class="glyphicon glyphicon-tag"></i> {{$tag->name}}</span>
            @endforeach
        @endif
    </div> 
</div> 
<div class="container"> 
    <div class="row">
        @if(isset(Current::$content->image1))
	    <div class="col-md-8">
        @else
		<div class="col-md-12">
        @endif
			<p>{{ $body }}</p>
            <div class="clearfix">&nbsp;</div>
            <p ><i>{{$opprettet}} {{Current::$page->author->firstname.' '.Current::$page->author->middlename.' '.Current::$page->author->lastname}}</i></p>
            @if(!$hide_comments)
                 @element('comments')
            @endif
        </div>
	@if(isset(Current::$content->image1))
        @if ($image1 = Current::$content->image1)
    	    <?php
               $media = Media::find($image1);
            ?>
            <div class="col-md-4">
                 {{ CMS::image($media, 800, 800, $subject, array('class' => 'img-responsive')) }}
            </div>
        @endif
	@endif
	</div> 
</div> 

 

 
@endsection 
