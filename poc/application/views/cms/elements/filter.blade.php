@query('alltags')
    $show = 0;
    if(isset(Current::$content->filter_show)){
        $show = Current::$content->filter_show;

    }

	if($show){
		$query = Keyword::all();
		return $query;
    }
	return array();
@endquery

@query('mytags')
    $show = 0;
    if(isset(Current::$content->filter_show)){
        $show = Current::$content->filter_show;
    }

	if($show){
        $user_id = Auth::user()->id;
    
        $query = Subscription::where('user_id','=',$user_id)->get('tag_id');
        $tags = array_group($query, 'tag_id');
        $tags = array_keys($tags);

        return $tags;
    }
	return array();
@endquery

@if($alltags)
<div id="filter">
    <p>Temaer som er grå abonnere du ikke på. Ønsker du å abonnere på disse, kan du gjøre dette på <a href="{{URL::to('my-info')}}">din side <i class="glyphicon glyphicon-user"></i></a>.</p>

    <p>Filtrer nyheter på:</p>
    @foreach($alltags as $tag)
        @if(in_array($tag->id,$mytags))
    		<input type="checkbox" name="filter" value="{{$tag->id}}" checked>{{$tag->name}} &nbsp;

        @elseif($tag->id == Current::$page->content('keys'))
            <input type="checkbox" name="filter" value="{{$tag->id}}" checked>{{$tag->name}} &nbsp;
        @else
            <input type="checkbox" name="filter" value="{{$tag->id}}">{{$tag->name}} &nbsp;

        @endif

    @endforeach
	<div id="pageurl" style="display:none;">{{ URL::Base().Current::$page->uri}}</div>
</div>
@endif