@query('data')

    $user_id = Auth::user()->id;
	$query = Page::join('custom.groupmembers', 'groupmembers.page_id', '=', 'translated_pages.id')
         		->join('cms.comments', 'groupmembers.page_id', '=', 'comments.page_id')
         		->where('groupmembers.user_id','=',$user_id)
          		->order_by('comments.created_at','desc')
				->take(3)
		        ->get();
    if($query){
        $user_ids = array_pluck($query, 'user_id');
        $users = User::where_in('id', $user_ids)->get();
        $users = array_group($users, 'id');
    
        return [
            'comments' => $query,
            'users' => $users
        ];
    }
	return array();

@endquery


@if($data)
    
<h3 class="h3">Mine gruppers siste kommentarer</h3>
<div class="row show-grid">

    <div class="col-md-12">

        @foreach($data['comments'] as $item)
            <div><b>{{$data['users'][$item->user_id][0]->firstname}} {{$data['users'][$item->user_id][0]->middlename}} {{$data['users'][$item->user_id][0]->lastname}} &nbsp; {{$item->created_at}}</b></div>
            <div id="comment{{ $item->id }}" class="commentlist">
                <div class="read-full-comm">Les mer</div>
            	<div class="content">{{nl2br($item->comment)}}</div>
			</div>

            <p><a href="{{ $item->url() }}">{{$item->subject}}</a></p>

        @endforeach



	</div>
</div>
@endif