

				<button type="button" class="btn btn-xs newPageBtn"><span class="glyphicon glyphicon-plus-sign"></span> &nbsp;Opprett ny</button>
  		        <div class="panel panel-default newPostForm">

    				<div class="panel-body">
						<form role="form" method="post" action="{{URL::base().'/ngi/page'}}" enctype="multipart/form-data">
                        	<div class="form-group">
                            	<label for="subject">Tittel</label>
                            	<input type="text" class="form-control" id="subject" name="subject" placeholder="Overskrift">
                          	</div>
                            <div class="form-group">
                            	<label>Dato</label>
                            	<input type="text" class="form-control" id="publish_date" name="publish_date" placeholder="åååå-mm-dd">
                          	</div>
                            <div class="form-group">
                            	<label for="preamble">Kort tekst</label>
                            	<textarea class="form-control" rows="3" name="preamble" id="preamble"></textarea>
                          	</div>
                            <div class="form-group">
                            	<label for="body">Informasjon</label>
                            	<textarea class="form-control" rows="8" name="body" id="body"></textarea>
                          	</div>
                            <div class="form-group">
                            	<label for="body">Bilde</label>
                            	{{Form::file('fil')}}
                    			<input type="hidden" name="folder_id" value="">
                          	</div>

  							<button type="submit" class="btn btn-default">Opprett</button>
                            <input type="hidden" name="parent_id" value="">
                            <input type="hidden" name="pageuri" value="{{URL::base().Current::$page->uri}}">
                            <input type="hidden" name="template_id" value="2">
						</form>
                    </div>
                </div>