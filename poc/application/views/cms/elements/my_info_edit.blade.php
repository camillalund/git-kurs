@query('data')
    $user_id = Auth::user()->id;

	$query = Employee::where('user_id','=',$user_id)->first();
	return $query;


@endquery


<form class="form-horizontal show-grid" role="form" method="post" action="{{URL::to('ngi/employee').'/'.Auth::user()->id}}" enctype="multipart/form-data">
  	<div class="form-group">
    	<label class="col-sm-2 control-label">Navn</label>

    		 @if(Input::get('edit')==1)
    			<div class="col-sm-3">
            		<input type="text" class="form-control" id="inputFirstname" name="firstname" placeholder="Fornavn" value="{{Auth::user()->firstname}}">
                </div>
                <div class="col-sm-3">
    				<input type="text" class="form-control" id="inputMiddlename" name="middlename" placeholder="Mellomnavn" value="{{Auth::user()->middlename}}">
                </div>
                <div class="col-sm-4">
    				<input type="text" class="form-control" id="inputLastname" name="lastname" placeholder="Etternavn" value="{{Auth::user()->lastname}}">
    			</div>
            @else
   				<div class="col-sm-10">
            		<p class="form-control-static">{{Auth::user()->firstname}} {{Auth::user()->middlename}} {{Auth::user()->lastname}}</p>
                </div>
            @endif

  	</div>
    <div class="form-group">
    	<label class="col-sm-2 control-label">E-post</label>
    	<div class="col-sm-10">
            @if(Input::get('edit')==1)
            	<input type="text" class="form-control" id="inputEmail" name="email" placeholder="E-post" value="{{Auth::user()->email}}">
            @else
            	<p class="form-control-static">{{Auth::user()->email}}</p>
            @endif

    	</div>
  	</div>
    <div class="form-group">
    	<label class="col-sm-2 control-label">Telefon</label>
    	<div class="col-sm-10">
            @if(Input::get('edit')==1)
            	<input type="text" class="form-control" id="inputTlf" name="phone" placeholder="Telefon" value="{{Auth::user()->phone}}">
            @else
            	<p class="form-control-static">{{Auth::user()->phone}}</p>
            @endif

    	</div>
  	</div>
    <div class="form-group">
    	<label class="col-sm-2 control-label">Avdeling</label>
    	<div class="col-sm-10">
            @if(Input::get('edit')==1)
                <select class="form-control" id="inputAvd" name="avdeling">
                    <option>Velg din avdeling</option>
                    <option>IT</option>
                    <option>Økonoomi</option>
                    <option>HR</option>
                    <option>Kantine</option>
                    <option>...</option>
                </select>

            @else
            	<p class="form-control-static"></p>
            @endif

    	</div>
  	</div>

    @if($data)
		<input type="hidden" name="_method" value="put">
        <div class="form-group">
            <label class="col-sm-2 control-label">Adresse</label>
            <div class="col-sm-10">
                @if(Input::get('edit')==1)
                    <input type="text" class="form-control" id="inputAdr" name="addresse" placeholder="Adresse" value="{{$data->addresse}}">
                @else
                    <p class="form-control-static">{{$data->addresse}}</p>
                @endif

            </div>
		</div>
        <div class="form-group">
            <label class="col-sm-2 control-label">&nbsp;</label>
            <div class="col-sm-2">
                @if(Input::get('edit')==1)
                    <input type="text" class="form-control" id="inputZip" name="zipcode" placeholder="Postnr" value="{{$data->zipcode}}">
                @else
                    <p class="form-control-static">{{$data->zipcode}}</p>
                @endif
            </div>
            <div class="col-sm-8">
                @if(Input::get('edit')==1)
                    <input type="text" class="form-control" id="inputSted" name="city" placeholder="Sted" value="{{$data->city}}">
                @else
                    <p class="form-control-static">{{$data->city}}</p>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Født</label>
            <div class="col-sm-10">
                @if(Input::get('edit')==1)
                    <input type="text" class="form-control" id="inputBorn" name="birthdate" placeholder="åååå-mm-dd" value="{{$data->birthdate}}">
                @else
                    <p class="form-control-static">{{$data->birthdate}}</p>
                @endif
    
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Ansvarsområder</label>
            <div class="col-sm-10">
                @if(Input::get('edit')==1)
                    <select multiple class="form-control" name="responsibility">
                        <option>Bibliotek</option>
                        <option>Ferie</option>
                        <option>HR</option>
                        <option>Hytter</option>
						<option>Kantine</option>
                        <option>...</option>
                    </select>
                @else
                    <p class="form-control-static"></p>
                @endif
    
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Informasjon</label>
            <div class="col-sm-10">
                @if(Input::get('edit')==1)
                    <textarea class="form-control" name="info">{{$data->info}}</textarea>
                @else
					<p class="form-control-static">{{$data->info}}</p>
                @endif
    
            </div>
        </div>
		<div class="form-group">
            <label class="col-sm-2 control-label">Bilde</label>
            <div class="col-sm-10">
                @if(Input::get('edit')==1)
					{{Form::file('fil')}}
                    <input type="hidden" name="folder_id" value="{{Current::$content->img_folder}}">
                @else
                    @if($data->img)
						<?php
                            $media = Media::find($data->img);
                        ?>
						@if($media)
			 			{{ CMS::image($media, 400, 400, $media->subject, array('class' => 'img-responsive')) }}
						@endif
                    @endif
                @endif

            </div>
        </div>

    @else
		<div class="form-group">
            <label class="col-sm-2 control-label">Adresse</label>
            <div class="col-sm-10">
                @if(Input::get('edit')==1)
                    <input type="text" class="form-control" id="inputAdr" name="addresse" placeholder="Adresse" value="">
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">&nbsp;</label>
            <div class="col-sm-2">
                @if(Input::get('edit')==1)
                    <input type="text" class="form-control" id="inputZip" name="zipcode" placeholder="Postnr" value="">
                @endif
            </div>
            <div class="col-sm-8">
                @if(Input::get('edit')==1)
                    <input type="text" class="form-control" id="inputSted" name="city" placeholder="Sted" value="">
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Født</label>
            <div class="col-sm-10">
                @if(Input::get('edit')==1)
                    <input type="text" class="form-control" id="inputBorn" name="birthdate" placeholder="1980-12-29" value="">
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Informasjon</label>
            <div class="col-sm-10">
                @if(Input::get('edit')==1)
                    <textarea class="form-control" name="info"></textarea>
                @endif
    
            </div>
        </div>
	@endif
        <div class="form-group">
            <label class="col-sm-2 control-label">Bilde</label>
            <div class="col-sm-10">
                @if(Input::get('edit')==1)
					{{Form::file('fil')}}
                    <input type="hidden" name="folder_id" value="{{Current::$content->img_folder}}">
                @endif

            </div>
        </div>
		<div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-10">
                @if(Input::get('edit')==1)
                    <input type="checkbox"> Jeg vil ha viktige meldinger på sms
                @endif

            </div>
        </div>



 	<div class="form-group">
		<div class="col-sm-10 pull-right">
    @if(Input::get('edit')==1)
		<button type="submit" class="btn btn-default">Oppdater mine data</button>
    @else
		<a href="?edit=1" class="btn btn-danger"><span class="glyphicon glyphicon-pencil"></span> Redigere min informasjon</a>
	@endif
  		</div>
    </div>

</form>



