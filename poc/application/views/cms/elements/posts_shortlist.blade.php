@query('data') 


    if(isset(Current::$content->path_posts)){
        $parent = Current::$content->path_posts;
        if ($parent) { 


        	$query = Page::with(array('images','tags','comments', 'likes'))
                            ->where_parent_id($parent);
        	
            $query = $query	->order_by('sort')
                    	    ->take(3)->get()
                            ; 
            return [
                'news' => $query
        	];
        } 
    }

	return [
    	'news' => ""
    ];

@endquery



@if($data['news'])
    <?php
    $parent="";
	if(isset(Current::$content->path_posts)){
        $parent = Current::$content->path_posts;
    }
    ?>
    <input type="hidden" name="list_parentid" class="list_parentid" value="{{$parent}}">
	@foreach($data['news'] as $item)
    <div class="row show-grid newslist">

 <?php
    //dd($item);
    $dato = new Datetime($item->created_at);
	if(isset(Current::$content->chk_date)){
		$opprettet = $dato->format('d.m.Y');
    }else{
		$opprettet = $dato->format('d.m.Y');
    }

    if($item->redirect_new_window == 1){
		$redir = " target='_blank'";

    }else{
		$redir = "";

    }

 	$currkey = @isset(Current::$content->keys);

	$media = "";
 	if($image1 = $item->content('image1')){
    	$media = Media::find($image1);
	}
?>

		<h4 class="newslist col-md-12"><a{{$redir}} href="{{ $item->url() }}">{{ $item->subject }}</a></h4>
      @if($media)
    	<div class="col-md-4 col-sm-4 pull-right">
			 <a{{$redir}} href="{{ $item->url() }}">{{ CMS::image($media, 400, 400, $item->subject, array('class' => 'img-responsive')) }}</a>
    	</div>
      @endif
		<p><i>{{ $opprettet }}</i> {{ $item->content('preamble') }}</p> 
        <p>
            <span class="text-primary"><i class="glyphicon glyphicon-comment"></i> <span class="numcomments">{{count($item->comments)}}</span></span> &nbsp;
			<span class="text-primary"><i class="glyphicon glyphicon-thumbs-up"></i> <span class="numlikes">{{count($item->likes)}}</span></span>
        </p>


    </div>
	@endforeach 


    @endif
