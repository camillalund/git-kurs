@query('data') 
	$parent = Current::$content->path_driftsmeldinger;
	if ($parent) { 
		$query = Page::where_parent_id($parent); 

		return $query	->order_by('sort')
            ->take(5)
            			->get()
            			; 
	} 
	return array(); 
@endquery 

@if($data)
 <?php
    $parent="";
	if(isset(Current::$content->path_driftsmeldinger)){
        $parent = Current::$content->path_driftsmeldinger;
    }
?>

<input type="hidden" name="list_parentid" class="list_parentid" value="{{$parent}}">
<div class="row show-grid">
    <div class="col-md-12">

        @foreach($data as $item)
<?php
    $dato = new Datetime($item->created_at);
	$dato = $dato->format('d.m.Y');
?>
    		<p>{{$dato}} - {{$item->subject}}</p>
        @endforeach
    </div>
</div>

@endif