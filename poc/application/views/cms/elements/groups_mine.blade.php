@query('data') 
    $user_id = Auth::user()->id;
    if(isset(Current::$content->path_groups)){
		//valgt parent
        $parent = Current::$content->path_groups;
	}else{
        //parent til denne siden
		$parent = Current::$page->parent_id;
    }


    $query = Page::join('custom.groupmembers', 'groupmembers.page_id', '=', 'translated_pages.id')
                  ->where('translated_pages.parent_id','=',$parent)
                  ->where('groupmembers.user_id','=',$user_id)
                  ->order_by('subject')
                  ->get();

    return $query;

@endquery 

<h3 class="h3">Mine grupper</h3>
<div class="row show-grid">
    <div class="col-md-12">

@if($data)
	    <ul class="list">
		@foreach($data as $item)

    		<li>
			@if($item->content('closed_group'))
				<span class="glyphicon glyphicon-lock"></span>
    		@else
    			<span class="glyphicon glyphicon-star"></span>
            @endif
    		<a href="{{ $item->url() }}">{{$item->subject}}</a>
            </li>

		@endforeach
    	</ul>
@else

                Ingen grupper
@endif

				<a href="/no/grupper" class="pull-right">Se alle åpne grupper ></a>

	</div>
</div>

