@query('data') 
	$parent = Page::findIdFromPath('/en/news'); 
	if ($parent) { 
		return Page::where_parent_id($parent)->default_order()->get(); 
	} 
	return array(); 
@endquery 
 
<div class="container"> 
	<!-- Example row of columns --> 
	<div class="row"> 
		@foreach($data as $item) 
			<div class="col-md-4"> 
				<h2>{{ $item->subject }}</h2> 
				<p>{{ $item->content('brief') }}</p> 
				<p><a class="btn btn-default" href="{{ $item->url() }}" role="button">View details &raquo;</a></p> 
			</div> 
		@endforeach 
	</div> 
</div> 
