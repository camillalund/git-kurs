@query('data') 
    $parent = Current::$page->content('path_bikechart');

	if ($parent) { 
		return Page::where_parent_id($parent)->default_order()->get(); 
	} 
	return array(); 
@endquery 


<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<div class="row">
    <div class="col-md-12 show-grid">
		<ul class="chartslide">

			@foreach($data as $item) 
                <li>
                    <h3 style="margin-left: 0;">{{ $item->subject }}</h3> 
                    {{ $item->content("chartid") }} 
                    {{ $item->content("googlechartscript") }}
                </li>			
			@endforeach 

        </ul>


	</div> 
</div>

<script type="text/javascript">
    window.onload = function(){
    $('.chartslide').bxSlider({
 	//mode: 'fade',
  	adaptiveHeight: true,
  	speed: 800,
  	auto: false,
  	pager: true
	});
	}

</script> 




