@query('data') 
    $parent = Current::$page->content('path_bikechart');

	if ($parent) { 
		return Page::where_parent_id($parent)->order_by('sort')->take(1)->get(); 
	} 
	return array(); 
@endquery


<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<div class="row show-grid">
    <div class="col-md-12 show-grid bikechart">
		@foreach($data as $item) 

				<h4>{{ $item->subject }}</h4> 
				{{ $item->content("chartid") }} 
				{{ $item->content("googlechartscript") }}

		@endforeach 

	</div> 
</div>
<style>
	.bikechart {
		cursor: pointer; 
        cursor: hand;
    }
</style>
            <script>
				$( document ).ready(function() {
					$(".bikechart").click(function() {
                        window.location.href = "{{CMS::pageUrl(Current::$content->path_bikechart)}}";	
                    })
				});
            </script>
