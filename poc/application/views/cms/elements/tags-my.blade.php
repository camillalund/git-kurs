@query('alltags')

		$query = Keyword::all();
		return $query;

@endquery

@query('mytags')

        $user_id = Auth::user()->id;
    
        $query = Subscription::where('user_id','=',$user_id)->get('tag_id');
        $tags = array_group($query, 'tag_id');
        $tags = array_keys($tags);
    
        return $tags;

@endquery

@if($alltags)
<div class="box" style="margin-top:0;">    
    <h3 class="h3">Tema</h3>

<div class="row show-grid">
    <div class="col-md-12">    
    <p>Temaer som er grå abonnere du ikke på. Ønsker du å abonnere på disse, er det bare å klikke på ønsket tema.</p>
    
    @foreach($alltags as $tag)
        @if(in_array($tag->id,$mytags))
            <span class="label label-primary subscription" id="{{$tag->id}}"><i class="glyphicon glyphicon-tag"></i> {{$tag->name}}</span> 
        @elseif($tag->id == Current::$page->content('keys'))
            <!--<span class="label label-success" id="{{$tag->id}}"><i class="glyphicon glyphicon-tag"></i> {{$tag->name}}</span> -->
        @else
            <span class="label label-default subscription" id="{{$tag->id}}"><i class="glyphicon glyphicon-tag"></i> {{$tag->name}}</span> 
        @endif

    @endforeach

</div>
</div>                
</div>

@endif