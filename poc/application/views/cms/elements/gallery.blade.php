@query('data')

    if(isset(Current::$content->folder_gallery)){
        $parent = Current::$content->folder_gallery;
        if ($parent) { 
            $query = Media::where('folder_id','=',$parent);

            return $query	->where('image','=',1)
    						->order_by('created_at','desc')
                            ->get()
                            ; 
        } 
    }
	return array();    

@endquery

@if($data)
<?php
//dd($data);
    ?>

<div class="row">
    <div class="col-md-12">
        <h3 class="h3" style="margin-bottom:0;">Galleri</h3>
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="border: 7px solid #000;">
  		<!-- Indicators -->
  		<ol class="carousel-indicators">

   		 @foreach($data as $index=>$img)
			@if($index == 0)
				<li data-target="#carousel-example-generic" data-slide-to="{{ $index }}" class="active"></li>
			@else
				<li data-target="#carousel-example-generic" data-slide-to="{{ $index }}"></li>
			@endif    
   		 @endforeach
  		</ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

	@foreach($data as $index=>$img)
		@if($index == 0)
			<div class="item active">
		@else
			<div class="item">
		@endif

      {{ CMS::image($img, 500, 500, $img->subject, array('class' => 'img-responsive', 'title' => $img->subject)) }}
      <div class="carousel-caption">
			{{$img->description}}
      </div>
    </div>

	@endforeach

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


</div>
</div>      



<!--
<div class="row">
    <div class="col-md-12">
        <h3 class="h3">Galleri</h3>
        <ul class="gallery">
        @foreach($data as $index=>$img)
    
            <div class="col-md-3">
                <li>{{ CMS::image($img, 500, 500, $img->subject, array('class' => 'img-responsive', 'title' => $img->subject)) }}</li>
            </div>
    
        @endforeach
        </ul>
	</div>
</div>
-->                    

@endif