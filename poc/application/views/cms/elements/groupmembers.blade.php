@query('data') 
    if(isset(Current::$content->path_groups)){
        $parent = Current::$content->path_groups;
        if ($parent) { 
//hvis de tskal begrensen til en foreldergruppe

        } 
    }

	$page_id = Current::$page->id;
    $query = User::join('custom.groupmembers', 'groupmembers.user_id', '=', 'users.id')
        ->where('groupmembers.page_id','=',$page_id)
        ->order_by('admin','desc')
        ->order_by('firstname')
        ->get();
    return ($query);

@endquery 

@query('admin')
    $user_id = Auth::user()->id;
    $query = Groupmember::where('user_id','=',$user_id)
        				->where('page_id','=',Current::$page->id)
        				->where('admin','=', 1)
        				->first();
	return $query;

@endquery    

@if($data)
<div class="row">
    <div class="col-md-12">

		<h3 class="h3">Medlemmer</h3>

	    <ul class="list">
		@foreach($data as $item)

    		<li id="member{{$item->user_id}}">
				@if($item->admin)
					<span class="glyphicon glyphicon-eye-open"></span> 
    				{{$item->firstname}} {{$item->middlename}} {{$item->lastname}}
    			@else
                    @if($admin)
                    	<span class="glyphicon glyphicon-remove-circle remove-member" title="Fjern medlem"></span>
                    	<input type="hidden" name="userid" value="{{$item->user_id}}">

                    @else
						<span class="glyphicon glyphicon-user"></span>
                    @endif
    				{{$item->firstname}} {{$item->middlename}} {{$item->lastname}}

                @endif

			</li>

		@endforeach
		</ul>
        <input type="hidden" name="pageid" id="pageid" value="{{Current::$page->id}}">
	</div>
</div>
@endif

