
@query('data')
	$parent = Current::$page->content('path_calendar');

        $output = Page::where_parent_id($parent)
                        ->default_order()
                        ->get();
    	return $output;



	return array();
@endquery






@if($data)


<?php

    $json = "";
    $eventsArray = array();

    foreach ($data as $item) {



        $event = new StdClass();
        $event->title = $item->subject;

        if ($item->content('content')) {
            $event->description = $item->content('content');
        }
        else {
            $event->description = "";
        }

        $date = new Datetime($item->content('publish_date'));
        $event->date = $date->format('Y-m-d H:i:s');
        $event->endDate = $item->content('end_date');
        $event->type = "event";
        $event->url = CMS::pageUrl((int)$item->id);

        $eventsArray[] = $event;


        if ($event->endDate) {
            $numOfDays = floor((strtotime($event->endDate) - strtotime($event->date))/86400);
            for ($i=1; $i<$numOfDays+1; $i++) {
                $event2 = clone $event;
                $event2->date = date('Y-m-d H:i:s', strtotime($event->date)+86400*$i); //2014-09-10 00:00:00

                $eventsArray[] = $event2;
            }
        }

    }

	$json = json_encode($eventsArray);

?>

{{ HTML::script('js/jquery.eventCalendar.js') }} 
  {{ HTML::script('js/jquery.eventCalendarTwo.js') }}  

<div class="row-fluid">
    <div class="col-md-12">


    <div id="event-calendar">

        <div id="eventCalendarHumanDateTwo"></div>

     <div id="eventCalendarHumanDate"></div>


    </div>

</div>
        </div>

</div>
    <script type="text/javascript">

            $(document).ready(function() {
               // console.log("hello");

                $("#eventCalendarHumanDate").eventCalendar({
                    jsonData: {{$json}},
                    jsonDateFormat: 'human',
                 	showDescription: false
                });

				$("#eventCalendarHumanDateTwo").eventCalendar({
                    jsonData: {{$json}},
                    jsonDateFormat: 'human',
                 	showDescription: false
                });

            });
        </script>

@endif

