<?php
	$instance = "symfonidemodk";
	$username = 'oleg.andrushko@symfoni.com';
	$password = 'oleg123';
	$table = 'kb_knowledge';
	$query = "ORDERBYDESCsys_updated_on&sysparm_record_count=500&displayvalue=true";

    $json = servicenow::pullRecords($instance, $username, $password, $table, $query);

	//dd($json["records"]);

	$articles = array();
	
	foreach ($json["records"] as &$record) {
	
		if (!array_key_exists($record["topic"], $articles)) {
			$articles[$record["topic"]] = array(); 
		}
	
		array_push($articles[$record["topic"]], $record);
    }
    $topics = array_keys($articles);


?>

<h3 class="h3 redhead">Kunnskapsbase (FAQ)</h3>
<div class="row show-grid">
    <div class="col-md-12">
		@foreach($topics as $topic)
		<li class="kb-li">
			{{$topic}}
			@foreach($articles[$topic] as $article)
				<ul class="kb-ul">
            		<a href="#" data-featherlight="<p>{{base64_encode($article["text"])}}</p>">
            			{{$article["short_description"]}}
            		</a>
            	</ul>
			@endforeach
		</li>
    	@endforeach
    </div>
</div>

<script type="text/javascript">

	$(document).ready(function() {
  		$('.kb-ul').hide();
    	$('.kb-li').click(function() {
        	$(this).siblings('.kb-li').find('ul').slideUp();
        	$(this).find('ul').slideToggle();
    	});
    	
    	$(".kb-ul").click(function(e) {
        	e.stopPropagation();
   		});
   		
   		
   		
   		$('.kb-ul a').featherlight(
   			
   		
   		
   		
   			{
   				afterOpen: function(event){
    				var content = $.featherlight.current().$content;
    				//console.log($(content).html());
    				var newContent = Base64.decode($(content).html());
    			
    			
    				$(content).html(newContent);
    				return true; // do not prevent lightbox from opening
			}
   		
   		
   		});
  		
  		var Base64 = {


    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",


    encode: function(input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },


    decode: function(input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    _utf8_encode: function(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    _utf8_decode: function(utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}    




  		
  		
  		
	});
	


</script>
<link href="//cdn.rawgit.com/noelboss/featherlight/1.0.3/release/featherlight.min.css" type="text/css" rel="stylesheet" title="Featherlight Styles" />
<script src="//cdn.rawgit.com/noelboss/featherlight/1.0.3/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>