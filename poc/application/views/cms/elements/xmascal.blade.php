@query('data') 
    if(isset(Current::$content->path_xmascal)){
        $parent = Current::$content->path_xmascal;
        if ($parent) { 
            $query = Page::with('images')
                            ->where_parent_id($parent); 
    
            return $query	->order_by('sort')
                            ->get()
                            ; 
        } 
    }
	return array(); 
@endquery 


@if($data)
	<div class="row-fluid"> 
    	<div class="col-md-12"> 
    		<h3>Julekalender</h3>
    	</div>
    @foreach($data as $index=>$item)
    	@if($index<9)
			<div class="col-xs-2 show-grid"> 
				<a class="btn btn-default" href="/no/sannkassen/julekalender?id={{$item->id}}" role="button">&nbsp;{{$index+1}}&nbsp;</a>
			</div> 
		@else
			<div class="col-xs-2 show-grid"> 
				<a class="btn btn-danger" href="/no/sannkassen/julekalender?id={{$item->id}}" role="button">{{$index+1}}</a>
			</div> 
        @endif
    @endforeach

	</div> 

@endif