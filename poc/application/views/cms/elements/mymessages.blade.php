@query('data')
    if(isset(Current::$content->path_mymessages)){
        $parent = Current::$content->path_mymessages;
        if ($parent) { 
            $query = Page::where_parent_id($parent); 
    
            return $query	->order_by('sort')
                            ->get()
                            ; 
        } 
    }
	return array(); 
@endquery 

@if($data)

<h3 class="h3 redhead">Mine meldinger <span class="badge pull-right"> {{count($data)}}</span></h3>    
<div class="row show-grid">
    <div class="col-md-12">
        @foreach($data as $item)
    		<p>12.12.2014 {{$item->subject}}</p>
        @endforeach
    </div>
</div>

@endif