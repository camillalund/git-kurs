@query('data') 
    if(isset(Current::$content->path_birthdays)){
        $parent = Current::$content->path_birthdays;
        if ($parent) { 
            $query = Page::with('images')
                            ->where_parent_id($parent); 
    
            return $query	->order_by('sort')
                            ->get()
                            ; 
        } 
    }
	return array(); 
@endquery 

<div class="row-fluid">
    <h3 class="h3">Jubilanter</h3>


@if($data)

    @foreach($data as $birthday)
<div class="col-md-4">

        @if ($image1 = $birthday->content('image1'))
            <?php
                $media = Media::find($image1);
            ?>
            @if($media)
            	{{ CMS::image($media, 200, 300, $birthday->subject, array('class' => 'img-responsive img-circle', 'title' => $birthday->content('preamble'))) }}
        	@endif
        @endif
	
    <p style="text-align:center;">{{$birthday->subject}}</p>
</div>
    @endforeach

@else
    <p>Ingen jubilanter i dag</p>
@endif

</div>


