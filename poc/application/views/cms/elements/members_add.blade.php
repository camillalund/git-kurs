@query('users')

    //$repo = new Symfoni\Sites\Cms\Repositories\UserRepositories;
	//return $repo->getAll();

    return User::order_by('firstname')
        ->get(array('id','firstname','middlename','lastname'));

@endquery

@query('admin')
    $user_id = Auth::user()->id;
    $query = Groupmember::where('user_id','=',$user_id)
        				->where('page_id','=',Current::$page->id)
        				->where('admin','=', 1)
        				->first();
	return $query;

@endquery    

@if($users && $admin)
<div class="row">
    <div class="col-md-12 show-grid">
    <h4>Legg til medlemmer</h4>
    <form method="post" action="{{URL::to('/ngi/members/add/').Current::$page->id}}">
        <select multiple class="form-control" name="users[]">

        @foreach($users as $user)
            <option value="{{ $user->id }}">{{ $user->firstname }} {{ $user->middlename }} {{ $user->lastname }}</option>
        @endforeach
    
        </select>
        <button type="submit" class="btn btn-default">Legg til</button>
		<input type="hidden" name="pageuri" value="{{URL::base().Current::$page->uri}}">
    </form>
    </div>
</div>
@endif