@query('users')

    return User::order_by('firstname')
        ->get(array('id','firstname','middlename','lastname'));

@endquery



@if($users)
<div class="row">
    <div class="col-md-12 show-grid">

        <select multiple class="form-control" name="users[]">

        @foreach($users as $user)
            <option value="{{ $user->id }}">{{ $user->firstname }} {{ $user->middlename }} {{ $user->lastname }}</option>
        @endforeach
    
        </select>

    </div>
</div>
@endif