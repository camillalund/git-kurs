@query('data') 
    if(isset(Current::$content->path_groups)){
        $parent = Current::$content->path_groups;
        if ($parent) { 
            $query = Page::with('images')
                            ->where_parent_id($parent)
                			->where_content('closed_group','!=','1') ;
    
            return $query	->order_by('created_at','desc')
                            ->paginate(10)
                            ; 
        } 
    }
	return array(); 
@endquery 

@foreach($data->results as $item)
    <div class="row">
<?php

    if($item->redirect_new_window == 1){
		$redir = " target='_blank'";

    }else{
		$redir = "";

    }
?>

        @if ($image1 = $item->content('image1'))
        <?php
           	$media = Media::find($image1);
        ?>
    	<div class="col-md-3 col-sm-3">
			 <a{{$redir}} href="{{ $item->url() }}" class="thumbnail">{{ CMS::image($media, 400, 400, $item->subject, array('class' => 'news-image')) }}</a>
    	</div>
		<div class="col-md-9 col-sm-9"> 
        @else
		<div class="col-md-12"> 
        @endif
			<h2 class="newslist"><a{{$redir}} href="{{ $item->url() }}">{{ $item->subject }}</a>
@if($item->content('closed_group'))
				<span class="glyphicon glyphicon-lock"></span>
            @endif
                </h2>
			<p>{{ $item->content('preamble') }}</p> 
		</div>
    </div>
	@endforeach 

    <div style="text-align: center">
	<ul class="pagination">
<?php
	$url = URL::base().Current::$page->uri;

	$ant = round($data->total/10) ;

    if($data->last > 1){

        if($data->page>1){
            $prev = $data->page -1;
			echo "<li class='previous_page'><a href='$url?page=$prev'>< Forrige</a></li>";
        }else{
            echo "<li class='previous_page disabled'><a href=''>< Forrige</a></li>";
        }

		for($i=1;$i<=$ant;$i++){
            if($data->page == $i){
				echo "<li class='active'><a href='$url?page=$i'>$i</a></li>";
            }else{
            	echo "<li><a href='$url?page=$i'>$i</a></li>";
            }
        }
        if($data->last == $data->page){
			echo "<li class='previous_page disabled'><a href=''>Neste ></a></li>";
        }else{
            $next = $data->page +1;
            echo "<li class='previous_page'><a href='$url?page=$next'>Neste ></a></li>";
        }
    }

?>
	</ul>
    </div>
