@query('data') 


    if(isset(Current::$content->path_posts)){
        $parent = Current::$content->path_posts;
        if ($parent) { 
			$keys = "";
            $keyid = @isset(Current::$content->keys);
            if($keyid!=""){
                $key = Keyword::find((int)$keyid, 'name');

                if($key){
					$keys = [(int)$keyid];
                }

            }

        	$query = Page::with(array('images','tags', 'author','comments', 'likes'))
                            ->where_parent_id($parent);
        	if( $keys){
            	$query = $query->where_keywords($keys);
            }

            $query = $query	->order_by('sort')
                    	    ->paginate(10)
                            ; 
            return [
                'news' => $query
        	];
        } 
    }

	return [
    	'news' => ""
    ];

@endquery



@if($data['news'])
	@foreach($data['news']->results as $item)
    <div class="row show-grid">

 <?php
    //dd($data['news']);
    $dato = new Datetime($item->created_at);
	if(isset(Current::$content->chk_date)){
		$opprettet = $dato->format('d.m.Y');
    }else{
		$opprettet = $dato->format('d.m.Y');;
    }

    if($item->redirect_new_window == 1){
		$redir = " target='_blank'";

    }else{
		$redir = "";

    }

 	$currkey = @isset(Current::$content->keys);
?>

        @if ($image1 = $item->content('image1'))
        <?php
           	$media = Media::find($image1);
        ?>
    	<div class="col-md-3 col-sm-3">
			 <a{{$redir}} href="{{ $item->url() }}">{{ CMS::image($media, 400, 400, $item->subject, array('class' => 'img-responsive')) }}</a>
    	</div>
		<div class="col-md-9 col-sm-9"> 
        @else
		<div class="col-md-12"> 
        @endif
			<h2 class="newslist"><a{{$redir}} href="{{ $item->url() }}">{{ $item->subject }}</a></h2>
			<p>{{ $item->content('preamble') }}<br>
                <i>{{ $opprettet }} {{$item->author->firstname.' '.$item->author->middlename.' '.$item->author->lastname}}</i>
            </p> 
            <p>
                <span class="text-primary"><i class="glyphicon glyphicon-comment"></i> <span class="numcomments">{{count($item->comments)}}</span></span> &nbsp;
				<span class="text-primary"><i class="glyphicon glyphicon-thumbs-up"></i> <span class="numlikes">{{count($item->likes)}}</span></span>
            </p>

		</div>
    </div>
	@endforeach 

    <div style="text-align: center">
	<ul class="pagination">
<?php
	$url = URL::base().Current::$page->uri;

	$ant = round($data['news']->total/10) ;

    if($data['news']->last > 1){

        if($data['news']->page>1){
            $prev = $data['news']->page -1;
			echo "<li class='previous_page'><a href='$url?page=$prev'>< Forrige</a></li>";
        }else{
            echo "<li class='previous_page disabled'><a href=''>< Forrige</a></li>";
        }

		for($i=1;$i<=$ant;$i++){
            if($data['news']->page == $i){
				echo "<li class='active'><a href='$url?page=$i'>$i</a></li>";
            }else{
            	echo "<li><a href='$url?page=$i'>$i</a></li>";
            }
        }
        if($data['news']->last == $data['news']->page){
			echo "<li class='previous_page disabled'><a href=''>Neste ></a></li>";
        }else{
            $next = $data['news']->page +1;
            echo "<li class='previous_page'><a href='$url?page=$next'>Neste ></a></li>";
        }
    }

?>
	</ul>
    </div>
    @endif
