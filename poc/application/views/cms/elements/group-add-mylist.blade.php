@query('data')
    $page_id = Current::$page->id;
	$user_id = Auth::user()->id;
	$query = Groupmember::where('page_id','=',$page_id)
        			->where('user_id','=',$user_id)
        			->first();

	return $query;
@endquery

@if($data)
    @if($data->admin)
		<button type="button" class="btn btn-danger btn-lg" disabled="disabled"><span class="glyphicon glyphicon-eye-open"></span> Administrator</button>
    @else
		<button type="button" id="add2group" class="btn btn-danger btn-lg" data-pageid="{{Current::$page->id}}"><span class="glyphicon glyphicon-star"></span> Fjern fra mine grupper</button>
    @endif
@else
	<button type="button" id="add2group" class="btn btn-danger btn-lg" data-pageid="{{Current::$page->id}}"><span class="glyphicon glyphicon-star-empty"></span> Legg til i mine grupper</button>
@endif