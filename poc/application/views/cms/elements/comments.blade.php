

	<div class="row-fluid">
        <div class="col-md-12">
        	<div class="pull-left badge"><span class="glyphicon glyphicon-comment"></span> <span id="numpagecomments">0</span> innlegg</div>
            <form class="pull-right" role="form" method="post" id="addLikeForm{{Current::$page->id}}" action="{{URL::to('/cms/pages/'.Current::$page->id.'/like')}}">
                <button type="button" onclick="addLike({{Current::$page->id}});" class="btn btn-danger pull-right">Like 
        			<span class="glyphicon glyphicon-thumbs-up"></span> &nbsp;
                        <span class="badge pull-right likenum" id="likenum{{Current::$page->id}}" data-pageid="{{Current::$page->id}}">0</span>
                </button>
            </form>
		</div>
    </div>

    <div class="row-fluid">
        <div class="col-md-12 show-grid">
			<h3>Nytt innlegg</h3>
            <form role="form" method="post" id="addComForm{{Current::$page->id}}" action="{{URL::to('/cms/pages/'.Current::$page->id.'/comments')}}" 
                onsubmit="addComment({{Current::$page->id}},'addComForm',1); return false">
				<div class="form-group">
                    <textarea class="form-control" rows="3" name="comment" id="comment"></textarea>
                </div>
                <button type="submit" class="btn btn-default">Send inn kommentar</button>
            </form>

        </div>
    </div>

	<div class="row-fluid pagecomments" id="com{{Current::$page->id}}" data-pageid="{{Current::$page->id}}"></div>
