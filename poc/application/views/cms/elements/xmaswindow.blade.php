@query('data') 

    $windowid = Input::get('id');
	if (isset($windowid)){

		$query = Page::with('images')
             		->where_id($windowid); 

        return $query->first(); 
    }else{

        if(isset(Current::$content->path_xmascal)){
            $parent = Current::$content->path_xmascal;
            if ($parent) { 
                $query = Page::with('images')
                                ->where_parent_id($parent); 
        
                return $query	->order_by('sort')
                                ->first(); 
            } 
        }
    }
	return array(); 
@endquery 

<div class="row-fluid">
    <div class="col-md-12">
@if($data)

<h3>{{$data->subject}}<h3>

@if ($image1 = $data->content('image1'))
    <?php
        $media = Media::find($image1);
    ?>
    @if($media)
        {{ CMS::image($media, 200, 300, $data->subject, array('class' => 'img-responsive', 'title' => $data->subject)) }}
    @endif

@endif


@endif
	</div>
</div>

