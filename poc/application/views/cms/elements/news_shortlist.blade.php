@query('data') 
    $user_id = Auth::user()->id;

	$query = Subscription::where('user_id','=',$user_id)->get('tag_id');

	if($query){
        $tags = array_group($query, 'tag_id');
        $tags = array_keys($tags);
    }else{
        $tags = array();
    }

    if(isset(Current::$content->pagepath_articles)){
        $parent = Current::$content->pagepath_articles;
        if ($parent) { 
            $keys = $tags;

            $keyid = @isset(Current::$content->keys);
            if($keyid!=""){
                $key = Keyword::find((int)$keyid, 'name');

                if($key){
                   	if($tags){
						array_push($keys,(int)$keyid);
                    /*}else{
						$keys = [(int)$keyid];*/
                    }
                }

            }

        	$query = Page::with(array('images','tags'))
                            ->where_parent_id($parent);
                			//->where_keyword((int)$keyid)
        	if( $keys){
            	$query = $query->where_keywords($keys);
            }

            $query = $query	->order_by('sort')
                    	    ->take(4)->get()
                            ; 
            return [
                'news' => $query,
                'mytags' => $tags
        	];
        } 
    }

	return [
    	'news' => ""
    ];
@endquery



@if($data['news'])
    <?php
    $parent="";
	if(isset(Current::$content->pagepath_articles)){
        $parent = Current::$content->pagepath_articles;
    }
    ?>
    <input type="hidden" name="list_parentid" class="list_parentid" value="{{$parent}}">
	@foreach($data['news'] as $item)
    <div class="row show-grid newslist">
<?php


    $dato = new Datetime($item->created_at);
	if(isset(Current::$content->chk_date)){
		$opprettet = $dato->format('d.m.Y');
    }else{
		$opprettet = $dato->format('d.m.Y');
    }

    if($item->redirect_new_window == 1){
		$redir = " target='_blank'";

    }else{
		$redir = "";

    }

 	$currkey = @isset(Current::$content->keys);
$media = "";
 	if ($image1 = $item->content('image1')){
		$media = Media::find($image1);
    }
?>
		<h4 class="newslist col-md-12"><a{{$redir}} href="{{ $item->url() }}">{{ $item->subject }}</a></h4>
      @if ($media)
        <div class="col-md-4 col-sm-4 pull-right">
			 <a{{$redir}} href="{{ $item->url() }}">{{ CMS::image($media, 400, 400, $item->subject, array('class' => 'img-responsive')) }}</a>
    	</div>
      @endif
		<p><i>{{ $opprettet }}</i> {{ $item->content('preamble') }}</p> 
        <p>
            @foreach($item->tags as $tag)
                @if(in_array($tag->id,$data['mytags']))
					<span class="text-primary"><i class="glyphicon glyphicon-tag"></i> {{$tag->name}}</span>
                @elseif($tag->id == $currkey)
					<span class="text-primary"><i class="glyphicon glyphicon-tag"></i> {{$tag->name}}</span> 
                @else
                    <span class="text-muted"><i class="glyphicon glyphicon-tag"></i> {{$tag->name}}</span> 
                @endif
            @endforeach
        </p>

    </div>
	@endforeach 


    @endif
