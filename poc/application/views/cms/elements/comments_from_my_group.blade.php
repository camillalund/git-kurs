@query('data')
    $user_id = Auth::user()->id;
	$query = Page::join('custom.groupmembers', 'groupmembers.page_id', '=', 'translated_pages.id')
         		->join('cms.comments', 'groupmembers.page_id', '=', 'comments.page_id')
         		->where('groupmembers.user_id','=',$user_id)
          		->order_by('comments.created_at','desc')
		        ->get();
//dd($query);
    return ($query);

@endquery


<div class="row">
    <div class="col-md-12">
		<div class="panel panel-danger">
			<div class="panel-heading">Mine gruppers innlegg/kommentarer</div>
    			<div class="panel-body">
@if($data)

		@foreach($data as $item)

    		<p>
    		{{$item->created_at}}<br>
			{{$item->comment}}<br>
    		<a href="{{ $item->url() }}">{{$item->subject}}</a>
            </p>

		@endforeach


@endif
				</div>

		</div>
	</div>
</div>
