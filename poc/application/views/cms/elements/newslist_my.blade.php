@query('data') 
    $user_id = Auth::user()->id;

	$query = Subscription::where('user_id','=',$user_id)->get('tag_id');

	if($query){
        $tags = array_group($query, 'tag_id');
        $tags = array_keys($tags);
    }else{
        $tags = array();
    }

    if(isset(Current::$content->pagepath_articles)){
        $parent = Current::$content->pagepath_articles;
        if ($parent) { 
            $keys = $tags;

            $keyid = @isset(Current::$content->keys);
            if($keyid!=""){
                $key = Keyword::find((int)$keyid, 'name');

                if($key){
                   	if($tags){
						array_push($keys,(int)$keyid);
                    /*}else{
						$keys = [(int)$keyid];*/
                    }
                }

            }

        	$query = Page::with(array('images','tags'))
                            ->where_parent_id($parent);
                			//->where_keyword((int)$keyid)
        	if( $keys){
            	$query = $query->where_keywords($keys);
            }

            $query = $query	->order_by('sort')
                    	    ->paginate(10)
                            ; 
            return [
                'news' => $query,
                'mytags' => $tags
        	];
        } 
    }

	return [
    	'news' => ""
    ];
@endquery



@if($data['news'])
	@foreach($data['news']->results as $item)
    <div class="row show-grid">
<?php


    $dato = new Datetime($item->created_at);
	if(isset(Current::$content->chk_date)){
		$opprettet = $dato->format('d.m.Y');
    }else{
		$opprettet = "";
    }

    if($item->redirect_new_window == 1){
		$redir = " target='_blank'";

    }else{
		$redir = "";

    }

 	$currkey = @isset(Current::$content->keys);
?>

        @if ($image1 = $item->content('image1'))
        <?php
           	$media = Media::find($image1);
        ?>
    	<div class="col-md-3 col-sm-3">
			 <a{{$redir}} href="{{ $item->url() }}">{{ CMS::image($media, 400, 400, $item->subject, array('class' => 'img-responsive')) }}</a>
    	</div>
		<div class="col-md-9 col-sm-9"> 
        @else
		<div class="col-md-12"> 
        @endif
			<h2 class="newslist"><a{{$redir}} href="{{ $item->url() }}">{{ $item->subject }}</a></h2>
            {{ $opprettet }}
			<p>{{ $item->content('preamble') }}</p> 
                <p>
            @foreach($item->tags as $tag)
                @if(in_array($tag->id,$data['mytags']))
					<span class="label label-primary"><i class="glyphicon glyphicon-tag"></i> {{$tag->name}}</span>
                @elseif($tag->id == $currkey)
					<span class="label label-primary"><i class="glyphicon glyphicon-tag"></i> {{$tag->name}}</span> 
                @else
                    <span class="label label-default"><i class="glyphicon glyphicon-tag"></i> {{$tag->name}}</span> 
                @endif
            @endforeach
                    </p>
		</div>
    </div>
	@endforeach 

    <div style="text-align: center">
	<ul class="pagination">
<?php
	$url = URL::base().Current::$page->uri;

	$ant = round($data['news']->total/10) ;

    if($data['news']->last > 1){

        if($data['news']->page>1){
            $prev = $data['news']->page -1;
			echo "<li class='previous_page'><a href='$url?page=$prev'>< Forrige</a></li>";
        }else{
            echo "<li class='previous_page disabled'><a href=''>< Forrige</a></li>";
        }

		for($i=1;$i<=$ant;$i++){
            if($data['news']->page == $i){
				echo "<li class='active'><a href='$url?page=$i'>$i</a></li>";
            }else{
            	echo "<li><a href='$url?page=$i'>$i</a></li>";
            }
        }
        if($data->last == $data->page){
			echo "<li class='previous_page disabled'><a href=''>Neste ></a></li>";
        }else{
            $next = $data['news']->page +1;
            echo "<li class='previous_page'><a href='$url?page=$next'>Neste ></a></li>";
        }
    }

?>
	</ul>
    </div>
    @endif
