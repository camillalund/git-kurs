@query('data') 


    if(isset(Current::$content->path_posts)){
        $parent = Current::$content->path_posts;
        if ($parent) { 
			$keys = "";
            $keyid = @isset(Current::$content->keys);
            if($keyid!=""){
                $key = Keyword::find((int)$keyid, 'name');

                if($key){
					$keys = [(int)$keyid];
                }

            }

        	$query = Page::with(array('images','tags', 'author'))
                            ->where_parent_id($parent);
        	if( $keys){
            	$query = $query->where_keywords($keys);
            }

            $query = $query	->order_by('sort')
                    	    ->take(3)
                            ; 
            return [
                'news' => $query
        	];
        } 
    }

	return [
    	'news' => ""
    ];

@endquery



@if($data['news'])
	@foreach($data['news'] as $item)
    <div class="row show-grid">

 <?php
    //dd($data['news']);
    $dato = new Datetime($item->created_at);
	if(isset(Current::$content->chk_date)){
		$opprettet = $dato->format('d.m.Y');
    }else{
		$opprettet = $dato->format('d.m.Y');;
    }

    if($item->redirect_new_window == 1){
		$redir = " target='_blank'";

    }else{
		$redir = "";

    }

 	$currkey = @isset(Current::$content->keys);
?>

        @if ($image1 = $item->content('image1'))
        <?php
           	$media = Media::find($image1);
        ?>
    	<div class="col-md-3 col-sm-3">
			 <a{{$redir}} href="{{ $item->url() }}">{{ CMS::image($media, 400, 400, $item->subject, array('class' => 'img-responsive')) }}</a>
    	</div>
		<div class="col-md-9 col-sm-9"> 
        @else
		<div class="col-md-12"> 
        @endif
			<h2 class="newslist"><a{{$redir}} href="{{ $item->url() }}">{{ $item->subject }}</a></h2>
			<p><i>{{ $opprettet }}</i> {{ $item->content('preamble') }}<br>
                <i>{{ $opprettet }} {{$item->author->firstname.' '.$item->author->middlename.' '.$item->author->lastname}}</i>
            </p> 

		</div>
    </div>
	@endforeach 

@endif