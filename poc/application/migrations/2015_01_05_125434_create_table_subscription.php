<?php

class Create_Table_Subscription {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('custom.subscriptions', function($table) {
			$table->increments('id');
			$table->integer('tag_id');
			$table->integer('user_id');
			$table->timestamps();
				  
			$table->foreign('tag_id')
				  ->references('id')->on('cms.keywords')
				  ->on_delete('cascade');
				 
			$table->foreign('user_id')
				  ->references('id')->on('cms.users')
				  ->on_delete('cascade');

		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//Sletter tabellen
		Schema::drop('custom.subscriptions');
	}

}