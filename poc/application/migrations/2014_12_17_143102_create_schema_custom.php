<?php

class Create_Schema_Custom {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//Create custom schema
		DB::query('CREATE SCHEMA custom AUTHORIZATION devngi');
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//Drop custom schema
		DB::query('DROP SCHEMA IF EXISTS custom RESTRICT');
	}

}