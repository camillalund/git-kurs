<?php

class Create_Table_Groupmembers {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('custom.groupmembers', function($table) {
			$table->increments('id');
			$table->integer('page_id');
			$table->integer('user_id');
			$table->integer('admin')->nullable();
			$table->timestamps();
				  
			$table->foreign('page_id')
				  ->references('id')->on('cms.pages')
				  ->on_delete('cascade');
				 
			$table->foreign('user_id')
				  ->references('id')->on('cms.users')
				  ->on_delete('cascade');

		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//Sletter tabellen
		Schema::drop('custom.groupmembers');
	}

}