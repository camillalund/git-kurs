<?php

class Create_Table_Employees {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('custom.employees', function($table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('addresse')->nullable();
			$table->string('zipcode')->nullable();
			$table->string('city')->nullable();
			$table->string('position')->nullable();
			$table->string('department')->nullable();
			$table->string('birthdate')->nullable();
			$table->text('info')->nullable();
			$table->string('img')->nullable();
			$table->timestamps();
				 
			$table->foreign('user_id')
				  ->references('id')->on('cms.users')
				  ->on_delete('cascade');

		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('custom.employees');
	}

}