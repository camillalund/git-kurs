<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Default Authentication Driver
	|--------------------------------------------------------------------------
	|
	| Laravel uses a flexible driver-based system to handle authentication.
	| You are free to register your own drivers using the Auth::extend
	| method. Of course, a few great drivers are provided out of
	| box to handle basic authentication simply and easily.
	|
	| Drivers: 'fluent', 'eloquent', 'cas', 'fast'.
	|
	*/

	'driver' => 'cas',

	/*
	|--------------------------------------------------------------------------
	| Authentication Username
	|--------------------------------------------------------------------------
	|
	| Here you may specify the database column that should be considered the
	| "username" for your users. Typically, this will either be "username"
	| or "email". Of course, you're free to change the value to anything.
	|
	*/

	'username' => 'username',

	/*
	|--------------------------------------------------------------------------
	| Authentication Password
	|--------------------------------------------------------------------------
	|
	| Here you may specify the database column that should be considered the
	| "password" for your users. Typically, this will be "password" but, 
	| again, you're free to change the value to anything you see fit.
	|
	*/

	'password' => 'password',

	/*
	|--------------------------------------------------------------------------
	| Authentication Model
	|--------------------------------------------------------------------------
	|
	| When using the "eloquent" authentication driver, you may specify the
	| model that should be considered the "User" model. This model will
	| be used to authenticate and load the users of your application.
	|
	*/

	'model' => 'User',

	/*
	|--------------------------------------------------------------------------
	| Authentication Table
	|--------------------------------------------------------------------------
	|
	| When using the "fluent" authentication driver, the database table used
	| to load users may be specified here. This table will be used in by
	| the fluent query builder to authenticate and load your users.
	|
	*/

	'table' => 'users',

	/*
	|--------------------------------------------------------------------------
	| CAS configuration
	|--------------------------------------------------------------------------
	|
	| When using the "cas" authentication driver, these settings will be used.
	|
	*/

	'cas' => array(
		// Full hostname of the CAS server
		'host' => 'devsites01.symfoni.com',

		// Context of the CAS server
		'context' => '/poc',

		// Port of the CAS server. Normally for a https server it's 443
		'port' => 8443,

		// Should we allow proxy access to other CAS services?
		'proxy' => false,

		// Path to the ca chain that issued the cas server certificate,
		// if empty no server validation will be performed
		'server_ca_cert_path' => null,

		// The default behaviour is to let CAS auto authenticate
		'auto_authenticate' => true,

		// The URL to the custom login form if manual authentication
		'custom_login_url' => null,

		// Log file for CAS auth, NULL for no logging
		'log_file' => null
	),

	/*
	|--------------------------------------------------------------------------
	| FastAuth configuration
	|--------------------------------------------------------------------------
	|
	| When using the "fast" authentication driver, these settings will be used.
	|
	*/

	'fast' => array(
		// The URL to the login form
		'login_url' => '/login'
	),
);
