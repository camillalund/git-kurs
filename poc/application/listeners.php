<?php

/*
|--------------------------------------------------------------------------
| Application listeners
|--------------------------------------------------------------------------
|
| Events can provide a great away to build de-coupled applications, and 
| allow plug-ins to tap into the core of your application without modifying its code.
|
| Let's walk through an example...
|
|		Event::listen('loaded', function()
|		{
|		    // I'm executed on the "loaded" event!
|		});
|
*/


