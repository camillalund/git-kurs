<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name.";
|		});
|
*/

Route::any( '(.*)', 'sites::pages@render');
/* page add */
Route::post('ngi/page','ngi@page');
/* Group add */
Route::post('ngi/group/(:num)','ngi@group');
/* Group deactivate */
Route::put('ngi/group/(:num)','ngi@group');

/* media add */
Route::post('ngi/media','ngi@media');

/* Member add */
Route::post('ngi/member/add/(:num)','ngi@memberAdd');
Route::post('ngi/members/add/(:num)','ngi@membersAdd');
/* Member remove */
Route::delete('ngi/member/remove/(:num)/(:num?)','ngi@memberRemove');

/* Employee add */
Route::post('ngi/employee/(:num)','ngi@employee');
/* Employee update */
Route::put('ngi/employee/(:num)','ngi@employee');

/* TAGS subscription */
Route::post('ngi/tag/(:num)','ngi@tag');
Route::delete('ngi/tag/(:num)','ngi@tag');

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application. The exception object
| that is captured during execution is then passed to the 500 listener.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function($exception)
{
	return Response::error('500');
});
