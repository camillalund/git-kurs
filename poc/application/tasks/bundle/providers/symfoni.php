<?php

namespace Symfoni\Tasks\Bundle\Providers;
use Laravel\File;

class Symfoni extends \Laravel\CLI\Tasks\Bundle\Providers\Provider {

	/**
	 * Install the given bundle into the application.
	 *
	 * @param  string  $bundle
	 * @param  string  $path
	 * @return void
	 */
	public function install($bundle, $path)
	{
		static::credentials();

		$url = "https://developer.symfoni.com/bundles/download/{$bundle['location']}";
		$version = get_cli_option('version');
		if ($version) $url .= "/$version";
		
		$this->zipball($url, $bundle, $path);
	}

	/**
	 * Install a bundle from by downloading a Zip.
	 *
	 * @param  string  $url
	 * @param  array   $bundle
	 * @param  string  $path
	 * @return void
	 */
	protected function zipball($url, $bundle, $path)
	{
		$work = path('storage').'work/';

		// When installing a bundle from a Zip archive, we'll first clone
		// down the bundle zip into the bundles "working" directory so
		// we have a spot to do all of our bundle extraction work.
		$target = $work.'laravel-bundle.zip';

		$this->downloadToFile($url, $target);

		$zip = new \ZipArchive;

		$zip->open($target);

		// Once we have the Zip archive, we can open it and extract it
		// into the working directory. Symfoni's ZIP-files do not contain
		// a root directory, so that's why we override the default
		// behaviour slightly here
		mkdir($work.'zip');

		$zip->extractTo($work.'zip');

		$latest = $work.'zip';

		@chmod($latest, 0777);

		// Once we have the latest modified directory, we should be
		// able to move its contents over into the bundles folder
		// so the bundle will be usable by the developer.
		if (File::exists($path)) File::rmdir($path);	// First remove the old bundle directory (if there is one)
		File::mvdir($latest, $path);					// to make sure we get a clean install.

		$zip->close();
		@unlink($target);
	}

	/**
	 * Download a remote zip archive from a URL to the specified file.
	 *
	 * @param  string  $url
	 * @param  string  $target
	 */
	protected function downloadToFile($url, $target)
	{
		$fp = fopen($target, 'w');
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, static::$username.':'.static::$password);
		curl_setopt($ch, CURLOPT_FILE, $fp);
		$success = curl_exec($ch);
		curl_close($ch);
		fclose($fp);

		// If we were unable to download the zip archive correctly
		// we'll bomb out since we don't want to extract the last
		// zip that was put in the storage directory.
		if (!$success)
		{
			throw new \Exception("Error downloading the requested bundle.");
		}
	}

	private static function credentials() {
		if (!static::$username || !static::$password) {
			$out = fopen('php://stdout', 'w');
			$in = fopen('php://stdin', 'r');

			fprintf($out, "Enter your Symfoni username: ");
			flush();
			static::$username = preg_replace('/\r?\n$/', '', fgets($in));

			fprintf($out, "Enter your password: ");
			flush();
			//TODO: might not work on winblows...
			static::$password = preg_replace('/\r?\n$/', '', `stty -echo; head -n1`);
			@exec("stty echo");
			fprintf($out, "\n");
			flush();

			if (!static::$username || !static::$password) throw new \Exception("Error: you need to specify both username and password.");
		}
	}

	private static $username;
	private static $password;
}