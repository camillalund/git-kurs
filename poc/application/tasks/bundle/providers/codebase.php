<?php

namespace Symfoni\Tasks\Bundle\Providers;
use \File;

class CodeBase extends \Laravel\CLI\Tasks\Bundle\Providers\Provider {

	/**
	 * Install the given bundle into the application.
	 *
	 * @param  string  $bundle
	 * @param  string  $path
	 * @return void
	 */
	public function install($bundle, $path)
	{
		$work = path('storage').'work/';
		$url = "https://deploy-user@symfoni.codebasehq.com/{$bundle['location']}.git";
		$target = $work.'laravel-bundle-clone';

		$this->git_clone($url, $target);
		$this->git_archive($target, $path);

		//Cleanup...
		File::rmdir($target);
	}

	protected function git_clone($url, $target) {
		//Make sure we remove any old repository clones....
		if (File::exists($target)) {
			File::rmdir($target);
		}

		echo "\n   Downloading from CodeBase. ";

		//Clone the repository
		@exec("git clone $url $target", $retval);
		if (!File::exists($target)) {
			throw new \Exception("Error downloading the requested bundle.");
		}
	}

	protected function git_archive($target, $path, $rev = 'master') {
		File::mkdir($path);
		@exec("cd $target ; git archive --format tar $rev |tar x -C $path");
		@chmod($path, 0777);
	}
}