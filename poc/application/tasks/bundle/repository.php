<?php

namespace Symfoni\Tasks\Bundle;

class Repository extends \Laravel\CLI\Tasks\Bundle\Repository {

	protected $symfoni_api = 'http://developer.symfoni.com/bundles/api/';

	public function get($bundle)
	{
		$results = @file_get_contents($this->symfoni_api.$bundle);
		if ($results) {
			$results = json_decode($results, true);
			if ($results && array_key_exists('status', $results) && $results['status'] == 'ok') {
				return $results;
			}
		}
		return parent::get($bundle);
	}
}
