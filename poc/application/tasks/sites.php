<?php

require_once path('sys') . 'cli' . DS . 'dependencies' . EXT;
use Laravel\CLI\Command as Command;


class Sites_Task {
	
	public function run() {
		$this->help();
	}

	public function help() {
		echo <<<EOT
\n==========================SITES INSTALLATION GUIDE=========================\n
This task contains an interactive guide to fetch, install and configure
SYMFONI: Sites on a clean Laravel installation. You can either run the
full script (recommended) or just individual steps:

   sites:install                    Run the full script (recommended).
                                    Includes all of the steps below...

   sites:configGit                  Configure the local Git repository.
   sites:download                   Download the Sites bundles.
   sites:patchBundles               Add Sites to the bundles configuration.
   sites:runInstallScript           Run the Sites installation scripts.
   sites:configureDatabase          Configure the database settings.
   sites:configureAuthentication    Configure the authentication settings.
   sites:migrateDatabase            Migrate the database.
   sites:seedDefaultSite            Seed the default site.
   sites:publishAssets              Publish the bundles assets.
   sites:generateKey                Generate application key.
\n====================================END====================================\n
EOT;
	}

	public function install() {
		$this->configGit();
		$this->download();
		$this->patchBundles();
		$this->runInstallScript();
		$this->configureDatabase();
		$this->configureAuthentication();
		$this->migrateDatabase();
		$this->seedDefaultSite();
		$this->publishAssets();
		$this->generateKey();
		$this->wrapUp();
	}

	//-----------------------------------------------------------------------------------------------
	// Individual steps

	public function configGit() {
		if (File::exists('.git')) {
			//First, check if origin points to the original Laravel repository. If so, rename it...
			$origin = exec('git config --get remote.origin.url');
			if (preg_match('#git@git.symfoni.com:libraries/laravel.git#', $origin) === 1) {
				$this->output("\nRenaming Laravel git origin...\n");
				exec('git remote rename origin laravel');
			}

			//Init git flow with the default values (suppress error output)...
			$this->output("\nInitiating git flow...\n");
			exec('git flow init -d 2>/dev/null');
		}
	}

	public function download() {
		$this->output("\nDownloading Sites bundles...\n");
		if (!File::exists(__DIR__.'/../../bundles/sites')) {
			$this->output("Please enter your SYMFONI Developer Portal credentials.\n");
			Command::run(array('bundle:install', 'sites'));
			//TODO: flush echo buffer...
		}
		else $this->output("   Skipping step: Bundle already downloaded!\n");
	}

	public function patchBundles() {
		$this->output("\nAdding Sites to the bundles configuration...\n");
		if (!Bundle::exists('sites')) {
			$content = file_get_contents(path('app').'bundles.php');
			$content = preg_replace(
				"/'docs' => array\('handles' => 'docs'\),/",
				"'sites' => array('auto' => true),",
				$content
			);
			file_put_contents(path('app').'bundles.php', $content);
		}
		else $this->output("   Skipping step: Bundle already installed!\n");
	}

	public function runInstallScript() {
		$this->output("\nRunning Sites installation scripts...\n");
		$this->exec('php artisan sites::install:bundles');
		$this->exec('php artisan sites::install');
	}

	public function configureDatabase() {
		$this->output("\nConfiguring PostgreSQL database settings...\n");
		if (Config::get('database.connections.pgsql.database') == 'database') {
			$this->output("Please enter your database details:\n");
			$host = $this->ask("   Host [127.0.0.1]: ", "127.0.0.1");
			$database = $this->ask("   Database: ");
			$username = $this->ask("   Username: ");
			$password = $this->askPassword("   Password: ");

			$content = file_get_contents(path('app').'config/database.php');
			$content = preg_replace(
				"/			\'driver\'   => \'pgsql\',\n".
				"			\'host\'     => \'127.0.0.1\',\n".
				"			\'database\' => 'database\',\n".
				"			\'username\' => \'root\',\n".
				"			\'password\' => \'\',/",

				"			'driver'   => 'pgsql',\n".
				"			'host'     => '$host',\n".
				"			'database' => '$database',\n".
				"			'username' => '$username',\n".
				"			'password' => '$password',",

				$content
			);
			file_put_contents(path('app').'config/database.php', $content);
		}
		else $this->output("   Skipping step: Database already configured!\n");
	}

	public function configureAuthentication() {
		$this->output("\nConfiguring CAS authentication settings...\n");
		if (Config::get('auth.cas.host') == 'cas.example.com') {
			$this->output("Please enter your CAS details:\n");
			$host = $this->ask("   Host: ");
			$context = $this->ask("   Context [/cas]: ", "/cas");
			$port = $this->ask("   Port [443]: ", "443");
			
			$content = file_get_contents(path('app').'config/auth.php');
			$content = preg_replace(
				"/'host' => 'cas.example.com',/",
				"'host' => '$host',",
				$content
			);
			$content = preg_replace(
				"/'context' => '\/cas',/",
				"'context' => '$context',",
				$content
			);
			$content = preg_replace(
				"/'port' => 443,/",
				"'port' => $port,",
				$content
			);
			$content = preg_replace(
				"/'server_ca_cert_path' => '\/path\/to\/cachain.pem',/",
				"'server_ca_cert_path' => null,",
				$content
			);
			file_put_contents(path('app').'config/auth.php', $content);
		}
		else $this->output("   Skipping step: Authentication already configured!\n");
	}

	public function migrateDatabase() {
		$this->output("\nMigrating the database...\n");
		$this->exec('php artisan migrate:install');
		$this->exec('php artisan migrate');
	}

	public function seedDefaultSite() {
		$this->output("\nSeeding the default site...\n");
		if (!File::exists(path('app').'views/cms/menus/main.blade.php')) {
			$this->exec('php artisan db:seed sites::DefaultSite');
		}
		else $this->output("   Skipping step: A sites already seems to be present!\n");
	}

	public function publishAssets() {
		$this->output("\nPublishing the bundle assets...\n");
		$this->exec('php artisan bundle:publish');
	}

	public function generateKey() {
		$this->output("\nGenerating an application key...\n");
		$key = Config::get('application.key');
		if (empty($key)) {
			Command::run(array('key:generate'));
		}
		else $this->output("   Skipping step: Application key already exists!\n");
	}

	public function wrapUp() {
		$this->output("\n--------------------------------------------------------------------------\n\n");
		$this->output("Sites has been installed. It's recommended that you remove the file:\n\n");
		$this->output("   application/tasks/sites.php\n\n");
		$this->output("Also, remember to setup appropriate file permissions.\n");
		$this->output("The Apache user typically needs write access to the following directories:\n\n");
		$this->output("   application/views/cms\n");
		$this->output("   application/sites\n");
		$this->output("   storage\n");
		$this->output("\n--------------------------------------------------------------------------\n\n");
	}

	//-----------------------------------------------------------------------------------------------
	// Implementation

	private function output($str) {
		fprintf($this->out, $str);
		flush();
	}

	private function ask($str, $defaults = '') {
		$this->output($str);
		$response = trim(fgets($this->in));
		return empty($response)? $defaults : $response;
	}

	private function askPassword($str) {
		$this->output($str);
		//TODO: might not work on winblows...
		$password = preg_replace('/\r?\n$/', '', `stty -echo; head -n1`);
		@exec("stty echo");
		$this->output("\n");
		return $password;
	}

	private function exec($cmd) {
		exec($cmd.$this->env);
	}

	public function __construct() {
		$env = get_cli_option('env', getenv('LARAVEL_ENV'));
		$this->env = $env? " --env=$env" : '';
		$this->out = fopen('php://stdout', 'w');
		$this->in = fopen('php://stdin', 'r');
	}

	private $env;
	private $out;
	private $in;
}