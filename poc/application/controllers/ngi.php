<?php
use Symfoni\Sites\Cms\Services\MediaService;


class Ngi_Controller extends Base_Controller {

	public $restful = true;
	
	//TODO:
	//Validation

	public function get_index()
	{
		return "";
	}
	
	//page ADD
	public function post_page()
	{
		$rules = array( 
			'parent_id' => 'required|integer',
			'template_id' => 'required|integer',
			'subject' => 'required',
		);
		$validator = Validator::make(Input::all(),$rules);
		
			
		if (!$validator->passes()) {	
			return Redirect::to(Input::get('pageuri'))->with('message',$validator->errors->all(':message'));
		}
		
		try {	
		
			//add image
			$fil = Input::file('fil');
			$folderid = null;
			if(Input::get('folder_id')!=""){
				$folderid = Input::get('folder_id');
			}
			
			$imgid = null;
			
			if($fil['name']!=""){
				$data = [
					'title' => $fil['name'],
					'filename' => $fil['name'],
					'extension' => File::extension($fil['name']),
					'type' => $fil['type'], //mime, t.ex.: image/jpeg, File::mime($path)
					'size' => $fil['size'],
					'folder_id' => $folderid, //optional
					'author_id' => Auth::user()->id
					//'expires_at' => 'date'
				];
				
				$service = new MediaService;
				$media = $service->create($data, $fil['tmp_name']);
				
				$imgid = $media['id'];
			}
			
			$service = new Symfoni\Sites\Cms\Services\PageService;
			$userid = Auth::user()->id;
			$content = [
						'preamble' => Input::get('preamble'),
						'body' => Input::get('body')
			];
			if($imgid){
				$content['image1'] = $imgid;
			}
			if(Input::get('publish_date')!=""){
				$content['publish_date'] = Input::get('publish_date');
			}
			$data = [
				'access' => Page::AccessDefault,
				'author_id' => Auth::user()->id,
				'parent_id' => Input::get('parent_id'),
				'status' => Page::StatusPublished,
				'template_id' => Input::get('template_id'),
				'translations' => [
					[
						'language_id' => 2,
						'subject' => Input::get('subject'),
						'content' => $content
					]
				]
			];
			
			
			if(Input::get('keyword')!=""){
				$data['keywords']= [
					[
						'id' => Input::get('keyword')
					]
				];
			}
			
			
			$page = $service->create($data);
			
			return Redirect::to(Input::get('pageuri'));
		}
		catch(Exception $e) {
			//...
			return $e->getMessage();
		}
		
	}
	
	//MEMBER ADD
	public function post_memberAdd($pageid)
	{
		
		if($pageid){
			$userid = Auth::user()->id;
			$member = Groupmember::where('user_id','=',$userid)
						->where('page_id','=',$pageid)
						->first();
			if(!$member){
				$grmember = Groupmember::create(array(
					'page_id' => $pageid,
					'user_id' => $userid,
					'admin' => 0
				));
			}
			$json = json_encode(array( 'success' => true, 'message' => 'Medlem er lagt til gruppen.' ));
		}else{
			$json = json_encode(array( 'success' => false, 'message' => 'Error' ));
		}
		return $json;
	}
	
	//MEMBERS ADD
	public function post_membersAdd($pageid)
	{
		
		if($pageid){
			$users = Input::get('users');
			
			foreach($users as $user){
				//dd($user);
				$member = Groupmember::where('user_id','=',$user)
							->where('page_id','=',$pageid)
							->first();
				if(!$member){
					$grmember = Groupmember::create(array(
						'page_id' => $pageid,
						'user_id' => $user,
						'admin' => 0
					));
				}
			}
			$json = json_encode(array( 'success' => true, 'message' => 'Medlem er lagt til gruppen.' ));
		}else{
			$json = json_encode(array( 'success' => false, 'message' => 'Error' ));
		}
		return Redirect::to(Input::get('pageuri'));
	}
	
	//MEMBER REMOVE
	public function delete_memberRemove($pageid, $userid=null)
	{
		if($userid===null){
			$userid = Auth::user()->id;
		}
		
		if($pageid && $userid){
			$memberRemove = Groupmember::where_page_id($pageid)
							->where_user_id($userid)
							->delete();
							
			$json = json_encode(array( 'success' => true, 'message' => 'Medlem er fjernet fra gruppen.' ));
		}else{
			$json = json_encode(array( 'success' => false, 'message' => 'Error' ));
		}
		return $json;
	}
	
	//GROUP ADD
	public function post_group($parentid)
	{
		if(Input::get('closed_group') === NULL){
			$closed = 0;
		}else{
			$closed = 1;
		}
		
		try {	
			$service = new Symfoni\Sites\Cms\Services\PageService;
			$userid = Auth::user()->id;
			$page = $service->create([
				'access' => Page::AccessDefault,
				'author_id' => Auth::user()->id,
				'parent_id' => $parentid,
				'status' => Page::StatusPublished,
				'template_id' => Input::get('template_id'),
				'translations' => [
					[
						'language_id' => 2,
						'subject' => Input::get('subject'),
						'content' => [
							'preamble' => Input::get('preamble'),
							'body' => Input::get('body'),
							'closed_group' => $closed
						]
					]
				]
			]);
			
			//Lukket gruppe
			$grmember = Groupmember::create(array(
					'page_id' => $page['id'],
					'user_id' => $userid,
					'admin' => 1
				));
				
			return Redirect::to(Input::get('pageuri').'/'.$page['translations'][0]['slug']);
		}
		catch(Exception $e) {
			//...
			dd($e);
		}
		
	}
	
	//GROUP REMOVE
	public function put_group($pageid)
	{
		//sette page til arkiv status
	}
	
	//EMPLOYEE SAVE
	public function post_employee($id){
		$msg = "Error";
				
		try{
			$created = Employee::create(array(
						'user_id' => $id,
						'addresse' => Input::get('addresse'),
						'zipcode' => Input::get('zipcode'),
						'city' => Input::get('city'),
						'position' => Input::get('position'),
						'department' => Input::get('department'),
						'birthdate' => Input::get('birthdate'),
						'info' => Input::get('info')
			));
				

			$msg = "Dine date er lagret";
		}catch(Exception $e){
			$msg = $e->getMessage();
		}
		return Redirect::to('my-info')->with('message',$msg);
	}
	
	//EMPLOYEE update
	public function put_employee($id){
		$msg = "Error";
				
		try{
			$employee = Employee::where('user_id','=',$id)->first();
			
			if($employee){
				
				//add image
				$fil = Input::file('fil');
				$imgid = null;
				if($fil){
					$data = [
						'title' => $fil['name'],
						'filename' => $fil['name'],
						'extension' => File::extension($fil['name']),
						'type' => $fil['type'], //mime, t.ex.: image/jpeg, File::mime($path)
						'size' => $fil['size'],
						'folder_id' => Input::get('folder_id'), //optional
						'author_id' => Auth::user()->id
						//'expires_at' => 'date'
					];
					
					$service = new MediaService;
					$media = $service->create($data, $fil['tmp_name']);
					$imgid = $media['id'];
				}
				
				
				$updated = Employee::where('user_id','=',$id)
					->update(array(
						'addresse' => Input::get('addresse'),
						'zipcode' => Input::get('zipcode'),
						'city' => Input::get('city'),
						'position' => Input::get('position'),
						'department' => Input::get('department'),
						'birthdate' => Input::get('birthdate'),
						'info' => Input::get('info'),
						'img' => $imgid
					));
				$user = User::find($id);
				if($user){
					User::where('id','=',$id)
						->update(array(
							'firstname' => Input::get('firstname'),
							'middlename' => Input::get('middlename'),
							'lastname' => Input::get('lastname'),
							'email' => Input::get('email'),
							'phone' => Input::get('phone')
						));	
				}
			
			}
			$msg = "Dine data er lagret";
		}catch(Exception $e){
			$msg = $e->getMessage();
		}
		return Redirect::to('my-info')->with('message',$msg);
	}
	
	//media ADD
	public function post_media()
	{
		$fil = Input::file('fil');
		
		$data = [
			'title' => $fil['name'],
			'filename' => $fil['name'],
			'extension' => File::extension($fil['name']),
			'type' => $fil['type'], //mime, t.ex.: image/jpeg, File::mime($path)
			'size' => $fil['size'],
			'folder_id' => Input::get('folder_id'), //optional
			'author_id' => Auth::user()->id
			//'expires_at' => 'date'
		];
		
		$service = new MediaService;
		$media = $service->create($data, $fil['tmp_name']);
		//dd($media);
		return Redirect::to(Input::get('pageuri'));
	}
	
	//Subscribe to tag
	public function post_tag($tagid){
		$userid = Auth::user()->id;
		if($tagid){
			$tagadd = Subscription::create(array(
						'tag_id' => $tagid,
						'user_id' => $userid
					));
							
			$json = json_encode(array( 'success' => true, 'message' => '' ));
		}else{
			$json = json_encode(array( 'success' => false, 'message' => 'Error' ));
		}
		return $json;
	}
	
	//UNsubscribe to tag
	public function delete_tag($tagid){
		$userid = Auth::user()->id;
		if($tagid){
			$tagRemove = Subscription::where_tag_id($tagid)
							->where_user_id($userid)
							->delete();
							
			$json = json_encode(array( 'success' => true, 'message' => '' ));
		}else{
			$json = json_encode(array( 'success' => false, 'message' => 'Error' ));
		}
		return $json;
	}

}