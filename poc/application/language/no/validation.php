<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used
	| by the validator class. Some of the rules contain multiple versions,
	| such as the size (max, min, between) rules. These versions are used
	| for different input types such as strings and files.
	|
	| These language lines may be easily changed to provide custom error
	| messages in your application. Error messages for custom validation
	| rules may also be added to this file.
	|
	*/

	"accepted"       => ":attribute m&aring; aksepteres.",
	"active_url"     => ":attribute er ikke en gyldig URL.",
	"after"          => ":attribute m&aring; v&aelig;re en dato etter :date.",
	"alpha"          => ":attribute m&aring; kun inneholde bokstaver.",
	"alpha_dash"     => ":attribute m&aring; kun inneholde bokstaver, tall og streker.",
	"alpha_num"      => ":attribute m&aring; kun inneholde bokstaver og tall.",
	"array"          => ":attribute m&aring; ha utvalgte elementer.",
	"before"         => ":attribute m&aring; v&aelig;re en dato dato f&oslash;r :date.",
	"between"        => array(
		"numeric" => ":attribute m&aring; v&aelig;re mellom :min - :max.",
		"file"    => ":attribute m&aring; v&aelig;re mellom :min - :max kilobyte.",
		"string"  => ":attribute m&aring; v&aelig;re mellom :min - :max tegn.",
	),
	"confirmed"      => ":attribute bekreftelsen stemmer ikke.",
	"count"          => ":attribute m&aring; ha n&oslash;yaktig :count utvalgte elementer.",
	"countbetween"   => ":attribute m&aring; ha mellom :min og :max utvalgte elementer.",
	"countmax"       => ":attribute m&aring; ha mindre enn :max utvalgte elementer.",
	"countmin"       => ":attribute m&aring; ha minst :min utvalgte elementer.",
	"date_format"	 => ":attribute mm&aring; ha en gyldig dato format.",
	"different"      => ":attribute og :other m&aring; v&aelig;re ulike.",
	"email"          => "Formatet for :attribute er ugyldig.",
	"exists"         => "Den valgte :attribute er ugyldig.",
	"image"          => ":attribute m&aring; v&aelig;re en bilde.",
	"in"             => "Den valgte :attribute er ugyldig.",
	"integer"        => ":attribute m&aring; m&aring; v&aelig;re et heltall.",
	"ip"             => ":attribute m&aring; v&aelig;re en gyldig IP adresse.",
	"match"          => "Formatet for :attribute er ugyldig.",
	"max"            => array(
		"numeric" => ":attribute m&aring; v&aelig;re mindre enn :max.",
		"file"    => ":attribute m&aring; v&aelig;re mindre enn :max kilobyte.",
		"string"  => ":attribute m&aring; v&aelig;re mindre enn :max tegn.",
	),
	"mimes"          => ":attribute m&aring; v&aelig;re en fil av typen: :values.",
	"min"            => array(
		"numeric" => ":attribute m&aring; v&aelig;re minst :min.",
		"file"    => ":attribute m&aring; v&aelig;re minst :min kilobytes",
		"string"  => ":attribute m&aring; v&aelig;re minst :min tegn.",
	),
	"not_in"         => "Den valgte :attribute er ugyldig.",
	"numeric"        => ":attribute m&aring; v&aelig;re et tall.",
	"required"       => ":attribute er p&aring;krevd.",
	"required_with"  => ":attribute er p&aring;krevd med :field",
	"same"           => ":attribute og :other m&aring; stemme overens.",
	"size"           => array(
		"numeric" => ":attribute m&aring; v&aelig;re :size.",
		"file"    => ":attribute m&aring; v&aelig;re :size kilobyte.",
		"string"  => ":attribute m&aring; v&aelig;re :size tegn.",
	),
	"unique"         => ":attribute er allerede tatt.",
	"url"            => ":attribute formatet er ugyldig.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute_rule" to name the lines. This helps keep your
	| custom validation clean and tidy.
	|
	| So, say you want to use a custom validation message when validating that
	| the "email" attribute is unique. Just add "email_unique" to this array
	| with your custom message. The Validator will handle the rest!
	|
	*/

	'custom' => array(),

	/*
	|--------------------------------------------------------------------------
	| Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as "E-Mail Address" instead
	| of "email". Your users will thank you.
	|
	| The Validator class will automatically search this array of lines it
	| is attempting to replace the :attribute place-holder in messages.
	| It's pretty slick. We think you'll like it.
	|
	*/

	'attributes' => array(),

);