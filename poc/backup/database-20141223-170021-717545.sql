--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: cms; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA cms;


--
-- Name: custom; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA custom;


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: plv8; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plv8 WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plv8; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plv8 IS 'PL/JavaScript (v8) trusted procedural language';


SET search_path = cms, pg_catalog;

--
-- Name: f_convert_page_to_text(text, json, bigint); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_convert_page_to_text(subject text, content json, template_id bigint) RETURNS text
    LANGUAGE plv8 IMMUTABLE STRICT
    AS $_$
					var result = subject;
					if (!!content) {
						var keyword,
							properties = plv8.execute( 'SELECT keyword FROM cms.properties WHERE template_id = $1 AND index = 1 AND (type = \'text\' OR type = \'textarea\' OR type = \'richtext\');', [ template_id ] );
						for(var n=0; n<properties.length; ++n) {
							keyword = properties[n].keyword;
							if (keyword in content) {
								result += ' ' + content[keyword];
							}
						}
					}
					result = result.replace(/\u([\d\w]{4})/gi, function (match, grp) {
						return String.fromCharCode(parseInt(grp, 16));
					});
					return unescape(result).toLowerCase();
				$_$;


--
-- Name: f_filter_page_trigger(); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_filter_page_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
				DECLARE
					newSort BIGINT;
					updateOthers BOOLEAN := true;
				BEGIN
					IF pg_trigger_depth() = 1 THEN
						IF NEW.sort IS NULL THEN
							IF NEW.parent_id IS NOT NULL AND TG_OP = 'INSERT' THEN
								NEW.sort := 1;
							ELSE
								IF NEW.parent_id IS NULL THEN
									SELECT MAX(sort) INTO newSort FROM cms.pages WHERE parent_id IS NULL;
								ELSE
									SELECT MAX(sort) INTO newSort FROM cms.pages WHERE parent_id = NEW.parent_id;
								END IF;
								IF newSort IS NULL THEN
									NEW.sort = 1;
								ELSE
									NEW.sort = newSort + 1;
								END IF;
								updateOthers := false;
							END IF;
						END IF;
						IF updateOthers THEN
							IF NEW.parent_id IS NULL THEN
								UPDATE cms.pages SET sort = sort + 1 WHERE parent_id IS NULL AND sort >= NEW.sort AND id <> NEW.id;
							ELSE
								UPDATE cms.pages SET sort = sort + 1 WHERE parent_id = NEW.parent_id AND sort >= NEW.sort AND id <> NEW.id;
							END IF;
						END IF;
					END IF;
					return NEW;
				END;
				$$;


--
-- Name: f_filter_translation_trigger(); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_filter_translation_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
				DECLARE
					parentId BIGINT;
					count INTEGER;
					newSlug TEXT;
				BEGIN
					count = 1;
					newSlug := NEW.slug;
					SELECT parent_id INTO parentId FROM cms.pages WHERE id = NEW.page_id;
					IF parentId IS NULL THEN
						WHILE EXISTS (SELECT 1 FROM cms.pages INNER JOIN cms.translations ON (pages.id = translations.page_id) WHERE parent_id IS NULL AND slug = newSlug AND language_id = NEW.language_id AND translations.id <> NEW.id) LOOP
							count := count + 1;
							newSlug = NEW.slug || '-' || count::text;
						END LOOP;
					ELSE
						WHILE EXISTS (SELECT 1 FROM cms.pages INNER JOIN cms.translations ON (pages.id = translations.page_id) WHERE parent_id = parentId AND slug = newSlug AND language_id = NEW.language_id AND translations.id <> NEW.id) LOOP
							count := count + 1;
							newSlug = NEW.slug || '-' || count::text;
						END LOOP;
					END IF;
					NEW.slug := newSlug;
					return NEW;
				END;
				$$;


--
-- Name: f_findpageidfrompath(text, bigint); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_findpageidfrompath(path text, root bigint DEFAULT NULL::bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
				DECLARE
					slugs text[];
					length integer;
					language bigint;
					result bigint;
				BEGIN
					path := lower(trim(both '/' from path));
					slugs := regexp_split_to_array(path, E'/+');
					length := array_length(slugs, 1);

					IF length > 1 THEN
						SELECT id INTO language FROM cms.languages WHERE code = slugs[1] LIMIT 1;
						IF FOUND THEN
							WITH RECURSIVE tree(depth, page) AS (
								SELECT 2, pages.id FROM
									cms.pages INNER JOIN cms.translations ON pages.id = translations.page_id
								WHERE
									CASE WHEN root IS NULL THEN pages.parent_id IS NULL ELSE pages.parent_id = root END AND
									pages.trash = 0 AND translations.language_id = language AND translations.slug = slugs[2]

								UNION ALL

								SELECT tree.depth+1, pages.id FROM
									tree, cms.pages INNER JOIN cms.translations ON pages.id = translations.page_id
								WHERE
									(tree.depth+1) <= length AND pages.parent_id = tree.page AND pages.trash = 0 AND translations.language_id = language AND translations.slug = slugs[tree.depth+1]
							)
							SELECT page INTO result FROM tree WHERE depth = length;

							RETURN result;
						END IF;
					END IF;

					RETURN null;
				END;
				$$;


--
-- Name: f_getmenustructure(text, integer, bigint, bigint, bigint); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_getmenustructure(key text, max_depth integer, language bigint, current_page bigint DEFAULT NULL::bigint, root bigint DEFAULT NULL::bigint) RETURNS TABLE(depth integer, parent_id bigint, id integer, subject text, slug text, summary text, current boolean, redirect_url text, redirect_new_window smallint)
    LANGUAGE plpgsql
    AS $$
				DECLARE
					menu bigint;
				BEGIN
					SELECT menus.id INTO menu FROM cms.menus WHERE keyword = key;
					IF menu IS NULL THEN
						RETURN;
					END IF;

					RETURN QUERY WITH RECURSIVE tree(depth, parent_id, id, subject, slug, summary, current, sort, redirect_url, redirect_new_window) AS (
						SELECT 0, pages.parent_id, pages.id, translations.subject, translations.slug, translations.summary, pages.id = current_page, pages.sort, pages.redirect_url, pages.redirect_new_window FROM
							cms.pages INNER JOIN cms.translations ON translations.page_id = pages.id INNER JOIN cms.menu_page ON menu_page.page_id = pages.id
						WHERE
							CASE WHEN root IS NULL THEN pages.parent_id IS NULL ELSE pages.parent_id = root END AND menu_id = menu AND language_id = language AND status = 2 AND trash = 0

						UNION ALL

						SELECT tree.depth+1, pages.parent_id, pages.id, translations.subject, translations.slug, translations.summary, pages.id = current_page, pages.sort, pages.redirect_url, pages.redirect_new_window FROM
							tree, cms.pages INNER JOIN cms.translations ON translations.page_id = pages.id INNER JOIN cms.menu_page ON menu_page.page_id = pages.id
						WHERE
							(tree.depth+1) < max_depth AND pages.parent_id = tree.id AND menu_id = menu AND language_id = language AND status = 2 AND trash = 0
					)
					SELECT tree.depth, tree.parent_id, tree.id, tree.subject, tree.slug, tree.summary, tree.current, tree.redirect_url, tree.redirect_new_window FROM tree ORDER BY depth, tree.parent_id NULLS FIRST, tree.sort;
				END;
				$$;


--
-- Name: f_geturlforpage(bigint, bigint, bigint, text, bigint); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_geturlforpage(page bigint, language bigint, parent bigint DEFAULT NULL::bigint, slug text DEFAULT NULL::text, root bigint DEFAULT NULL::bigint) RETURNS text
    LANGUAGE plpgsql
    AS $$
				DECLARE
					url text;
					code text;
				BEGIN
					IF root = 0 THEN
						root := NULL;
					END IF;
					SELECT languages.code INTO code FROM cms.languages WHERE id = language;
					IF page = root THEN
						RETURN '/' || code;
					ELSE
						IF slug IS NOT NULL THEN
							WITH RECURSIVE tree(parent_id, url) AS (
								SELECT parent, slug

								UNION ALL

								SELECT pages.parent_id, translations.slug || '/' || tree.url FROM
									tree, cms.pages INNER JOIN cms.translations ON pages.id = translations.page_id
								WHERE
									CASE WHEN root IS NULL THEN tree.parent_id IS NOT NULL ELSE tree.parent_id <> root END AND
									pages.id = tree.parent_id AND translations.language_id = language
							)
							SELECT tree.url INTO url FROM tree WHERE CASE WHEN root IS NULL THEN parent_id IS NULL ELSE parent_id = root END;
						ELSE
							WITH RECURSIVE tree(parent_id, url) AS (
								SELECT pages.parent_id, translations.slug::text FROM
									cms.pages INNER JOIN cms.translations ON pages.id = translations.page_id
								WHERE
									pages.id = page AND translations.language_id = language

								UNION ALL

								SELECT pages.parent_id, translations.slug || '/' || tree.url FROM
									tree, cms.pages INNER JOIN cms.translations ON pages.id = translations.page_id
								WHERE
									CASE WHEN root IS NULL THEN tree.parent_id IS NOT NULL ELSE tree.parent_id <> root END AND
									pages.id = tree.parent_id AND translations.language_id = language
							)
							SELECT tree.url INTO url FROM tree WHERE CASE WHEN root IS NULL THEN parent_id IS NULL ELSE parent_id = root END;
						END IF;
					
						RETURN '/' || code || '/' || url;
					END IF;
				END;
				$$;


--
-- Name: f_property_renamed_trigger(); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_property_renamed_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
				BEGIN
					IF OLD.keyword <> NEW.keyword THEN
						UPDATE cms.translations
							SET content = cms.f_rename_property_in_content(OLD.keyword::TEXT, NEW.keyword::TEXT, content)
							FROM cms.pages WHERE translations.page_id = pages.id AND pages.template_id = NEW.template_id;
					END IF;
					return NEW;
				END;
				$$;


--
-- Name: f_rename_property_in_content(text, text, json); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_rename_property_in_content(old_name text, new_name text, content json) RETURNS json
    LANGUAGE plv8 IMMUTABLE STRICT
    AS $$
					if (!!content && (old_name in content)) {
						content[new_name] = content[old_name];
						delete content[old_name];
					}
					return content;
				$$;


--
-- Name: f_update_media_index_extracts_trigger(); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_update_media_index_extracts_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
				DECLARE
					config REGCONFIG;
					index TSVECTOR;
					media cms.media%ROWTYPE;
				BEGIN
					SELECT * INTO media FROM cms.media WHERE id = NEW.media_id;
					IF media.index <> 0 THEN
						IF media.language_id IS NOT NULL THEN
							SELECT dictionary::REGCONFIG INTO config FROM cms.languages WHERE id = media.language_id;
						ELSE
							config := 'simple'::REGCONFIG;
						END IF;
						index := to_tsvector(config, COALESCE(media.title, '') || ' ' || COALESCE(media.filename, '') || ' ' || COALESCE(media.description, '') || ' ' || COALESCE(NEW.content, ''));
						UPDATE cms.media_index SET phrase = index WHERE media_id = NEW.media_id;
					END IF;
					return NEW;
				END;
				$$;


--
-- Name: f_update_media_index_trigger(); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_update_media_index_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
				DECLARE
					config REGCONFIG;
					index TSVECTOR;
					extracts TEXT;
				BEGIN
					IF NEW.index <> 0 THEN
						SELECT content INTO extracts FROM cms.media_extracts WHERE media_id = NEW.id;
						IF NEW.language_id IS NOT NULL THEN
							SELECT dictionary::REGCONFIG INTO config FROM cms.languages WHERE id = NEW.language_id;
						ELSE
							config := 'simple'::REGCONFIG;
						END IF;
						index := to_tsvector(config, COALESCE(NEW.title, '') || ' ' || COALESCE(NEW.filename, '') || ' ' || COALESCE(NEW.description, '') || ' ' || COALESCE(extracts, ''));
						IF TG_OP = 'INSERT' THEN
							INSERT INTO cms.media_index (media_id, phrase) VALUES (NEW.id, index);
						ELSE
							UPDATE cms.media_index SET phrase = index WHERE media_id = NEW.id;
						END IF;
					ELSIF TG_OP = 'UPDATE' THEN
						DELETE FROM cms.media_index WHERE media_id = NEW.id;
					END IF;
					return NEW;
				END;
				$$;


--
-- Name: f_update_translation_index_trigger(); Type: FUNCTION; Schema: cms; Owner: -
--

CREATE FUNCTION f_update_translation_index_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
				DECLARE
					templateId BIGINT;
					pageIndex SMALLINT;
					templateIndex SMALLINT;
					config REGCONFIG;
					index TSVECTOR;
				BEGIN
					SELECT template_id, pages.index INTO templateId, pageIndex FROM cms.pages WHERE id = NEW.page_id;
					SELECT templates.index INTO templateIndex FROM cms.templates WHERE id = templateId;
					IF pageIndex <> 0 AND templateIndex <> 0 THEN
						SELECT dictionary::REGCONFIG INTO config FROM cms.languages WHERE id = NEW.language_id;
						index := to_tsvector(config, COALESCE(cms.f_convert_page_to_text(NEW.subject::TEXT, NEW.content, templateId), ''));
						IF TG_OP = 'INSERT' THEN
							INSERT INTO cms.search_index (translation_id, phrase) VALUES (NEW.id, index);
						ELSE
							UPDATE cms.search_index SET phrase = index WHERE translation_id = NEW.id;
						END IF;
					ELSIF TG_OP = 'UPDATE' THEN
						DELETE FROM cms.search_index WHERE translation_id = NEW.id;
					END IF;
					return NEW;
				END;
				$$;


SET search_path = public, pg_catalog;

--
-- Name: f_cas_to_sites_person_synchronization_trigger(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION f_cas_to_sites_person_synchronization_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN	IF TG_OP = 'UPDATE' THEN		UPDATE cms.users SET firstname = NEW.firstname, middlename = NEW.middlename, lastname = NEW.lastname, updated_at = NEW.modified_date WHERE id = NEW.id;	END IF;	RETURN NEW;END;$$;


--
-- Name: f_cas_to_sites_user_synchronization_trigger(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION f_cas_to_sites_user_synchronization_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE	p public.person%ROWTYPE;BEGIN	IF TG_OP = 'INSERT' THEN		SELECT * INTO p FROM public.person WHERE id = NEW.id;		INSERT INTO cms.users (id, username, firstname, middlename, lastname, email, password, deleted_at, created_at, updated_at) VALUES (NEW.id, NEW.login, p.firstname, p.middlename, p.lastname, NEW.email, NEW.password, (CASE WHEN NEW.is_enabled=FALSE THEN NOW() ELSE NULL END), NEW.created_date, NEW.created_date);	ELSIF TG_OP = 'UPDATE' THEN		UPDATE cms.users SET username = NEW.login, email = NEW.email, password = NEW.password, deleted_at = (CASE WHEN NEW.is_enabled=FALSE THEN NOW() ELSE NULL END), updated_at = NOW() WHERE id = NEW.id;	ELSIF TG_OP = 'DELETE' THEN		DELETE FROM cms.users WHERE id = OLD.id;	END IF;	RETURN NEW;END;$$;


--
-- Name: f_sites_to_cas_user_synchronization_trigger(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION f_sites_to_cas_user_synchronization_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN	IF TG_OP = 'INSERT' THEN		INSERT INTO public.person (id, firstname, middlename, lastname, email, company_id, persont_status_type_id, is_external, created_date, modified_date) VALUES (NEW.id, NEW.firstname, NEW.middlename, NEW.lastname, NEW.email, 0, 1, FALSE, NEW.created_at, NEW.updated_at);		INSERT INTO public.users (id, login, email, password, is_enabled, created_date) VALUES (NEW.id, NEW.username, NEW.email, NEW.password, TRUE, NEW.created_at);	ELSIF TG_OP = 'UPDATE' THEN		UPDATE public.person SET firstname = NEW.firstname, middlename = NEW.middlename, lastname = NEW.lastname, email = NEW.email, modified_date = NEW.updated_at WHERE id = NEW.id;		UPDATE public.users SET login = NEW.username, email = NEW.email, password = NEW.password, is_enabled=(CASE WHEN NEW.deleted_at IS NULL THEN TRUE ELSE FALSE END) WHERE id = NEW.id;	ELSIF TG_OP = 'DELETE' THEN		DELETE FROM public.users WHERE id = OLD.id;	END IF;	RETURN NEW;END;$$;


--
-- Name: f_unlink_st(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION f_unlink_st() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
				IF TG_OP = 'INSERT' THEN
				  RETURN NEW;
				ELSIF TG_OP = 'UPDATE' THEN
				  PERFORM lo_unlink(OLD.expiration_policy);
				  PERFORM lo_unlink(OLD.service);  
				  RETURN NEW;
				ELSIF TG_OP = 'DELETE' THEN
				  PERFORM lo_unlink(OLD.expiration_policy);
				  PERFORM lo_unlink(OLD.service);  
				  RETURN OLD;
				END IF;
				END;$$;


--
-- Name: f_unlink_tgt(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION f_unlink_tgt() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
				IF TG_OP = 'INSERT' THEN
				  RETURN NEW;
				ELSIF TG_OP = 'UPDATE' THEN
				  PERFORM lo_unlink(OLD.expiration_policy);
				  PERFORM lo_unlink(OLD.authentication);
				  PERFORM lo_unlink(OLD.services_granted_access_to);
				  RETURN NEW;
				ELSIF TG_OP = 'DELETE' THEN
				  PERFORM lo_unlink(OLD.expiration_policy);
				  PERFORM lo_unlink(OLD.authentication);
				  PERFORM lo_unlink(OLD.services_granted_access_to);
				  RETURN OLD;
				END IF;
				END;$$;


SET search_path = cms, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: channels; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE channels (
    id integer NOT NULL,
    name text NOT NULL,
    description text,
    type bigint NOT NULL,
    criteria text NOT NULL,
    active smallint DEFAULT 1::smallint NOT NULL,
    layout_id bigint,
    language_id bigint,
    root_id bigint,
    start_id bigint,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: channels_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE channels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: channels_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE channels_id_seq OWNED BY channels.id;


--
-- Name: comments; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE comments (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    page_id bigint NOT NULL,
    language_id bigint NOT NULL,
    comment text NOT NULL,
    edited smallint DEFAULT 0::smallint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    parent_id bigint
);


--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE comments_id_seq OWNED BY comments.id;


--
-- Name: configurations; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE configurations (
    id integer NOT NULL,
    site_title character varying(200) NOT NULL,
    customer_name character varying(200) NOT NULL,
    trash_can_limit bigint NOT NULL,
    cdn_url character varying(200) NOT NULL,
    default_language bigint NOT NULL,
    start_page bigint,
    publish_twitter smallint NOT NULL,
    publish_facebook smallint NOT NULL,
    publish_linkedin smallint NOT NULL,
    page_keywords text NOT NULL,
    image_alt_text character varying(200) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    default_layout bigint,
    default_access bigint NOT NULL,
    default_page_access bigint NOT NULL,
    page_title bigint NOT NULL,
    page_description bigint NOT NULL,
    site_description text
);


--
-- Name: configurations_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE configurations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: configurations_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE configurations_id_seq OWNED BY configurations.id;


--
-- Name: data_attributes; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE data_attributes (
    id integer NOT NULL,
    definition_id bigint NOT NULL,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    type character varying(200) NOT NULL,
    rules text,
    default_value text,
    required smallint,
    validation text,
    sort bigint DEFAULT 1::bigint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: data_attributes_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE data_attributes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: data_attributes_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE data_attributes_id_seq OWNED BY data_attributes.id;


--
-- Name: data_definitions; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE data_definitions (
    id integer NOT NULL,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    description text,
    api smallint DEFAULT 1::smallint NOT NULL,
    administrable smallint DEFAULT 0::smallint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    author_id bigint NOT NULL
);


--
-- Name: data_definitions_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE data_definitions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: data_definitions_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE data_definitions_id_seq OWNED BY data_definitions.id;


--
-- Name: data_items; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE data_items (
    id integer NOT NULL,
    definition_id bigint NOT NULL,
    deleted_at timestamp without time zone,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    content json
);


--
-- Name: data_items_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE data_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: data_items_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE data_items_id_seq OWNED BY data_items.id;


--
-- Name: element_instances; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE element_instances (
    id integer NOT NULL,
    element_id bigint NOT NULL,
    layout_id bigint,
    template_id bigint,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: element_instances_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE element_instances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: element_instances_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE element_instances_id_seq OWNED BY element_instances.id;


--
-- Name: elements; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE elements (
    id integer NOT NULL,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    description character varying(200),
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    author_id bigint NOT NULL
);


--
-- Name: elements_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE elements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: elements_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE elements_id_seq OWNED BY elements.id;


--
-- Name: folders; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE folders (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    trash smallint DEFAULT 0::smallint NOT NULL,
    parent_id bigint,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: folders_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE folders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: folders_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE folders_id_seq OWNED BY folders.id;


--
-- Name: help; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE help (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    type character varying(200) NOT NULL,
    data character varying(200) NOT NULL,
    parent_id bigint,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    icon character varying(200) DEFAULT ''::character varying NOT NULL,
    disabled smallint DEFAULT 0::smallint NOT NULL
);


--
-- Name: help_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE help_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: help_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE help_id_seq OWNED BY help.id;


--
-- Name: keyword_collections; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE keyword_collections (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: keyword_collections_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE keyword_collections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: keyword_collections_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE keyword_collections_id_seq OWNED BY keyword_collections.id;


--
-- Name: keyword_page; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE keyword_page (
    id integer NOT NULL,
    keyword_id bigint NOT NULL,
    page_id bigint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: keyword_page_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE keyword_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: keyword_page_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE keyword_page_id_seq OWNED BY keyword_page.id;


--
-- Name: keywords; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE keywords (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    description text,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    collection_id bigint NOT NULL
);


--
-- Name: keywords_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE keywords_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: keywords_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE keywords_id_seq OWNED BY keywords.id;


--
-- Name: languages; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE languages (
    id integer NOT NULL,
    code character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    term_copy character varying(200) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    flag character varying(10),
    dictionary character varying(50) NOT NULL
);


--
-- Name: languages_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE languages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: languages_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE languages_id_seq OWNED BY languages.id;


--
-- Name: layouts; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE layouts (
    id integer NOT NULL,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    description character varying(200),
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    author_id bigint NOT NULL
);


--
-- Name: layouts_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE layouts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: layouts_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE layouts_id_seq OWNED BY layouts.id;


--
-- Name: likes; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE likes (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    page_id bigint NOT NULL,
    language_id bigint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    comment_id bigint
);


--
-- Name: likes_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE likes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: likes_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE likes_id_seq OWNED BY likes.id;


--
-- Name: media; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE media (
    id integer NOT NULL,
    title character varying(200) NOT NULL,
    filename character varying(260) NOT NULL,
    description text,
    type character varying(200) NOT NULL,
    size bigint NOT NULL,
    width bigint,
    height bigint,
    uploaded_at timestamp(0) without time zone,
    expires_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    folder_id bigint,
    trash smallint DEFAULT 0::smallint NOT NULL,
    image smallint DEFAULT 0::smallint NOT NULL,
    extension character varying(32) NOT NULL,
    path character varying(200),
    language_id bigint,
    index smallint DEFAULT 1::smallint NOT NULL,
    author_id bigint NOT NULL
);


--
-- Name: media_exports; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE media_exports (
    id integer NOT NULL,
    media_id bigint NOT NULL,
    token character varying(23) NOT NULL,
    used smallint DEFAULT 0::smallint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: media_exports_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE media_exports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_exports_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE media_exports_id_seq OWNED BY media_exports.id;


--
-- Name: media_extracts; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE media_extracts (
    id integer NOT NULL,
    media_id bigint NOT NULL,
    content text
);


--
-- Name: media_extracts_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE media_extracts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_extracts_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE media_extracts_id_seq OWNED BY media_extracts.id;


--
-- Name: media_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE media_id_seq OWNED BY media.id;


--
-- Name: media_index; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE media_index (
    id integer NOT NULL,
    media_id bigint NOT NULL,
    phrase tsvector NOT NULL
);


--
-- Name: media_index_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE media_index_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_index_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE media_index_id_seq OWNED BY media_index.id;


--
-- Name: media_translation; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE media_translation (
    id integer NOT NULL,
    media_id bigint NOT NULL,
    translation_id bigint NOT NULL,
    keyword character varying(200) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: media_translation_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE media_translation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_translation_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE media_translation_id_seq OWNED BY media_translation.id;


--
-- Name: menu_page; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE menu_page (
    id integer NOT NULL,
    menu_id bigint NOT NULL,
    page_id bigint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: menu_page_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE menu_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: menu_page_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE menu_page_id_seq OWNED BY menu_page.id;


--
-- Name: menus; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE menus (
    id integer NOT NULL,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    description character varying(200),
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    author_id bigint NOT NULL
);


--
-- Name: menus_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE menus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: menus_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE menus_id_seq OWNED BY menus.id;


--
-- Name: pages; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE pages (
    id integer NOT NULL,
    template_id bigint NOT NULL,
    parent_id bigint,
    status bigint DEFAULT 1 NOT NULL,
    trash smallint DEFAULT 0::smallint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    sort bigint,
    access bigint DEFAULT 1::bigint NOT NULL,
    notes text,
    redirect_url text,
    redirect_new_window smallint DEFAULT 0::smallint NOT NULL,
    index smallint DEFAULT 1::smallint NOT NULL,
    author_id bigint NOT NULL
);


--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pages_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE pages_id_seq OWNED BY pages.id;


--
-- Name: pages_subtree; Type: VIEW; Schema: cms; Owner: -
--

CREATE VIEW pages_subtree AS
 WITH RECURSIVE tree(id, template_id, parent_id, status, trash, created_at, updated_at, sort, access, notes, redirect_url, redirect_new_window, index, author_id) AS (
         SELECT p.id,
            p.template_id,
            p.parent_id,
            p.status,
            p.trash,
            p.created_at,
            p.updated_at,
            p.sort,
            p.access,
            p.notes,
            p.redirect_url,
            p.redirect_new_window,
            p.index,
            p.author_id
           FROM pages p
          WHERE (p.parent_id = (current_setting('cms.current_root'::text))::bigint)
        UNION ALL
         SELECT p.id,
            p.template_id,
            p.parent_id,
            p.status,
            p.trash,
            p.created_at,
            p.updated_at,
            p.sort,
            p.access,
            p.notes,
            p.redirect_url,
            p.redirect_new_window,
            p.index,
            p.author_id
           FROM tree t,
            pages p
          WHERE (p.parent_id = t.id)
        )
 SELECT tree.id,
    tree.template_id,
    tree.parent_id,
    tree.status,
    tree.trash,
    tree.created_at,
    tree.updated_at,
    tree.sort,
    tree.access,
    tree.notes,
    tree.redirect_url,
    tree.redirect_new_window,
    tree.index,
    tree.author_id
   FROM tree;


--
-- Name: parameter_values; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE parameter_values (
    id integer NOT NULL,
    parameter_id bigint NOT NULL,
    element_instance_id bigint NOT NULL,
    value character varying(200) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: parameter_values_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE parameter_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parameter_values_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE parameter_values_id_seq OWNED BY parameter_values.id;


--
-- Name: parameters; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE parameters (
    id integer NOT NULL,
    element_id bigint NOT NULL,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    type character varying(200) NOT NULL,
    default_value character varying(200),
    editable smallint,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: parameters_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE parameters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: parameters_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE parameters_id_seq OWNED BY parameters.id;


--
-- Name: permission_categories; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE permission_categories (
    id integer NOT NULL,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    description text,
    builtin smallint DEFAULT 0::smallint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: permission_categories_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE permission_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: permission_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE permission_categories_id_seq OWNED BY permission_categories.id;


--
-- Name: permission_role; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE permission_role (
    id integer NOT NULL,
    role_id bigint NOT NULL,
    permission_id bigint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: permission_role_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE permission_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: permission_role_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE permission_role_id_seq OWNED BY permission_role.id;


--
-- Name: permissions; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE permissions (
    id integer NOT NULL,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    category_id bigint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE permissions_id_seq OWNED BY permissions.id;


--
-- Name: profiles; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE profiles (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    language character varying(6) DEFAULT 'en'::character varying NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: profiles_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE profiles_id_seq OWNED BY profiles.id;


--
-- Name: properties; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE properties (
    id integer NOT NULL,
    template_id bigint NOT NULL,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    type character varying(200) NOT NULL,
    rules text,
    default_value text,
    required smallint,
    validation text,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    sort bigint DEFAULT 1::bigint NOT NULL,
    index smallint DEFAULT 1::smallint NOT NULL
);


--
-- Name: properties_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE properties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: properties_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE properties_id_seq OWNED BY properties.id;


--
-- Name: related_links; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE related_links (
    id integer NOT NULL,
    page_id bigint NOT NULL,
    title character varying(200) NOT NULL,
    url character varying(400) NOT NULL,
    new_window smallint DEFAULT 0 NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: related_links_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE related_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: related_links_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE related_links_id_seq OWNED BY related_links.id;


--
-- Name: related_media; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE related_media (
    id integer NOT NULL,
    page_id bigint NOT NULL,
    media_id bigint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: related_media_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE related_media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: related_media_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE related_media_id_seq OWNED BY related_media.id;


--
-- Name: related_pages; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE related_pages (
    id integer NOT NULL,
    page_id bigint NOT NULL,
    related_id bigint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: related_pages_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE related_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: related_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE related_pages_id_seq OWNED BY related_pages.id;


--
-- Name: role_user; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE role_user (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    role_id bigint NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: role_user_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE role_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: role_user_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE role_user_id_seq OWNED BY role_user.id;


--
-- Name: roles; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    builtin smallint DEFAULT 0::smallint NOT NULL
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: search_index; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE search_index (
    id integer NOT NULL,
    translation_id bigint NOT NULL,
    phrase tsvector NOT NULL
);


--
-- Name: search_index_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE search_index_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_index_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE search_index_id_seq OWNED BY search_index.id;


--
-- Name: templates; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE templates (
    id integer NOT NULL,
    keyword character varying(200) NOT NULL,
    name character varying(200) NOT NULL,
    description character varying(200),
    summarizer text,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    default_access bigint,
    author_hide text DEFAULT '0'::text NOT NULL,
    content_type character varying(200),
    index smallint DEFAULT 1::smallint NOT NULL,
    author_id bigint NOT NULL
);


--
-- Name: templates_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE templates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: templates_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE templates_id_seq OWNED BY templates.id;


--
-- Name: translations; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE translations (
    id integer NOT NULL,
    page_id bigint NOT NULL,
    language_id bigint NOT NULL,
    subject text NOT NULL,
    slug text NOT NULL,
    summary text,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    keywords text,
    description text,
    title character varying(200),
    content json
);


--
-- Name: translated_pages; Type: VIEW; Schema: cms; Owner: -
--

CREATE VIEW translated_pages AS
 SELECT pages.id,
    pages.template_id,
    pages.parent_id,
    pages.status,
    pages.trash,
    pages.created_at,
    pages.updated_at,
    pages.sort,
    pages.access,
    pages.notes,
    pages.redirect_url,
    pages.redirect_new_window,
    pages.index,
    pages.author_id,
    translations.id AS translation_id,
    translations.created_at AS translation_created_at,
    translations.updated_at AS translation_updated_at,
    translations.language_id,
    translations.subject,
    translations.slug,
    translations.summary,
    translations.keywords,
    translations.description,
    translations.title,
    translations.content,
    f_geturlforpage((pages.id)::bigint, translations.language_id, pages.parent_id, translations.slug, (current_setting('cms.current_root'::text))::bigint) AS uri
   FROM (pages
     JOIN translations ON ((translations.page_id = pages.id)))
  WHERE (((pages.trash = 0) AND (pages.status = 2)) AND (translations.language_id = (current_setting('cms.current_language'::text))::bigint));


--
-- Name: translated_pages_preview; Type: VIEW; Schema: cms; Owner: -
--

CREATE VIEW translated_pages_preview AS
 SELECT pages.id,
    pages.template_id,
    pages.parent_id,
    pages.status,
    pages.trash,
    pages.created_at,
    pages.updated_at,
    pages.sort,
    pages.access,
    pages.notes,
    pages.redirect_url,
    pages.redirect_new_window,
    pages.index,
    pages.author_id,
    translations.id AS translation_id,
    translations.created_at AS translation_created_at,
    translations.updated_at AS translation_updated_at,
    translations.language_id,
    translations.subject,
    translations.slug,
    translations.summary,
    translations.keywords,
    translations.description,
    translations.title,
    translations.content,
    f_geturlforpage((pages.id)::bigint, translations.language_id, pages.parent_id, translations.slug, (current_setting('cms.current_root'::text))::bigint) AS uri
   FROM (pages
     JOIN translations ON ((translations.page_id = pages.id)))
  WHERE (translations.language_id = (current_setting('cms.current_language'::text))::bigint);


--
-- Name: translated_pages_subtree; Type: VIEW; Schema: cms; Owner: -
--

CREATE VIEW translated_pages_subtree AS
 WITH RECURSIVE tree(id, template_id, parent_id, status, trash, created_at, updated_at, sort, access, notes, redirect_url, redirect_new_window, index, author_id, translation_id, translation_created_at, translation_updated_at, language_id, subject, slug, summary, keywords, description, title, content, uri) AS (
         SELECT p.id,
            p.template_id,
            p.parent_id,
            p.status,
            p.trash,
            p.created_at,
            p.updated_at,
            p.sort,
            p.access,
            p.notes,
            p.redirect_url,
            p.redirect_new_window,
            p.index,
            p.author_id,
            p.translation_id,
            p.translation_created_at,
            p.translation_updated_at,
            p.language_id,
            p.subject,
            p.slug,
            p.summary,
            p.keywords,
            p.description,
            p.title,
            p.content,
            p.uri
           FROM translated_pages p
          WHERE (p.parent_id = (current_setting('cms.current_root'::text))::bigint)
        UNION ALL
         SELECT p.id,
            p.template_id,
            p.parent_id,
            p.status,
            p.trash,
            p.created_at,
            p.updated_at,
            p.sort,
            p.access,
            p.notes,
            p.redirect_url,
            p.redirect_new_window,
            p.index,
            p.author_id,
            p.translation_id,
            p.translation_created_at,
            p.translation_updated_at,
            p.language_id,
            p.subject,
            p.slug,
            p.summary,
            p.keywords,
            p.description,
            p.title,
            p.content,
            p.uri
           FROM tree t,
            translated_pages p
          WHERE (p.parent_id = t.id)
        )
 SELECT tree.id,
    tree.template_id,
    tree.parent_id,
    tree.status,
    tree.trash,
    tree.created_at,
    tree.updated_at,
    tree.sort,
    tree.access,
    tree.notes,
    tree.redirect_url,
    tree.redirect_new_window,
    tree.index,
    tree.author_id,
    tree.translation_id,
    tree.translation_created_at,
    tree.translation_updated_at,
    tree.language_id,
    tree.subject,
    tree.slug,
    tree.summary,
    tree.keywords,
    tree.description,
    tree.title,
    tree.content,
    tree.uri
   FROM tree;


--
-- Name: translations_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE translations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: translations_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE translations_id_seq OWNED BY translations.id;


--
-- Name: users; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    firstname character varying(255) NOT NULL,
    middlename character varying(255),
    lastname character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    phone character varying(200),
    password character varying(50),
    deleted_at timestamp without time zone,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: variations; Type: TABLE; Schema: cms; Owner: -; Tablespace: 
--

CREATE TABLE variations (
    id integer NOT NULL,
    media_id bigint NOT NULL,
    width bigint NOT NULL,
    height bigint NOT NULL,
    size bigint,
    filename character varying(200) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: variations_id_seq; Type: SEQUENCE; Schema: cms; Owner: -
--

CREATE SEQUENCE variations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: variations_id_seq; Type: SEQUENCE OWNED BY; Schema: cms; Owner: -
--

ALTER SEQUENCE variations_id_seq OWNED BY variations.id;


SET search_path = custom, pg_catalog;

--
-- Name: employees; Type: TABLE; Schema: custom; Owner: -; Tablespace: 
--

CREATE TABLE employees (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    addresse character varying(200),
    zipcode character varying(200),
    city character varying(200),
    "position" character varying(200),
    department character varying(200),
    birthdate character varying(200),
    info text,
    img character varying(200),
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: employees_id_seq; Type: SEQUENCE; Schema: custom; Owner: -
--

CREATE SEQUENCE employees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: employees_id_seq; Type: SEQUENCE OWNED BY; Schema: custom; Owner: -
--

ALTER SEQUENCE employees_id_seq OWNED BY employees.id;


--
-- Name: groupmembers; Type: TABLE; Schema: custom; Owner: -; Tablespace: 
--

CREATE TABLE groupmembers (
    id integer NOT NULL,
    page_id bigint NOT NULL,
    user_id bigint NOT NULL,
    admin bigint,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: groupmembers_id_seq; Type: SEQUENCE; Schema: custom; Owner: -
--

CREATE SEQUENCE groupmembers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: groupmembers_id_seq; Type: SEQUENCE OWNED BY; Schema: custom; Owner: -
--

ALTER SEQUENCE groupmembers_id_seq OWNED BY groupmembers.id;


SET search_path = public, pg_catalog;

--
-- Name: cas_sessions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE cas_sessions (
    id integer NOT NULL,
    tgt character varying(255),
    username character varying(255),
    ip_address character varying(25),
    open_date timestamp with time zone
);


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE failed_jobs (
    id integer NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    failed_at timestamp without time zone NOT NULL
);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE failed_jobs_id_seq OWNED BY failed_jobs.id;


--
-- Name: laravel_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE laravel_migrations (
    bundle character varying(50) NOT NULL,
    name character varying(200) NOT NULL,
    batch bigint NOT NULL
);


--
-- Name: locks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE locks (
    application_id character varying(255) NOT NULL,
    expiration_date timestamp without time zone,
    unique_id character varying(255)
);


--
-- Name: person; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE person (
    id integer NOT NULL,
    department_id integer,
    company_id integer,
    firstname character varying(255),
    middlename character varying(255),
    lastname character varying(255),
    job_title character varying(200),
    persont_status_type_id integer,
    is_external boolean DEFAULT true NOT NULL,
    is_history boolean DEFAULT false NOT NULL,
    is_male boolean,
    email character varying(255),
    is_trash boolean DEFAULT false NOT NULL,
    is_restricted boolean DEFAULT false NOT NULL,
    owner_id integer,
    imported_responsible character varying(40),
    import_id integer,
    contact_id character varying(100),
    created_date timestamp with time zone NOT NULL,
    modified_date timestamp with time zone NOT NULL,
    old_id character varying(255)
);


--
-- Name: serviceticket; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE serviceticket (
    id character varying(255) NOT NULL,
    number_of_times_used integer,
    creation_time bigint,
    expiration_policy oid NOT NULL,
    last_time_used bigint,
    previous_last_time_used bigint,
    from_new_login boolean NOT NULL,
    ticket_already_granted boolean NOT NULL,
    service oid NOT NULL,
    ticketgrantingticket_id character varying(255)
);


--
-- Name: session_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE session_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: suite_properties; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE suite_properties (
    id integer NOT NULL,
    property_name character varying(100) NOT NULL,
    property_value oid
);


--
-- Name: symfoni_scheduler; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE symfoni_scheduler (
    id integer NOT NULL,
    hash character varying(32) NOT NULL,
    "timestamp" timestamp(0) without time zone
);


--
-- Name: symfoni_scheduler_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE symfoni_scheduler_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: symfoni_scheduler_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE symfoni_scheduler_id_seq OWNED BY symfoni_scheduler.id;


--
-- Name: ticketgrantingticket; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ticketgrantingticket (
    id character varying(255) NOT NULL,
    number_of_times_used integer,
    creation_time bigint,
    expiration_policy oid NOT NULL,
    last_time_used bigint,
    previous_last_time_used bigint,
    authentication oid NOT NULL,
    expired boolean NOT NULL,
    services_granted_access_to oid NOT NULL,
    ticketgrantingticket_id character varying(255)
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    login character varying(255) NOT NULL,
    password character varying(50),
    id integer NOT NULL,
    is_enabled boolean DEFAULT false NOT NULL,
    created_date date,
    email character varying(255),
    private_folder_id integer
);


SET search_path = cms, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY channels ALTER COLUMN id SET DEFAULT nextval('channels_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY comments ALTER COLUMN id SET DEFAULT nextval('comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY configurations ALTER COLUMN id SET DEFAULT nextval('configurations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY data_attributes ALTER COLUMN id SET DEFAULT nextval('data_attributes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY data_definitions ALTER COLUMN id SET DEFAULT nextval('data_definitions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY data_items ALTER COLUMN id SET DEFAULT nextval('data_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY element_instances ALTER COLUMN id SET DEFAULT nextval('element_instances_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY elements ALTER COLUMN id SET DEFAULT nextval('elements_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY folders ALTER COLUMN id SET DEFAULT nextval('folders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY help ALTER COLUMN id SET DEFAULT nextval('help_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY keyword_collections ALTER COLUMN id SET DEFAULT nextval('keyword_collections_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY keyword_page ALTER COLUMN id SET DEFAULT nextval('keyword_page_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY keywords ALTER COLUMN id SET DEFAULT nextval('keywords_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY languages ALTER COLUMN id SET DEFAULT nextval('languages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY layouts ALTER COLUMN id SET DEFAULT nextval('layouts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY likes ALTER COLUMN id SET DEFAULT nextval('likes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media ALTER COLUMN id SET DEFAULT nextval('media_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media_exports ALTER COLUMN id SET DEFAULT nextval('media_exports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media_extracts ALTER COLUMN id SET DEFAULT nextval('media_extracts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media_index ALTER COLUMN id SET DEFAULT nextval('media_index_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media_translation ALTER COLUMN id SET DEFAULT nextval('media_translation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY menu_page ALTER COLUMN id SET DEFAULT nextval('menu_page_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY menus ALTER COLUMN id SET DEFAULT nextval('menus_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY pages ALTER COLUMN id SET DEFAULT nextval('pages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY parameter_values ALTER COLUMN id SET DEFAULT nextval('parameter_values_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY parameters ALTER COLUMN id SET DEFAULT nextval('parameters_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY permission_categories ALTER COLUMN id SET DEFAULT nextval('permission_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY permission_role ALTER COLUMN id SET DEFAULT nextval('permission_role_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY permissions ALTER COLUMN id SET DEFAULT nextval('permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY profiles ALTER COLUMN id SET DEFAULT nextval('profiles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY properties ALTER COLUMN id SET DEFAULT nextval('properties_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY related_links ALTER COLUMN id SET DEFAULT nextval('related_links_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY related_media ALTER COLUMN id SET DEFAULT nextval('related_media_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY related_pages ALTER COLUMN id SET DEFAULT nextval('related_pages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY role_user ALTER COLUMN id SET DEFAULT nextval('role_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY search_index ALTER COLUMN id SET DEFAULT nextval('search_index_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY templates ALTER COLUMN id SET DEFAULT nextval('templates_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY translations ALTER COLUMN id SET DEFAULT nextval('translations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: cms; Owner: -
--

ALTER TABLE ONLY variations ALTER COLUMN id SET DEFAULT nextval('variations_id_seq'::regclass);


SET search_path = custom, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: custom; Owner: -
--

ALTER TABLE ONLY employees ALTER COLUMN id SET DEFAULT nextval('employees_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: custom; Owner: -
--

ALTER TABLE ONLY groupmembers ALTER COLUMN id SET DEFAULT nextval('groupmembers_id_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY failed_jobs ALTER COLUMN id SET DEFAULT nextval('failed_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY symfoni_scheduler ALTER COLUMN id SET DEFAULT nextval('symfoni_scheduler_id_seq'::regclass);


--
-- Name: 75182; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('75182');


--
-- Name: 76350; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76350');


--
-- Name: 76351; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76351');


--
-- Name: 76352; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76352');


--
-- Name: 76385; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76385');


--
-- Name: 76386; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76386');


--
-- Name: 76387; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76387');


--
-- Name: 76395; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76395');


--
-- Name: 76396; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76396');


--
-- Name: 76397; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76397');


--
-- Name: 76405; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76405');


--
-- Name: 76406; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76406');


--
-- Name: 76407; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76407');


--
-- Name: 76435; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76435');


--
-- Name: 76436; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76436');


--
-- Name: 76437; Type: BLOB; Schema: -; Owner: -
--

SELECT pg_catalog.lo_create('76437');


SET search_path = cms, pg_catalog;

--
-- Data for Name: channels; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY channels (id, name, description, type, criteria, active, layout_id, language_id, root_id, start_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: channels_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('channels_id_seq', 1, false);


--
-- Data for Name: comments; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY comments (id, user_id, page_id, language_id, comment, edited, created_at, updated_at, parent_id) FROM stdin;
1	1	50	2	Her kommer comment	0	2014-12-15 16:45:37	2014-12-15 16:45:37	\N
2	1	50	2	Enda en kommentar	0	2014-12-15 16:52:50	2014-12-15 16:52:50	\N
3	4	50	2	Hva skjer'a?	0	2014-12-16 12:31:48	2014-12-16 12:31:48	\N
4	4	50	2	Blir det oppdatert no då?	0	2014-12-16 12:36:17	2014-12-16 12:36:17	\N
5	4	50	2	enda en oppdaterimng :)	0	2014-12-16 12:37:57	2014-12-16 12:37:57	\N
6	4	49	2	Det blir sykling hele jula, vinteren er avlyst!	0	2014-12-16 12:42:57	2014-12-16 12:42:57	\N
7	4	49	2	gsdfgsd	0	2014-12-16 12:44:39	2014-12-16 12:44:39	\N
8	4	49	2	Nei, eg vil ake!!	0	2014-12-16 13:07:16	2014-12-16 13:07:16	6
9	4	49	2	Eum id tota deterruisset, clita reprimique eos ea, ius et zril ornatus epicurei. Mea exerci dolores imperdiet at. Prima adversarium mea no. Ei quem debet impetus vis, eos mazim feugiat ei. Et dolore aliquid accusam duo, etiam utroque nominati quo at.\r\n\r\nNonumy prompta omittantur pro ex, vel ipsum officiis philosophia eu. Mel et officiis efficiantur theophrastus, ex illum iusto dolorum usu, at ferri fabellas constituam usu. Molestiae consectetuer est te. Ne agam magna oportere per. Sumo consul in eum, nominavi torquatos maiestatis ad duo.	0	2014-12-17 11:23:37	2014-12-17 11:23:37	\N
10	4	49	2	Glemte å nevne at prompta omittantur pro ex	0	2014-12-17 11:24:38	2014-12-17 11:24:38	9
11	4	49	2	Reque insolens signiferumque et mea, pri dolor mucius ex, quo at sint vero. Cetero saperet ne sit, eius philosophia necessitatibus no has. Pro ei euismod eruditi repudiare. His ex vitae graecis petentium. An inani intellegat efficiantur quo, no vix legimus albucius adversarium. Ad elitr aliquam vim. Ius in civibus facilisi.\r\n	0	2014-12-17 11:34:30	2014-12-17 11:34:30	9
12	4	49	2	Min kommentar her...	0	2014-12-17 12:04:56	2014-12-17 12:04:56	7
13	4	49	2	Din kommentar...	0	2014-12-17 12:05:39	2014-12-17 12:05:39	7
14	4	49	2	bananer i pysjamas	0	2014-12-17 12:07:17	2014-12-17 12:07:17	7
15	4	49	2	gsdfsd	0	2014-12-17 12:08:16	2014-12-17 12:08:16	7
16	4	49	2	gsgsd	0	2014-12-17 12:12:44	2014-12-17 12:12:44	9
17	4	49	2	yhdtgdfgdf	0	2014-12-17 12:13:14	2014-12-17 12:13:14	\N
18	4	49	2	hgsffgsd	0	2014-12-17 12:13:52	2014-12-17 12:13:52	\N
19	4	49	2	gsdfsd	0	2014-12-17 12:18:13	2014-12-17 12:18:13	18
20	4	49	2	2fgdgdfgdf	0	2014-12-17 12:18:54	2014-12-17 12:18:54	18
21	4	49	2	dssa	0	2014-12-17 12:19:22	2014-12-17 12:19:22	18
22	4	49	2	3 trftgretre	0	2014-12-17 12:20:55	2014-12-17 12:20:55	18
23	2	49	2	hmm\r\nErat error in cum, feugiat meliore eos an. Errem dolor dissentias vix no, vim ponderum repudiandae te. Fierent oportere elaboraret sed te, nonumy partem constituam te eos. Officiis probatus quo in, wisi nihil homero ex cum. Utinam mnesarchum at eum, eu eam quem nominati definitiones, nisl principes posidonium cu pri.	0	2014-12-17 12:23:47	2014-12-17 12:23:47	18
24	2	49	2	Nonumy prompta omittantur pro ex, vel ipsum officiis philosophia eu. Mel et officiis efficiantur theophrastus, ex illum iusto dolorum usu, at ferri fabellas constituam usu. Molestiae consectetuer est te. Ne agam magna oportere per. Sumo consul in eum, nominavi torquatos maiestatis ad duo.\r\n	0	2014-12-17 12:23:58	2014-12-17 12:23:58	\N
25	4	58	2	Husk at finalen i det store norkse symesterskapet er i kveld!!	0	2014-12-22 16:06:14	2014-12-22 16:06:14	\N
26	2	58	2	Anne vant!!	0	2014-12-23 12:03:23	2014-12-23 12:03:23	25
27	3	68	2	Øl er sundt året rundt	0	2014-12-23 12:22:33	2014-12-23 12:22:33	\N
28	2	68	2	Så rett så rett :)	0	2014-12-23 13:17:05	2014-12-23 13:17:05	27
29	2	82	2	Velkommen til OM og Anne :)	0	2014-12-23 13:20:53	2014-12-23 13:20:53	\N
30	2	50	2	Trikke dagen lang	0	2014-12-23 13:55:37	2014-12-23 13:55:37	5
\.


--
-- Name: comments_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('comments_id_seq', 30, true);


--
-- Data for Name: configurations; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY configurations (id, site_title, customer_name, trash_can_limit, cdn_url, default_language, start_page, publish_twitter, publish_facebook, publish_linkedin, page_keywords, image_alt_text, created_at, updated_at, default_layout, default_access, default_page_access, page_title, page_description, site_description) FROM stdin;
1	NGI sannkassen	NGI	24	https://cdn.symfoni.com	2	10	1	1	1			2014-12-10 12:48:18	2014-12-17 14:07:37	1	4	1	0	0	\N
\.


--
-- Name: configurations_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('configurations_id_seq', 1, true);


--
-- Data for Name: data_attributes; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY data_attributes (id, definition_id, keyword, name, type, rules, default_value, required, validation, sort, created_at, updated_at) FROM stdin;
\.


--
-- Name: data_attributes_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('data_attributes_id_seq', 1, false);


--
-- Data for Name: data_definitions; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY data_definitions (id, keyword, name, description, api, administrable, created_at, updated_at, author_id) FROM stdin;
\.


--
-- Name: data_definitions_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('data_definitions_id_seq', 1, false);


--
-- Data for Name: data_items; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY data_items (id, definition_id, deleted_at, created_at, updated_at, content) FROM stdin;
\.


--
-- Name: data_items_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('data_items_id_seq', 1, false);


--
-- Data for Name: element_instances; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY element_instances (id, element_id, layout_id, template_id, keyword, name, created_at, updated_at) FROM stdin;
1	1	\N	1	newsflash	Newsflashes	2014-12-10 12:48:19	2014-12-10 12:48:19
3	2	\N	6	xmascal	xmasCalendar	2014-12-11 15:27:01	2014-12-11 15:27:01
4	3	\N	8	newslist	Newslist	2014-12-12 11:24:42	2014-12-12 11:24:42
5	4	\N	6	birthdays	Birthdays	2014-12-12 11:59:51	2014-12-12 11:59:51
6	5	\N	9	gallery	Gallery	2014-12-12 14:10:06	2014-12-12 14:10:06
7	5	\N	6	gallery	Gallery	2014-12-12 14:30:09	2014-12-12 14:30:09
8	7	\N	6	mymessages	Mine meldinger	2014-12-12 15:17:31	2014-12-12 15:17:31
11	8	\N	12	xmaswindow	xmasWindow	2014-12-15 10:53:46	2014-12-15 10:53:46
12	2	\N	12	xmascal	xmasCalendar	2014-12-15 11:08:14	2014-12-15 11:08:14
14	10	\N	13	groups_mine	Groups mine	2014-12-15 12:05:23	2014-12-15 12:05:23
15	9	\N	13	groups_all	Groups all	2014-12-15 12:05:23	2014-12-15 12:05:23
17	6	\N	6	calendarstartpage	Calendar startpage	2014-12-15 12:34:17	2014-12-15 12:34:17
18	10	\N	6	groups_mine	Groups mine	2014-12-15 13:13:52	2014-12-15 13:13:52
20	12	\N	14	comments	Comments	2014-12-15 13:48:53	2014-12-15 13:48:53
21	10	\N	14	groups_mine	Groups mine	2014-12-15 13:50:57	2014-12-15 13:50:57
22	13	\N	6	myincidents	My Incidents	2014-12-16 11:47:23	2014-12-16 11:48:41
23	14	\N	11	weather	Weather	2014-12-16 15:25:19	2014-12-16 15:25:19
24	15	\N	14	users	Users	2014-12-17 13:58:32	2014-12-17 13:58:32
25	16	\N	6	driftsmeldinger	Driftsmeldinger	2014-12-18 12:43:32	2014-12-18 12:43:32
26	17	\N	14	group_add_mylist	Group add to my list	2014-12-19 14:46:46	2014-12-19 14:46:46
27	14	\N	6	weather	Weather	2014-12-22 10:19:21	2014-12-22 10:19:21
28	18	\N	11	calendartwo	Calendar two	2014-12-22 12:28:19	2014-12-22 12:28:19
29	19	\N	15	my_info_edit	My info edit	2014-12-22 12:46:07	2014-12-22 12:46:07
30	10	\N	15	groups_mine	Groups mine	2014-12-22 12:46:07	2014-12-22 12:46:07
31	20	\N	14	groupmembers	Groupmembers	2014-12-22 15:51:44	2014-12-22 15:51:44
32	13	\N	15	myincidents	My Incidents	2014-12-22 16:34:13	2014-12-22 16:34:13
33	7	\N	15	mymessages	My messages	2014-12-22 16:34:13	2014-12-22 16:34:13
34	21	\N	15	comments_from_my_group	Comments from my group	2014-12-23 13:27:14	2014-12-23 13:27:14
35	21	\N	6	comments_from_my_groups	Comments from my groups	2014-12-23 13:52:48	2014-12-23 13:52:48
\.


--
-- Name: element_instances_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('element_instances_id_seq', 35, true);


--
-- Data for Name: elements; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY elements (id, keyword, name, description, created_at, updated_at, author_id) FROM stdin;
1	newsflash	Newsflash	\N	2014-12-10 12:48:19	2014-12-10 12:48:19	1
2	xmascal	xmasCalendar	xmas calendar, 24 windows	2014-12-11 15:16:44	2014-12-11 15:18:46	1
3	newslist	Newslist		2014-12-12 11:23:30	2014-12-12 11:23:30	1
4	birthdays	Birthdays		2014-12-12 11:38:26	2014-12-12 11:38:26	1
5	gallery	Gallery		2014-12-12 13:13:38	2014-12-12 13:13:38	1
7	mymessages	My messages		2014-12-12 15:03:36	2014-12-15 10:32:00	1
8	xmaswindow	xmasWindow		2014-12-15 10:46:29	2014-12-15 10:46:29	1
9	groups_all	Groups all	Lister alle åpne grupper	2014-12-15 11:59:24	2014-12-15 11:59:53	1
10	groups_mine	Groups mine	Lister kun mine grupper	2014-12-15 12:02:15	2014-12-15 12:02:22	1
6	calendarstartpage	Calendar startpage		2014-12-12 13:43:57	2014-12-15 12:32:23	3
11	calendarone	calendarone		2014-12-15 13:22:49	2014-12-15 13:22:49	3
12	comments	Comments	Kommentarer til artikkel	2014-12-15 13:44:09	2014-12-15 13:44:09	1
13	myincidents	My Incidents	Incidents from service-now	2014-12-16 11:42:39	2014-12-16 11:47:29	1
14	weather	Weather		2014-12-16 15:24:49	2014-12-16 15:24:49	3
15	users	Users		2014-12-17 13:53:55	2014-12-17 13:53:55	2
16	driftsmeldinger	Driftsmeldinger		2014-12-18 12:40:52	2014-12-18 12:40:52	1
17	group-add-mylist	Group add to my list		2014-12-19 14:38:02	2014-12-19 14:38:02	2
18	calendartwo	Calendar two		2014-12-22 12:27:36	2014-12-22 12:27:36	3
19	my_info_edit	My info edit		2014-12-22 12:31:57	2014-12-22 12:31:57	2
20	groupmembers	Groupmembers		2014-12-22 15:49:03	2014-12-22 15:49:03	2
21	comments_from_my_groups	Comments from my groups		2014-12-23 13:24:14	2014-12-23 13:50:31	2
\.


--
-- Name: elements_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('elements_id_seq', 21, true);


--
-- Data for Name: folders; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY folders (id, title, trash, parent_id, created_at, updated_at) FROM stdin;
1	Uploads	0	\N	2014-12-10 12:48:19	2014-12-10 12:48:19
2	Gallery	0	\N	2014-12-12 14:00:26	2014-12-12 14:00:26
\.


--
-- Name: folders_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('folders_id_seq', 4, true);


--
-- Data for Name: help; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY help (id, name, type, data, parent_id, created_at, updated_at, icon, disabled) FROM stdin;
1	Online Documentation...	folder		\N	2014-12-10 12:48:17	2014-12-10 12:48:17	book_go.png	0
4	Bug Reporting	url	https://symfoni.service-now.com/ssp	\N	2014-12-10 12:48:17	2014-12-10 12:48:17	bug.png	0
2	User Guide	url	/cms/docs/guide/	1	2014-12-10 12:48:17	2014-12-10 12:48:17	book.png	0
5	About Symfoni Sites	md	readme	\N	2014-12-10 12:48:17	2014-12-10 12:48:17	information.png	0
6	Release Notes	md	guide/Contents/changes	\N	2014-12-10 12:48:17	2014-12-10 12:48:17	note.png	0
7	Acknowledgements	md	guide/Contents/acknowledgements	\N	2014-12-10 12:48:17	2014-12-10 12:48:17	award_star_gold_3.png	0
3	Downloads	md	downloads	1	2014-12-10 12:48:17	2014-12-10 12:48:17	book_link.png	0
12	Advanced Wiki	url	https://developer.symfoni.com/wiki/	1	2014-12-10 12:48:18	2014-12-10 12:48:18	book_open.png	0
\.


--
-- Name: help_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('help_id_seq', 12, true);


--
-- Data for Name: keyword_collections; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY keyword_collections (id, name, created_at, updated_at) FROM stdin;
1	Page Keywords	2014-12-10 12:48:18	2014-12-10 12:48:18
\.


--
-- Name: keyword_collections_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('keyword_collections_id_seq', 1, true);


--
-- Data for Name: keyword_page; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY keyword_page (id, keyword_id, page_id, created_at, updated_at) FROM stdin;
4	3	86	2014-12-23 16:02:46	2014-12-23 16:02:46
5	3	87	2014-12-23 16:03:17	2014-12-23 16:03:17
6	2	88	2014-12-23 16:04:18	2014-12-23 16:04:18
7	4	89	2014-12-23 16:05:14	2014-12-23 16:05:14
\.


--
-- Name: keyword_page_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('keyword_page_id_seq', 7, true);


--
-- Data for Name: keywords; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY keywords (id, name, description, created_at, updated_at, collection_id) FROM stdin;
1	News	All pages that can be considered as newsworthy...	2014-12-10 12:48:19	2014-12-10 12:48:19	1
2	Nyheter Oslo		2014-12-23 16:00:20	2014-12-23 16:00:20	1
3	Nyheter Viktig		2014-12-23 16:01:35	2014-12-23 16:01:35	1
4	Nyheter Diverse		2014-12-23 16:01:46	2014-12-23 16:01:46	1
\.


--
-- Name: keywords_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('keywords_id_seq', 4, true);


--
-- Data for Name: languages; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY languages (id, code, name, term_copy, created_at, updated_at, flag, dictionary) FROM stdin;
1	en	English	Copy	2014-12-10 12:48:17	2014-12-10 12:48:17	gb	english
2	no	Norwegian	Kopi	2014-12-10 12:48:17	2014-12-10 12:48:17	no	norwegian
3	sv	Swedish	Kopia	2014-12-10 12:48:17	2014-12-10 12:48:17	se	swedish
\.


--
-- Name: languages_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('languages_id_seq', 3, true);


--
-- Data for Name: layouts; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY layouts (id, keyword, name, description, created_at, updated_at, author_id) FROM stdin;
1	default	Default Layout		2014-12-10 12:48:19	2014-12-12 11:49:39	1
\.


--
-- Name: layouts_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('layouts_id_seq', 1, true);


--
-- Data for Name: likes; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY likes (id, user_id, page_id, language_id, created_at, updated_at, comment_id) FROM stdin;
1	1	50	2	2014-12-15 15:01:23	2014-12-15 15:01:23	\N
2	1	50	2	2014-12-15 17:42:01	2014-12-15 17:42:01	1
3	1	50	2	2014-12-15 17:43:06	2014-12-15 17:43:06	2
4	4	50	2	2014-12-15 18:08:41	2014-12-15 18:08:41	2
5	4	50	2	2014-12-15 18:09:04	2014-12-15 18:09:04	1
6	4	50	2	2014-12-16 12:38:04	2014-12-16 12:38:04	5
7	4	49	2	2014-12-17 11:37:36	2014-12-17 11:37:36	9
8	4	49	2	2014-12-17 11:39:06	2014-12-17 11:39:06	\N
9	4	49	2	2014-12-17 11:41:59	2014-12-17 11:41:59	6
10	4	49	2	2014-12-17 11:47:13	2014-12-17 11:47:13	8
11	4	49	2	2014-12-17 12:21:06	2014-12-17 12:21:06	20
12	2	58	2	2014-12-23 12:03:27	2014-12-23 12:03:27	26
13	2	58	2	2014-12-23 12:03:28	2014-12-23 12:03:28	25
14	2	68	2	2014-12-23 13:16:55	2014-12-23 13:16:55	27
\.


--
-- Name: likes_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('likes_id_seq', 14, true);


--
-- Data for Name: media; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY media (id, title, filename, description, type, size, width, height, uploaded_at, expires_at, created_at, updated_at, folder_id, trash, image, extension, path, language_id, index, author_id) FROM stdin;
1	redrobin	redrobin	\N	image/jpeg	33277	300	375	2014-12-12 11:33:22	\N	2014-12-12 11:33:22	2014-12-12 11:33:22	\N	0	1	jpg	c/4ca4/238a/c4ca4238a0b923820dcc509a6f75849b/	\N	1	1
2	staff_avatar_profile	staff_avatar_profile	\N	image/jpeg	3850	300	225	2014-12-12 11:33:23	\N	2014-12-12 11:33:23	2014-12-12 11:33:23	\N	0	1	jpg	c/81e7/28d9/c81e728d9d4c2f636f067f89cc14862c/	\N	1	1
7	Desert	desert	\N	image/jpeg	845941	1024	768	2014-12-12 14:03:14	\N	2014-12-12 14:03:14	2014-12-12 14:03:27	2	0	1	jpg	8/f14e/45fc/8f14e45fceea167a5a36dedd4bea2543/	\N	1	1
8	Hydrangeas	hydrangeas	\N	image/jpeg	595284	1024	768	2014-12-12 14:03:15	\N	2014-12-12 14:03:15	2014-12-12 14:03:27	2	0	1	jpg	c/9f0f/895f/c9f0f895fb98ab9159f51fd0297e236d/	\N	1	1
6	Chrysanthemum	chrysanthemum	\N	image/jpeg	879394	1024	768	2014-12-12 14:03:13	\N	2014-12-12 14:03:13	2014-12-12 14:03:27	2	0	1	jpg	1/6790/91c5/1679091c5a880faf6fb5e6087eb1b2dc/	\N	1	1
11	Koala	koala	\N	image/jpeg	780831	1024	768	2014-12-12 14:03:17	\N	2014-12-12 14:03:17	2014-12-12 14:03:27	2	0	1	jpg	6/512b/d43d/6512bd43d9caa6e02c990b0a82652dca/	\N	1	1
10	Jellyfish	jellyfish	\N	image/jpeg	775702	1024	768	2014-12-12 14:03:16	\N	2014-12-12 14:03:16	2014-12-12 14:03:27	2	0	1	jpg	d/3d94/4680/d3d9446802a44259755d38e6d163e820/	\N	1	1
4	Penguins	penguins	\N	image/jpeg	777835	1024	768	2014-12-12 14:03:12	\N	2014-12-12 14:03:12	2014-12-12 14:03:27	2	0	1	jpg	a/87ff/679a/a87ff679a2f3e71d9181a67b7542122c/	\N	1	1
3	Lighthouse	lighthouse	\N	image/jpeg	561276	1024	768	2014-12-12 14:03:11	\N	2014-12-12 14:03:11	2014-12-12 14:03:27	2	0	1	jpg	e/ccbc/87e4/eccbc87e4b5ce2fe28308fd9f2a7baf3/	\N	1	1
5	Tulips	tulips	\N	image/jpeg	620888	1024	768	2014-12-12 14:03:12	\N	2014-12-12 14:03:12	2014-12-12 14:03:27	2	0	1	jpg	e/4da3/b7fb/e4da3b7fbbce2345d7772b0674a318d5/	\N	1	1
12	christmas_illustration	christmas_illustration	\N	image/png	95987	454	336	2014-12-15 11:20:28	\N	2014-12-15 11:20:28	2014-12-15 11:20:28	\N	0	1	png	c/20ad/4d76/c20ad4d76fe97759aa27a0c99bff6710/	\N	1	1
13	christmas_illustration2	christmas_illustration2	\N	image/png	77711	454	278	2014-12-15 11:24:46	\N	2014-12-15 11:24:46	2014-12-15 11:24:46	\N	0	1	png	c/51ce/410c/c51ce410c124a10e0db5e4b97fc2af39/	\N	1	1
14	theodor-11-12-2014	theodor-11-12-2014	\N	image/png	461192	934	740	2014-12-23 16:04:59	\N	2014-12-23 16:04:59	2014-12-23 16:04:59	\N	0	1	png	a/ab32/3892/aab3238922bcc25a6f606eb525ffdc56/	\N	1	2
\.


--
-- Data for Name: media_exports; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY media_exports (id, media_id, token, used, created_at, updated_at) FROM stdin;
1	12	548eb62b281a57.10024110	0	2014-12-15 11:21:31	2014-12-15 11:21:31
2	12	548eb64474b8c8.87684335	0	2014-12-15 11:21:56	2014-12-15 11:21:56
3	12	548eb67ef38548.75612585	0	2014-12-15 11:22:55	2014-12-15 11:22:55
4	2	548eb83ab097b4.52979743	0	2014-12-15 11:30:18	2014-12-15 11:30:18
5	2	548eb8548f1738.11290881	0	2014-12-15 11:30:44	2014-12-15 11:30:44
\.


--
-- Name: media_exports_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('media_exports_id_seq', 5, true);


--
-- Data for Name: media_extracts; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY media_extracts (id, media_id, content) FROM stdin;
\.


--
-- Name: media_extracts_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('media_extracts_id_seq', 1, false);


--
-- Name: media_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('media_id_seq', 14, true);


--
-- Data for Name: media_index; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY media_index (id, media_id, phrase) FROM stdin;
1	1	'redrobin':1,2
2	2	'avatar':2,5 'profile':3,6 'staff':1,4
7	7	'desert':1,2
8	8	'hydrangeas':1,2
6	6	'chrysanthemum':1,2
11	11	'koala':1,2
10	10	'jellyfish':1,2
4	4	'penguins':1,2
3	3	'lighthouse':1,2
5	5	'tulips':1,2
12	12	'christmas':1,3 'illustration':2,4
13	13	'christmas':1,3 'illustration2':2,4
14	14	'-11':2,6 '-12':3,7 '-2014':4,8 'theodor':1,5
\.


--
-- Name: media_index_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('media_index_id_seq', 14, true);


--
-- Data for Name: media_translation; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY media_translation (id, media_id, translation_id, keyword, created_at, updated_at) FROM stdin;
\.


--
-- Name: media_translation_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('media_translation_id_seq', 1, false);


--
-- Data for Name: menu_page; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY menu_page (id, menu_id, page_id, created_at, updated_at) FROM stdin;
8	1	8	2014-12-10 12:48:19	2014-12-10 12:48:19
9	1	9	2014-12-10 12:48:19	2014-12-10 12:48:19
10	1	11	2014-12-12 08:58:59	2014-12-12 08:58:59
11	1	85	2014-12-23 14:08:21	2014-12-23 14:08:21
\.


--
-- Name: menu_page_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('menu_page_id_seq', 11, true);


--
-- Data for Name: menus; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY menus (id, keyword, name, description, created_at, updated_at, author_id) FROM stdin;
1	main	Main Menu	\N	2014-12-10 12:48:19	2014-12-10 12:48:19	1
\.


--
-- Name: menus_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('menus_id_seq', 1, true);


--
-- Data for Name: pages; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY pages (id, template_id, parent_id, status, trash, created_at, updated_at, sort, access, notes, redirect_url, redirect_new_window, index, author_id) FROM stdin;
53	10	51	2	0	2014-12-15 12:47:23	2014-12-15 12:47:43	1	1			0	1	3
41	12	23	2	0	2014-12-15 11:00:34	2014-12-15 11:14:09	18	1			0	1	1
23	12	10	2	0	2014-12-15 10:44:26	2014-12-15 10:44:26	8	1			0	1	1
48	13	10	2	0	2014-12-15 12:06:48	2014-12-15 12:06:48	6	1			0	1	1
12	8	10	2	0	2014-12-12 11:25:21	2014-12-12 11:28:03	3	1			0	1	1
40	12	23	2	0	2014-12-15 11:00:34	2014-12-15 11:14:28	20	1			0	1	1
15	9	10	2	0	2014-12-12 14:24:58	2014-12-12 14:24:58	4	1			0	1	1
16	8	10	2	0	2014-12-12 15:17:55	2014-12-12 15:17:55	5	1			0	1	1
19	10	10	2	0	2014-12-15 08:55:11	2014-12-15 08:55:11	2	1			0	1	3
13	2	12	2	0	2014-12-12 11:31:13	2014-12-12 12:06:31	3	1			0	1	1
14	2	12	2	0	2014-12-12 11:33:54	2014-12-12 12:06:31	1	1			0	1	1
10	6	\N	2	0	2014-12-11 14:51:54	2014-12-12 14:24:04	6	1			0	1	1
9	5	\N	2	0	2014-12-10 12:48:19	2014-12-10 12:48:19	8	1	\N	\N	0	1	1
11	7	\N	2	0	2014-12-12 08:51:10	2014-12-12 08:58:48	12	1			0	1	3
8	4	\N	2	0	2014-12-10 12:48:19	2014-12-12 14:24:12	7	1	\N	\N	0	1	1
17	2	16	2	0	2014-12-12 15:18:21	2014-12-12 15:18:21	2	1			0	1	1
18	2	16	2	0	2014-12-12 15:18:46	2014-12-12 15:18:46	1	1			0	1	1
22	11	\N	2	0	2014-12-15 09:30:52	2014-12-15 09:31:08	13	1			0	1	3
24	12	23	2	0	2014-12-15 10:45:00	2014-12-15 10:45:09	1	1			0	1	1
20	10	62	2	0	2014-12-15 08:56:10	2014-12-22 09:35:42	1	1			0	1	3
21	10	62	2	0	2014-12-15 08:57:28	2014-12-22 09:37:15	2	1			0	1	3
63	10	19	2	0	2014-12-22 09:31:31	2014-12-22 10:00:18	2	1			0	1	3
38	12	23	2	0	2014-12-15 11:00:23	2014-12-15 11:15:24	24	1			0	1	1
36	12	23	2	0	2014-12-15 11:00:22	2014-12-15 11:25:41	30	1			0	1	1
34	12	23	2	0	2014-12-15 11:00:22	2014-12-15 11:25:52	32	1			0	1	1
33	12	23	2	0	2014-12-15 11:00:22	2014-12-15 11:26:04	34	1			0	1	1
31	12	23	2	0	2014-12-15 11:00:15	2014-12-15 11:26:35	38	1			0	1	1
54	10	52	2	0	2014-12-15 12:48:33	2014-12-15 12:48:33	4	1			0	1	3
59	3	10	2	0	2014-12-18 12:34:44	2014-12-18 12:34:44	1	1			0	1	1
49	14	48	2	0	2014-12-15 12:13:42	2014-12-15 12:13:42	23	1			0	1	1
85	8	\N	2	0	2014-12-23 14:08:17	2014-12-23 14:08:47	15	1			0	1	2
89	2	85	2	0	2014-12-23 16:05:14	2014-12-23 16:05:14	1	1			0	1	2
43	12	23	2	0	2014-12-15 11:00:34	2014-12-15 11:01:01	2	1			0	1	1
29	12	23	2	0	2014-12-15 11:00:15	2014-12-15 11:26:48	40	1			0	1	1
28	12	23	2	0	2014-12-15 11:00:14	2014-12-15 11:27:53	42	1			0	1	1
60	3	59	2	0	2014-12-18 12:37:15	2014-12-18 12:37:15	3	1			0	1	1
65	10	63	2	0	2014-12-22 09:33:10	2014-12-22 09:33:10	2	1			0	1	3
50	14	48	2	0	2014-12-15 12:14:17	2014-12-15 12:14:17	22	1			0	1	1
70	14	48	2	0	2014-12-23 10:45:50	2014-12-23 10:45:50	15	1	\N	\N	0	1	2
82	14	48	2	0	2014-12-23 11:35:34	2014-12-23 11:35:34	3	1	\N	\N	0	1	2
86	2	85	2	0	2014-12-23 16:02:46	2014-12-23 16:02:46	3	1			0	1	2
37	12	23	2	0	2014-12-15 11:00:23	2014-12-15 11:15:10	22	1			0	1	1
27	12	23	2	0	2014-12-15 11:00:10	2014-12-15 11:28:07	44	1			0	1	1
25	12	23	2	0	2014-12-15 11:00:04	2014-12-15 11:28:17	46	1			0	1	1
51	10	22	2	0	2014-12-15 12:42:11	2014-12-15 12:42:11	2	1			0	1	3
56	10	52	2	0	2014-12-15 13:12:28	2014-12-15 13:12:28	1	1			0	1	3
61	3	59	2	0	2014-12-18 12:40:07	2014-12-18 12:40:11	1	1			0	1	1
66	15	\N	2	0	2014-12-22 12:48:04	2014-12-22 12:48:04	14	1			0	1	2
32	12	23	2	0	2014-12-15 11:00:22	2014-12-15 11:02:08	10	1			0	1	1
35	12	23	2	0	2014-12-15 11:00:22	2014-12-15 11:15:44	26	1			0	1	1
30	12	23	2	0	2014-12-15 11:00:15	2014-12-15 11:15:57	28	1			0	1	1
87	2	85	2	0	2014-12-23 16:03:17	2014-12-23 16:03:39	4	1			0	1	2
46	12	23	2	0	2014-12-15 11:00:34	2014-12-15 11:01:31	4	1			0	1	1
26	12	23	2	0	2014-12-15 11:00:10	2014-12-15 11:26:22	36	1			0	1	1
45	12	23	2	0	2014-12-15 11:00:34	2014-12-15 11:01:40	6	1			0	1	1
39	12	23	2	0	2014-12-15 11:00:23	2014-12-15 11:13:59	16	1			0	1	1
52	10	22	2	0	2014-12-15 12:42:38	2014-12-15 12:42:38	1	1			0	1	3
62	10	19	2	0	2014-12-22 09:30:51	2014-12-22 09:30:51	1	1			0	1	3
57	10	63	2	0	2014-12-15 15:09:07	2014-12-22 09:37:09	1	1			0	1	1
47	12	23	2	0	2014-12-15 11:00:35	2014-12-15 11:01:55	8	1			0	1	1
58	14	48	2	0	2014-12-17 14:44:24	2014-12-17 14:44:35	20	1			0	1	2
68	14	48	2	0	2014-12-22 16:38:59	2014-12-22 16:38:59	17	1			0	1	2
88	2	85	2	0	2014-12-23 16:04:10	2014-12-23 16:04:10	2	1			0	1	2
44	12	23	2	0	2014-12-15 11:00:34	2014-12-15 11:02:18	12	1			0	1	1
42	12	23	2	0	2014-12-15 11:00:34	2014-12-15 11:13:46	14	1			0	1	1
\.


--
-- Name: pages_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('pages_id_seq', 89, true);


--
-- Data for Name: parameter_values; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY parameter_values (id, parameter_id, element_instance_id, value, created_at, updated_at) FROM stdin;
\.


--
-- Name: parameter_values_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('parameter_values_id_seq', 1, false);


--
-- Data for Name: parameters; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY parameters (id, element_id, keyword, name, type, default_value, editable, created_at, updated_at) FROM stdin;
1	1	number	Number of items	integer	3	1	2014-12-10 12:48:19	2014-12-10 12:48:19
\.


--
-- Name: parameters_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('parameters_id_seq', 1, true);


--
-- Data for Name: permission_categories; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY permission_categories (id, keyword, name, description, builtin, created_at, updated_at) FROM stdin;
1	general	General	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
2	pages	Pages	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
3	media	Media	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
4	video	Video	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
5	tags	Tags	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
6	comments	Comments & Likes	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
7	admin	Administration	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
8	keywords	Keywords	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
9	users	Users	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
10	roles	Roles	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
11	channels	Channels	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
12	data	Data	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
13	developer	Developer	\N	1	2014-12-10 12:48:19	2014-12-10 12:48:19
\.


--
-- Name: permission_categories_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('permission_categories_id_seq', 13, true);


--
-- Data for Name: permission_role; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY permission_role (id, role_id, permission_id, created_at, updated_at) FROM stdin;
1	1	3	2014-12-10 12:48:20	2014-12-10 12:48:20
2	1	12	2014-12-10 12:48:20	2014-12-10 12:48:20
3	1	28	2014-12-10 12:48:20	2014-12-10 12:48:20
4	2	3	2014-12-10 12:48:20	2014-12-10 12:48:20
5	2	12	2014-12-10 12:48:20	2014-12-10 12:48:20
6	2	28	2014-12-10 12:48:20	2014-12-10 12:48:20
7	2	30	2014-12-10 12:48:20	2014-12-10 12:48:20
8	2	36	2014-12-10 12:48:20	2014-12-10 12:48:20
9	2	37	2014-12-10 12:48:20	2014-12-10 12:48:20
10	3	1	2014-12-10 12:48:20	2014-12-10 12:48:20
11	3	3	2014-12-10 12:48:20	2014-12-10 12:48:20
12	3	4	2014-12-10 12:48:20	2014-12-10 12:48:20
13	3	5	2014-12-10 12:48:20	2014-12-10 12:48:20
14	3	6	2014-12-10 12:48:20	2014-12-10 12:48:20
15	3	8	2014-12-10 12:48:20	2014-12-10 12:48:20
16	3	10	2014-12-10 12:48:20	2014-12-10 12:48:20
17	3	12	2014-12-10 12:48:20	2014-12-10 12:48:20
18	3	13	2014-12-10 12:48:20	2014-12-10 12:48:20
19	3	14	2014-12-10 12:48:20	2014-12-10 12:48:20
20	3	15	2014-12-10 12:48:20	2014-12-10 12:48:20
21	3	17	2014-12-10 12:48:20	2014-12-10 12:48:20
22	3	20	2014-12-10 12:48:20	2014-12-10 12:48:20
23	3	24	2014-12-10 12:48:20	2014-12-10 12:48:20
24	3	42	2014-12-10 12:48:20	2014-12-10 12:48:20
25	3	28	2014-12-10 12:48:20	2014-12-10 12:48:20
26	3	30	2014-12-10 12:48:20	2014-12-10 12:48:20
27	3	31	2014-12-10 12:48:20	2014-12-10 12:48:20
28	3	33	2014-12-10 12:48:20	2014-12-10 12:48:20
29	3	36	2014-12-10 12:48:20	2014-12-10 12:48:20
30	3	37	2014-12-10 12:48:20	2014-12-10 12:48:20
31	3	48	2014-12-10 12:48:20	2014-12-10 12:48:20
32	4	1	2014-12-10 12:48:20	2014-12-10 12:48:20
33	4	3	2014-12-10 12:48:20	2014-12-10 12:48:20
34	4	4	2014-12-10 12:48:20	2014-12-10 12:48:20
35	4	5	2014-12-10 12:48:20	2014-12-10 12:48:20
36	4	7	2014-12-10 12:48:20	2014-12-10 12:48:20
37	4	9	2014-12-10 12:48:20	2014-12-10 12:48:20
38	4	10	2014-12-10 12:48:20	2014-12-10 12:48:20
39	4	12	2014-12-10 12:48:20	2014-12-10 12:48:20
40	4	13	2014-12-10 12:48:20	2014-12-10 12:48:20
41	4	14	2014-12-10 12:48:20	2014-12-10 12:48:20
42	4	16	2014-12-10 12:48:20	2014-12-10 12:48:20
43	4	18	2014-12-10 12:48:20	2014-12-10 12:48:20
44	4	20	2014-12-10 12:48:20	2014-12-10 12:48:20
45	4	21	2014-12-10 12:48:20	2014-12-10 12:48:20
46	4	22	2014-12-10 12:48:20	2014-12-10 12:48:20
47	4	23	2014-12-10 12:48:20	2014-12-10 12:48:20
48	4	24	2014-12-10 12:48:20	2014-12-10 12:48:20
49	4	25	2014-12-10 12:48:20	2014-12-10 12:48:20
50	4	42	2014-12-10 12:48:20	2014-12-10 12:48:20
51	4	28	2014-12-10 12:48:20	2014-12-10 12:48:20
52	4	29	2014-12-10 12:48:20	2014-12-10 12:48:20
53	4	30	2014-12-10 12:48:20	2014-12-10 12:48:20
54	4	32	2014-12-10 12:48:20	2014-12-10 12:48:20
55	4	34	2014-12-10 12:48:20	2014-12-10 12:48:20
56	4	36	2014-12-10 12:48:20	2014-12-10 12:48:20
57	4	37	2014-12-10 12:48:20	2014-12-10 12:48:20
58	4	48	2014-12-10 12:48:20	2014-12-10 12:48:20
59	5	1	2014-12-10 12:48:20	2014-12-10 12:48:20
60	5	3	2014-12-10 12:48:20	2014-12-10 12:48:20
61	5	4	2014-12-10 12:48:20	2014-12-10 12:48:20
62	5	5	2014-12-10 12:48:20	2014-12-10 12:48:20
63	5	7	2014-12-10 12:48:20	2014-12-10 12:48:20
64	5	9	2014-12-10 12:48:20	2014-12-10 12:48:20
65	5	10	2014-12-10 12:48:20	2014-12-10 12:48:20
66	5	11	2014-12-10 12:48:20	2014-12-10 12:48:20
67	5	12	2014-12-10 12:48:20	2014-12-10 12:48:20
68	5	13	2014-12-10 12:48:20	2014-12-10 12:48:20
69	5	14	2014-12-10 12:48:20	2014-12-10 12:48:20
70	5	16	2014-12-10 12:48:20	2014-12-10 12:48:20
71	5	18	2014-12-10 12:48:20	2014-12-10 12:48:20
72	5	19	2014-12-10 12:48:20	2014-12-10 12:48:20
73	5	20	2014-12-10 12:48:20	2014-12-10 12:48:20
74	5	21	2014-12-10 12:48:20	2014-12-10 12:48:20
75	5	22	2014-12-10 12:48:20	2014-12-10 12:48:20
76	5	23	2014-12-10 12:48:20	2014-12-10 12:48:20
77	5	24	2014-12-10 12:48:20	2014-12-10 12:48:20
78	5	25	2014-12-10 12:48:20	2014-12-10 12:48:20
79	5	26	2014-12-10 12:48:20	2014-12-10 12:48:20
80	5	42	2014-12-10 12:48:20	2014-12-10 12:48:20
81	5	43	2014-12-10 12:48:20	2014-12-10 12:48:20
82	5	44	2014-12-10 12:48:20	2014-12-10 12:48:20
83	5	28	2014-12-10 12:48:20	2014-12-10 12:48:20
84	5	29	2014-12-10 12:48:20	2014-12-10 12:48:20
85	5	30	2014-12-10 12:48:20	2014-12-10 12:48:20
86	5	32	2014-12-10 12:48:20	2014-12-10 12:48:20
87	5	34	2014-12-10 12:48:20	2014-12-10 12:48:20
88	5	35	2014-12-10 12:48:20	2014-12-10 12:48:20
89	5	36	2014-12-10 12:48:20	2014-12-10 12:48:20
90	5	37	2014-12-10 12:48:20	2014-12-10 12:48:20
91	5	38	2014-12-10 12:48:20	2014-12-10 12:48:20
92	5	40	2014-12-10 12:48:20	2014-12-10 12:48:20
93	5	57	2014-12-10 12:48:20	2014-12-10 12:48:20
94	5	46	2014-12-10 12:48:20	2014-12-10 12:48:20
95	5	47	2014-12-10 12:48:20	2014-12-10 12:48:20
96	5	49	2014-12-10 12:48:20	2014-12-10 12:48:20
97	5	50	2014-12-10 12:48:20	2014-12-10 12:48:20
98	5	53	2014-12-10 12:48:20	2014-12-10 12:48:20
99	5	61	2014-12-10 12:48:20	2014-12-10 12:48:20
100	5	62	2014-12-10 12:48:20	2014-12-10 12:48:20
101	5	63	2014-12-10 12:48:20	2014-12-10 12:48:20
102	5	64	2014-12-10 12:48:20	2014-12-10 12:48:20
103	5	65	2014-12-10 12:48:20	2014-12-10 12:48:20
104	6	1	2014-12-10 12:48:20	2014-12-10 12:48:20
105	6	3	2014-12-10 12:48:20	2014-12-10 12:48:20
106	6	4	2014-12-10 12:48:20	2014-12-10 12:48:20
107	6	5	2014-12-10 12:48:20	2014-12-10 12:48:20
108	6	7	2014-12-10 12:48:20	2014-12-10 12:48:20
109	6	9	2014-12-10 12:48:20	2014-12-10 12:48:20
110	6	10	2014-12-10 12:48:20	2014-12-10 12:48:20
111	6	11	2014-12-10 12:48:20	2014-12-10 12:48:20
112	6	12	2014-12-10 12:48:20	2014-12-10 12:48:20
113	6	13	2014-12-10 12:48:20	2014-12-10 12:48:20
114	6	14	2014-12-10 12:48:20	2014-12-10 12:48:20
115	6	16	2014-12-10 12:48:20	2014-12-10 12:48:20
116	6	18	2014-12-10 12:48:20	2014-12-10 12:48:20
117	6	19	2014-12-10 12:48:20	2014-12-10 12:48:20
118	6	20	2014-12-10 12:48:20	2014-12-10 12:48:20
119	6	21	2014-12-10 12:48:20	2014-12-10 12:48:20
120	6	22	2014-12-10 12:48:20	2014-12-10 12:48:20
121	6	23	2014-12-10 12:48:20	2014-12-10 12:48:20
122	6	24	2014-12-10 12:48:20	2014-12-10 12:48:20
123	6	25	2014-12-10 12:48:20	2014-12-10 12:48:20
124	6	26	2014-12-10 12:48:20	2014-12-10 12:48:20
125	6	27	2014-12-10 12:48:20	2014-12-10 12:48:20
126	6	42	2014-12-10 12:48:20	2014-12-10 12:48:20
127	6	43	2014-12-10 12:48:20	2014-12-10 12:48:20
128	6	44	2014-12-10 12:48:20	2014-12-10 12:48:20
129	6	45	2014-12-10 12:48:20	2014-12-10 12:48:20
130	6	28	2014-12-10 12:48:20	2014-12-10 12:48:20
131	6	29	2014-12-10 12:48:20	2014-12-10 12:48:20
132	6	30	2014-12-10 12:48:20	2014-12-10 12:48:20
133	6	32	2014-12-10 12:48:20	2014-12-10 12:48:20
134	6	34	2014-12-10 12:48:20	2014-12-10 12:48:20
135	6	35	2014-12-10 12:48:20	2014-12-10 12:48:20
136	6	36	2014-12-10 12:48:20	2014-12-10 12:48:20
137	6	37	2014-12-10 12:48:20	2014-12-10 12:48:20
138	6	38	2014-12-10 12:48:20	2014-12-10 12:48:20
139	6	39	2014-12-10 12:48:20	2014-12-10 12:48:20
140	6	40	2014-12-10 12:48:20	2014-12-10 12:48:20
141	6	41	2014-12-10 12:48:20	2014-12-10 12:48:20
142	6	57	2014-12-10 12:48:20	2014-12-10 12:48:20
143	6	59	2014-12-10 12:48:20	2014-12-10 12:48:20
144	6	46	2014-12-10 12:48:20	2014-12-10 12:48:20
145	6	47	2014-12-10 12:48:20	2014-12-10 12:48:20
146	6	49	2014-12-10 12:48:20	2014-12-10 12:48:20
147	6	50	2014-12-10 12:48:20	2014-12-10 12:48:20
148	6	51	2014-12-10 12:48:20	2014-12-10 12:48:20
149	6	52	2014-12-10 12:48:20	2014-12-10 12:48:20
150	6	53	2014-12-10 12:48:20	2014-12-10 12:48:20
151	6	54	2014-12-10 12:48:20	2014-12-10 12:48:20
152	6	55	2014-12-10 12:48:20	2014-12-10 12:48:20
153	6	56	2014-12-10 12:48:20	2014-12-10 12:48:20
154	6	61	2014-12-10 12:48:20	2014-12-10 12:48:20
155	6	62	2014-12-10 12:48:20	2014-12-10 12:48:20
156	6	63	2014-12-10 12:48:20	2014-12-10 12:48:20
157	6	64	2014-12-10 12:48:20	2014-12-10 12:48:20
158	6	65	2014-12-10 12:48:20	2014-12-10 12:48:20
159	7	1	2014-12-10 12:48:20	2014-12-10 12:48:20
160	7	2	2014-12-10 12:48:20	2014-12-10 12:48:20
161	7	3	2014-12-10 12:48:20	2014-12-10 12:48:20
162	7	4	2014-12-10 12:48:20	2014-12-10 12:48:20
163	7	5	2014-12-10 12:48:20	2014-12-10 12:48:20
164	7	7	2014-12-10 12:48:20	2014-12-10 12:48:20
165	7	9	2014-12-10 12:48:20	2014-12-10 12:48:20
166	7	10	2014-12-10 12:48:20	2014-12-10 12:48:20
167	7	11	2014-12-10 12:48:20	2014-12-10 12:48:20
168	7	12	2014-12-10 12:48:20	2014-12-10 12:48:20
169	7	13	2014-12-10 12:48:20	2014-12-10 12:48:20
170	7	14	2014-12-10 12:48:20	2014-12-10 12:48:20
171	7	16	2014-12-10 12:48:20	2014-12-10 12:48:20
172	7	18	2014-12-10 12:48:20	2014-12-10 12:48:20
173	7	19	2014-12-10 12:48:20	2014-12-10 12:48:20
174	7	20	2014-12-10 12:48:20	2014-12-10 12:48:20
175	7	21	2014-12-10 12:48:20	2014-12-10 12:48:20
176	7	22	2014-12-10 12:48:20	2014-12-10 12:48:20
177	7	23	2014-12-10 12:48:20	2014-12-10 12:48:20
178	7	24	2014-12-10 12:48:20	2014-12-10 12:48:20
179	7	25	2014-12-10 12:48:20	2014-12-10 12:48:20
180	7	26	2014-12-10 12:48:20	2014-12-10 12:48:20
181	7	27	2014-12-10 12:48:20	2014-12-10 12:48:20
182	7	42	2014-12-10 12:48:20	2014-12-10 12:48:20
183	7	43	2014-12-10 12:48:20	2014-12-10 12:48:20
184	7	44	2014-12-10 12:48:20	2014-12-10 12:48:20
185	7	45	2014-12-10 12:48:20	2014-12-10 12:48:20
186	7	28	2014-12-10 12:48:20	2014-12-10 12:48:20
187	7	29	2014-12-10 12:48:20	2014-12-10 12:48:20
188	7	30	2014-12-10 12:48:20	2014-12-10 12:48:20
189	7	32	2014-12-10 12:48:20	2014-12-10 12:48:20
190	7	34	2014-12-10 12:48:20	2014-12-10 12:48:20
191	7	35	2014-12-10 12:48:20	2014-12-10 12:48:20
192	7	36	2014-12-10 12:48:20	2014-12-10 12:48:20
193	7	37	2014-12-10 12:48:20	2014-12-10 12:48:20
194	7	38	2014-12-10 12:48:20	2014-12-10 12:48:20
195	7	39	2014-12-10 12:48:20	2014-12-10 12:48:20
196	7	40	2014-12-10 12:48:20	2014-12-10 12:48:20
197	7	41	2014-12-10 12:48:20	2014-12-10 12:48:20
198	7	57	2014-12-10 12:48:20	2014-12-10 12:48:20
199	7	58	2014-12-10 12:48:20	2014-12-10 12:48:20
200	7	59	2014-12-10 12:48:20	2014-12-10 12:48:20
201	7	60	2014-12-10 12:48:20	2014-12-10 12:48:20
202	7	46	2014-12-10 12:48:20	2014-12-10 12:48:20
203	7	47	2014-12-10 12:48:20	2014-12-10 12:48:20
204	7	49	2014-12-10 12:48:20	2014-12-10 12:48:20
205	7	50	2014-12-10 12:48:20	2014-12-10 12:48:20
206	7	51	2014-12-10 12:48:20	2014-12-10 12:48:20
207	7	52	2014-12-10 12:48:20	2014-12-10 12:48:20
208	7	53	2014-12-10 12:48:20	2014-12-10 12:48:20
209	7	54	2014-12-10 12:48:20	2014-12-10 12:48:20
210	7	55	2014-12-10 12:48:20	2014-12-10 12:48:20
211	7	56	2014-12-10 12:48:20	2014-12-10 12:48:20
212	7	61	2014-12-10 12:48:20	2014-12-10 12:48:20
213	7	62	2014-12-10 12:48:20	2014-12-10 12:48:20
214	7	63	2014-12-10 12:48:20	2014-12-10 12:48:20
215	7	64	2014-12-10 12:48:20	2014-12-10 12:48:20
216	7	65	2014-12-10 12:48:20	2014-12-10 12:48:20
217	7	66	2014-12-10 12:48:20	2014-12-10 12:48:20
218	7	67	2014-12-10 12:48:20	2014-12-10 12:48:20
219	7	69	2014-12-10 12:48:20	2014-12-10 12:48:20
220	7	71	2014-12-10 12:48:20	2014-12-10 12:48:20
221	7	72	2014-12-10 12:48:20	2014-12-10 12:48:20
\.


--
-- Name: permission_role_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('permission_role_id_seq', 221, true);


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY permissions (id, keyword, name, category_id, created_at, updated_at) FROM stdin;
1	general.admin	Use admin client	1	2014-12-10 12:48:19	2014-12-10 12:48:19
2	general.api	API access	1	2014-12-10 12:48:19	2014-12-10 12:48:19
3	pages.view	View	2	2014-12-10 12:48:19	2014-12-10 12:48:19
4	pages.read	Read	2	2014-12-10 12:48:19	2014-12-10 12:48:19
5	pages.create	Create	2	2014-12-10 12:48:19	2014-12-10 12:48:19
6	pages.edit_own	Edit own	2	2014-12-10 12:48:19	2014-12-10 12:48:19
7	pages.edit	Edit	2	2014-12-10 12:48:19	2014-12-10 12:48:19
8	pages.delete_own	Delete own	2	2014-12-10 12:48:19	2014-12-10 12:48:19
9	pages.delete	Delete	2	2014-12-10 12:48:19	2014-12-10 12:48:19
10	pages.publish	Publish	2	2014-12-10 12:48:19	2014-12-10 12:48:19
11	pages.trash	Empty trash	2	2014-12-10 12:48:19	2014-12-10 12:48:19
12	media.view	View	3	2014-12-10 12:48:19	2014-12-10 12:48:19
13	media.read	Read	3	2014-12-10 12:48:19	2014-12-10 12:48:19
14	media.create	Create	3	2014-12-10 12:48:19	2014-12-10 12:48:19
15	media.edit_own	Edit own	3	2014-12-10 12:48:19	2014-12-10 12:48:19
16	media.edit	Edit	3	2014-12-10 12:48:19	2014-12-10 12:48:19
17	media.delete_own	Delete own	3	2014-12-10 12:48:19	2014-12-10 12:48:19
18	media.delete	Delete	3	2014-12-10 12:48:19	2014-12-10 12:48:19
19	media.trash	Empty trash	3	2014-12-10 12:48:19	2014-12-10 12:48:19
20	video.read	Read	4	2014-12-10 12:48:19	2014-12-10 12:48:19
21	video.create	Create	4	2014-12-10 12:48:19	2014-12-10 12:48:19
22	video.edit	Edit	4	2014-12-10 12:48:19	2014-12-10 12:48:19
23	video.delete	Delete	4	2014-12-10 12:48:19	2014-12-10 12:48:19
24	tags.read	Read	4	2014-12-10 12:48:19	2014-12-10 12:48:19
25	tags.create	Create	4	2014-12-10 12:48:19	2014-12-10 12:48:19
26	tags.edit	Edit	4	2014-12-10 12:48:19	2014-12-10 12:48:19
27	tags.delete	Delete	4	2014-12-10 12:48:19	2014-12-10 12:48:19
28	comments.view	View	6	2014-12-10 12:48:19	2014-12-10 12:48:19
29	comments.read	Read	6	2014-12-10 12:48:19	2014-12-10 12:48:19
30	comments.create	Create	6	2014-12-10 12:48:19	2014-12-10 12:48:19
31	comments.edit_own	Edit own	6	2014-12-10 12:48:19	2014-12-10 12:48:19
32	comments.edit	Edit	6	2014-12-10 12:48:19	2014-12-10 12:48:19
33	comments.delete_own	Delete own	6	2014-12-10 12:48:19	2014-12-10 12:48:19
34	comments.delete	Delete	6	2014-12-10 12:48:19	2014-12-10 12:48:19
35	comments.trash	Empty trash	6	2014-12-10 12:48:19	2014-12-10 12:48:19
36	comments.like	Like	6	2014-12-10 12:48:19	2014-12-10 12:48:19
37	comments.unlike	Unlike	6	2014-12-10 12:48:19	2014-12-10 12:48:19
38	admin.read	Read	7	2014-12-10 12:48:19	2014-12-10 12:48:19
39	admin.edit	Edit	7	2014-12-10 12:48:19	2014-12-10 12:48:19
40	admin.read_logs	Read logs	7	2014-12-10 12:48:19	2014-12-10 12:48:19
41	admin.delete_logs	Delete logs	7	2014-12-10 12:48:19	2014-12-10 12:48:19
42	keywords.read	Read	8	2014-12-10 12:48:19	2014-12-10 12:48:19
43	keywords.create	Create	8	2014-12-10 12:48:19	2014-12-10 12:48:19
44	keywords.edit	Edit	8	2014-12-10 12:48:19	2014-12-10 12:48:19
45	keywords.delete	Delete	8	2014-12-10 12:48:19	2014-12-10 12:48:19
46	users.read	Read	9	2014-12-10 12:48:19	2014-12-10 12:48:19
47	users.create	Create	9	2014-12-10 12:48:19	2014-12-10 12:48:19
48	users.edit_own	Edit own	9	2014-12-10 12:48:19	2014-12-10 12:48:19
49	users.edit	Edit	9	2014-12-10 12:48:19	2014-12-10 12:48:19
50	users.delete	Delete	9	2014-12-10 12:48:19	2014-12-10 12:48:19
51	users.trash	Empty trash	9	2014-12-10 12:48:19	2014-12-10 12:48:19
52	users.roles	Assign roles	9	2014-12-10 12:48:19	2014-12-10 12:48:19
53	roles.read	Read	10	2014-12-10 12:48:19	2014-12-10 12:48:19
54	roles.create	Create	10	2014-12-10 12:48:19	2014-12-10 12:48:19
55	roles.edit	Edit	10	2014-12-10 12:48:19	2014-12-10 12:48:19
56	roles.delete	Delete	10	2014-12-10 12:48:19	2014-12-10 12:48:19
57	channels.read	Read	11	2014-12-10 12:48:19	2014-12-10 12:48:19
58	channels.create	Create	11	2014-12-10 12:48:19	2014-12-10 12:48:19
59	channels.edit	Edit	11	2014-12-10 12:48:19	2014-12-10 12:48:19
60	channels.delete	Delete	11	2014-12-10 12:48:19	2014-12-10 12:48:19
61	data.read	Read	12	2014-12-10 12:48:19	2014-12-10 12:48:19
62	data.create	Create	12	2014-12-10 12:48:19	2014-12-10 12:48:19
63	data.edit	Edit	12	2014-12-10 12:48:19	2014-12-10 12:48:19
64	data.delete	Delete	12	2014-12-10 12:48:19	2014-12-10 12:48:19
65	data.trash	Empty trash	12	2014-12-10 12:48:19	2014-12-10 12:48:19
66	developer.read	Read	13	2014-12-10 12:48:19	2014-12-10 12:48:19
67	developer.create	Create	13	2014-12-10 12:48:19	2014-12-10 12:48:19
68	developer.edit_own	Edit own	13	2014-12-10 12:48:19	2014-12-10 12:48:19
69	developer.edit	Edit	13	2014-12-10 12:48:19	2014-12-10 12:48:19
70	developer.delete_own	Delete own	13	2014-12-10 12:48:19	2014-12-10 12:48:19
71	developer.delete	Delete	13	2014-12-10 12:48:19	2014-12-10 12:48:19
72	developer.trash	Empty trash	13	2014-12-10 12:48:19	2014-12-10 12:48:19
\.


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('permissions_id_seq', 72, true);


--
-- Data for Name: profiles; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY profiles (id, user_id, language, created_at, updated_at) FROM stdin;
1	1	en	2014-12-10 12:48:19	2014-12-10 12:48:19
\.


--
-- Name: profiles_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('profiles_id_seq', 1, true);


--
-- Data for Name: properties; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY properties (id, template_id, keyword, name, type, rules, default_value, required, validation, created_at, updated_at, sort, index) FROM stdin;
1	1	welcome	Welcome	text			1		2014-12-10 12:48:19	2014-12-10 12:48:19	1	1
2	1	intro	Intro	textarea			1		2014-12-10 12:48:19	2014-12-10 12:48:19	1	1
3	1	content	Content	richtext			1		2014-12-10 12:48:19	2014-12-10 12:48:19	1	1
6	3	preamble	Preamble	textarea			1		2014-12-10 12:48:19	2014-12-10 12:48:19	1	1
7	3	content	Content	richtext			0		2014-12-10 12:48:19	2014-12-10 12:48:19	1	1
8	5	feed_author	Author	text			1		2014-12-10 12:48:19	2014-12-10 12:48:19	1	1
9	5	feed_email	Email	text			1		2014-12-10 12:48:19	2014-12-10 12:48:19	1	1
10	5	feed_description	Description	textarea			1		2014-12-10 12:48:19	2014-12-10 12:48:19	1	1
11	5	feed_format	Format	dropdown	rss092:RSS 0.92|rss20:RSS 2.0|atom:Atom	rss20	1	in:rss092,rss20,atom	2014-12-10 12:48:19	2014-12-10 12:48:19	1	1
12	6	preamble	Kort tekst	textarea			0		2014-12-11 14:37:31	2014-12-11 14:37:31	1	1
13	8	preamble	Kort tekst	textarea			0		2014-12-12 11:14:36	2014-12-12 11:14:36	1	1
14	8	body	Innhold	richtext			0		2014-12-12 11:22:54	2014-12-12 11:22:54	2	1
4	2	preamble	Kort tekst	textarea			0		2014-12-10 12:48:19	2014-12-12 11:32:18	1	1
5	2	body	Innhold	richtext			0		2014-12-10 12:48:19	2014-12-12 11:32:18	2	1
15	2	image1	Bilde	image			0		2014-12-12 11:32:19	2014-12-12 11:32:19	3	1
16	6	path_birthdays	Jubilantmappe	page			0		2014-12-12 11:59:25	2014-12-12 11:59:25	2	1
17	9	preamble	Kort tekst	textarea			0		2014-12-12 14:07:16	2014-12-12 14:07:16	1	1
18	9	body	Innhold	richtext			0		2014-12-12 14:07:16	2014-12-12 14:07:16	2	1
19	9	folder_gallery	Bildemappe	folder			0		2014-12-12 14:21:08	2014-12-12 14:21:08	3	1
20	6	folder_gallery	Gallerimappe	folder			0		2014-12-12 14:29:49	2014-12-12 14:29:49	3	1
21	6	path_mymessages	Mine meldinger mappe	page			0		2014-12-12 15:17:07	2014-12-12 15:17:07	4	1
23	10	end_date	Slutt dato	date			0		2014-12-15 08:48:05	2014-12-15 08:48:05	2	1
25	10	content	Content	richtext			0		2014-12-15 08:49:45	2014-12-15 08:49:45	4	1
27	11	preamble	Preamble	textarea			0		2014-12-15 09:28:56	2014-12-15 09:28:56	1	1
28	11	path_calendar	Kalendermappe	page			0		2014-12-15 09:33:20	2014-12-15 09:33:20	2	1
29	12	preamble	Kort tekst	textarea			0		2014-12-15 10:36:14	2014-12-15 10:36:14	1	1
30	12	path_xmascal	Julekalender mappe	page			0		2014-12-15 10:45:43	2014-12-15 10:45:43	2	1
31	6	path_xmascal	Julekalender mappe	page			0		2014-12-15 11:05:04	2014-12-15 11:05:04	5	1
32	12	body	Innhold	richtext			0		2014-12-15 11:09:03	2014-12-15 11:09:03	3	1
33	13	preamble	Kort tekst	textarea			0		2014-12-15 12:03:56	2014-12-15 12:03:56	1	1
34	13	body	Innhold	richtext			0		2014-12-15 12:03:56	2014-12-15 12:03:56	2	1
35	14	preamble	Kort tekst	textarea			0		2014-12-15 12:09:23	2014-12-15 12:09:23	1	1
36	14	body	Innhold	richtext			0		2014-12-15 12:09:34	2014-12-15 12:09:34	2	1
37	14	closed_group	Lukket gruppe	checkbox			0		2014-12-15 12:10:24	2014-12-15 12:10:24	3	1
38	13	path_groups	Gruppe mappe	page			0		2014-12-15 12:12:31	2014-12-15 12:12:31	3	1
39	6	path_calendar	Kalendermappe	page			0		2014-12-15 12:28:26	2014-12-15 12:28:26	6	1
40	6	path_groups	Gruppe mappe	page			0		2014-12-15 13:13:52	2014-12-15 13:13:52	7	1
41	14	path_groups	Gruppemappe	page			0		2014-12-15 13:49:30	2014-12-15 13:49:30	4	1
42	6	path_driftsmeldinger	Driftsmeldinger mappe	page			0		2014-12-18 12:33:06	2014-12-18 12:33:06	8	1
22	10	publish_date	Dato	date			1		2014-12-15 08:48:05	2014-12-22 09:34:08	1	1
24	10	preamble	Preamble	textarea			0		2014-12-15 08:49:19	2014-12-22 09:34:08	3	1
43	15	preamble	Kort tekst	textarea			0		2014-12-22 12:31:27	2014-12-22 12:31:27	1	1
44	15	body	Innhold	richtext			0		2014-12-22 12:46:07	2014-12-22 12:46:07	2	1
45	15	path_mymessages	Meldinger mappe	page			0		2014-12-23 12:06:38	2014-12-23 12:06:38	3	1
46	8	pagepath_articles	Nyhetesmappe	page			0		2014-12-23 16:06:10	2014-12-23 16:06:10	3	1
47	8	keys	Nøkkelord	dropdown	collection_id:1		0		2014-12-23 16:20:11	2014-12-23 16:20:11	4	1
\.


--
-- Name: properties_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('properties_id_seq', 47, true);


--
-- Data for Name: related_links; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY related_links (id, page_id, title, url, new_window, created_at, updated_at) FROM stdin;
1	20	Test	http://www.vg.no	1	2014-12-15 15:53:21	2014-12-15 15:53:21
\.


--
-- Name: related_links_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('related_links_id_seq', 1, true);


--
-- Data for Name: related_media; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY related_media (id, page_id, media_id, created_at, updated_at) FROM stdin;
1	20	11	2014-12-15 08:58:35	2014-12-15 08:58:35
\.


--
-- Name: related_media_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('related_media_id_seq', 1, true);


--
-- Data for Name: related_pages; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY related_pages (id, page_id, related_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: related_pages_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('related_pages_id_seq', 4, true);


--
-- Data for Name: role_user; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY role_user (id, user_id, role_id, created_at, updated_at) FROM stdin;
1	1	2	2014-12-10 12:48:19	2014-12-10 12:48:19
2	1	7	2014-12-10 12:48:19	2014-12-10 12:48:19
3	2	7	2014-12-11 14:34:33	2014-12-11 14:34:33
4	2	2	2014-12-11 14:34:33	2014-12-11 14:34:33
5	3	2	2014-12-11 14:35:49	2014-12-11 14:35:49
6	3	7	2014-12-11 14:50:16	2014-12-11 14:50:16
7	4	2	2014-12-15 18:08:07	2014-12-15 18:08:07
8	5	3	2014-12-17 12:23:12	2014-12-17 12:23:12
9	5	2	2014-12-17 12:23:12	2014-12-17 12:23:12
10	6	3	2014-12-22 15:02:51	2014-12-22 15:02:51
11	6	2	2014-12-22 15:02:51	2014-12-22 15:02:51
\.


--
-- Name: role_user_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('role_user_id_seq', 11, true);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY roles (id, name, created_at, updated_at, builtin) FROM stdin;
1	Guest	2014-12-10 12:48:19	2014-12-10 12:48:19	1
2	User	2014-12-10 12:48:19	2014-12-10 12:48:19	1
3	Author	2014-12-10 12:48:19	2014-12-10 12:48:19	0
4	Editor	2014-12-10 12:48:19	2014-12-10 12:48:19	0
5	Manager	2014-12-10 12:48:19	2014-12-10 12:48:19	0
6	Administrator	2014-12-10 12:48:19	2014-12-10 12:48:19	0
7	Developer	2014-12-10 12:48:19	2014-12-10 12:48:19	0
\.


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('roles_id_seq', 7, true);


--
-- Data for Name: search_index; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY search_index (id, translation_id, phrase) FROM stdin;
47	53	'17':2 'luk':1
46	52	'18':2 'luk':1
39	45	'19':2 'luk':1
44	50	'20':2 'luk':1
42	48	'21':2 'luk':1
41	47	'22':2 'luk':1
40	46	'23':2 'luk':1
38	44	'24':2 'luk':1
61	67	'gr':1
72	78	'alert':5 'driftsmelding':1 'list':3 'of':4 'the':2
73	79	'-13.00':14 '12.30':13,31 '13.00':33 'be':7,24 'connection':10,27 'datacent':4,21 'due':15 'friday':12,29 'from':30 'in':2,19 'intern':9,26 'it':34 'maintenanc':17 'operation':35 'our':20 'the':3 'ther':5,22 'this':11,28 'to':16,32 'will':6,23 'work':1,18
64	70	'cal':1,3 'one':2,4
65	71	'cal':1,3 'two':2,4
23	29	'googl':1 'search':2
24	30	'googl':1 'søk':2
74	80	'a':3 'problem':1
37	43	'1':2 'luk':1
25	31	'b':1
75	81	'calend':1 'css':7 'forandr':4 'one':2 's':5 't':6
26	32	'andreass':2 'ann':1 'b':3
27	33	'red':1 'robin':2
28	34	'galleri':1
29	35	'melding':2
30	36	'blomst':4 'husk':1 'jul':6 'vann':3
31	37	'se':1 'tilbud':3
76	82	'calend':1 'css':7 'forandr':4 's':5 't':6 'two':2
32	38	'kalend':1,2
35	41	'again':3 'calend':1 'hello':2
63	69	'adipiscing':8 'aenean':10,15 'amet':6 'commodo':11 'consectetu':7 'cum':17 'dis':23 'dolor':4,14 'eget':13 'elit':9 'ipsum':3 'ligul':12 'lorem':2 'magnis':22 'mass':16 'natoqu':19 'part':24 'penatibus':20 'sit':5 'sociis':18 'strikkeklா':1
62	68	'aliqu':13 'consequat':4 'donec':8 'enim':7 'fringill':11 'justo':10 'mass':5 'nec':14 'null':3 'ped':9 'quis':6 'sykl':2 'v':15 'vel':12
78	84	'event':1,3 'two':2,4
33	39	'j':1
70	76	'6':2 'hurr':4,5,6 'jonatan':1 'år':3
34	40	'god':2 'hurr':1 'mat':3
81	87	'l':2 'ølklா':1
69	75	'cal':1,4 'new':3,6 'two':2,5
56	62	'2':2 'luk':1
59	65	'3':2 'luk':1
58	64	'4':2 'luk':1
60	66	'5':2 'luk':1
45	51	'6':2 'luk':1
57	63	'7':2 'luk':1
55	61	'8':2 'luk':1
52	58	'9':2 'luk':1
54	60	'10':2 'luk':1
53	59	'11':2 'luk':1
50	56	'12':2 'luk':1
51	57	'13':2 'luk':1
48	54	'14':2 'luk':1
43	49	'15':2 'luk':1
66	72	'cal':1 'calend':3 'one':2,4
36	42	'j':1
49	55	'16':2 'luk':1
67	73	'cal':1 'calend':3 'two':2,4
82	89	'grupp':2 'henriks':1
94	101	'grupp':3 'null':4
79	85	'adress':16 'e.l':17 'endr':15 'inforamsjon':5 'informasjon':2 'nedenfor':3 'oppdater':11
22	28	'kort':2 'sannkass':1 'tekst':3
71	77	'ad':48,96,144 'agam':37,85,133 'consectetu':33,81,129 'constituam':30,78,126 'consul':42,90,138 'dolorum':25,73,121 'duo':49,97,145 'efficiantur':20,68,116 'est':34,82,130 'eu':16,64,112 'eum':44,92,140 'ex':11,22,59,70,107,118 'fabell':29,77,125 'ferri':28,76,124 'illum':23,71,119 'in':43,91,139 'ipsum':13,61,109 'iusto':24,72,120 'magn':38,86,134 'maiestatis':47,95,143 'mel':17,65,113 'molestia':32,80,128 'ne':36,84,132 'nominavi':45,93,141 'nonumy':7,55,103 'officiis':14,19,62,67,110,115 'omittantur':9,57,105 'oporter':39,87,135 'per':40,88,136 'philosophi':15,63,111 'pro':10,58,106 'prompt':8,56,104 'sumo':41,89,137 'sying':5,53,101 'sykrok':1 'te':35,83,131 'theophrastus':21,69,117 'torq':46,94,142 'usu':26,31,74,79,122,127 'vel':12,60,108
98	105	'adipiscing':13 'aenean':15,20 'amet':11 'commodo':16 'consectetu':12 'cum':22 'dis':28 'dolor':9,19 'eget':18 'elit':14 'ipsum':8 'ligul':17 'lorem':7 'magnis':27 'mass':21 'natoqu':24 'nyh':6 'part':29 'penatibus':25 'sit':10 'sociis':23 'veld':4 'vikt':5
99	106	'adipiscing':7 'aenean':9,14 'commodo':10 'consectetu':6 'cum':16 'dis':22 'dolor':13 'eget':12 'elit':8 'god':1 'godt':3 'jul':2 'ligul':11 'magnis':21 'mass':15 'natoqu':18 'nytt':4 'part':23 'penatibus':19 'sociis':17 'år':5
100	107	'aenean':3,8 'commodo':4 'cum':10 'dis':16 'dolor':7 'eget':6 'ligul':5 'magnis':15 'mass':9 'natoqu':12 'nyh':2 'oslo':1 'part':17 'penatibus':13 'sociis':11
101	108	'fikk':2 'gutt':4 'het':6 'theodor':5 'vanni':1
97	104	'nyhet':1
\.


--
-- Name: search_index_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('search_index_id_seq', 101, true);


--
-- Data for Name: templates; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY templates (id, keyword, name, description, summarizer, created_at, updated_at, default_access, author_hide, content_type, index, author_id) FROM stdin;
1	welcome	Welcome	\N	return data.preamble;	2014-12-10 12:48:19	2014-12-10 12:48:19	\N	0	\N	1	1
3	page	Page	\N	return data.preamble;	2014-12-10 12:48:19	2014-12-10 12:48:19	\N	0	\N	1	1
4	search	Search	\N	return "";	2014-12-10 12:48:19	2014-12-10 12:48:19	\N	0	\N	0	1
5	rss	RSS Feed	\N	return "";	2014-12-10 12:48:19	2014-12-10 12:48:19	\N	0	application/xml	0	1
2	news	News		return data.preamble;	2014-12-10 12:48:19	2014-12-23 16:57:30	\N	0		1	1
11	start-test	Start test			2014-12-15 09:27:58	2014-12-23 08:19:29	\N	0		1	3
15	my_info	My information			2014-12-22 12:31:27	2014-12-23 13:46:49	\N	1		1	2
6	startpage	Startpage			2014-12-11 14:37:31	2014-12-23 13:52:48	\N	0		1	1
13	groups	Groups	Oversikt over gruppene. \nOpprettelse av nye grupper.		2014-12-15 12:03:56	2014-12-23 14:10:24	\N	1		1	1
14	group	Group	Gruppeside der man kan diskutere og dele filer		2014-12-15 12:09:23	2014-12-23 14:12:09	\N	1		1	1
9	gallery	Gallery			2014-12-12 14:07:16	2014-12-12 14:21:08	\N	0		1	1
7	search-results	 Google search			2014-12-11 15:44:50	2014-12-12 15:27:16	\N	0		1	3
10	calendar	Calendar			2014-12-15 08:45:55	2014-12-22 09:34:08	\N	0		1	3
12	xmascalendar	Xmas Calendar			2014-12-15 10:35:41	2014-12-15 11:10:50	\N	1		1	1
8	newslist	Newslist			2014-12-12 11:14:35	2014-12-23 16:46:00	\N	0		1	1
\.


--
-- Name: templates_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('templates_id_seq', 15, true);


--
-- Data for Name: translations; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY translations (id, page_id, language_id, subject, slug, summary, created_at, updated_at, keywords, description, title, content) FROM stdin;
107	88	2	Oslo nyhet	oslo-nyhet	Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.	2014-12-23 16:04:10	2014-12-23 16:04:18				{"preamble":"Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.","body":""}
67	48	2	Grupper	grupper	\N	2014-12-15 12:06:48	2014-12-15 12:12:46				{"preamble":"Her er alle de \\u00e5pne gruppene man kan melde seg p\\u00e5.\\nTil venstre kan du se hvilke grupper du er medlem av.","body":"","path_groups":48}
65	46	2	Luke 3	luke-3	\N	2014-12-15 11:00:34	2014-12-15 11:01:31				{"preamble":""}
61	42	2	Luke 8	luke-8	\N	2014-12-15 11:00:34	2014-12-15 11:13:46				{"preamble":"","body":""}
56	37	2	Luke 12	luke-12	\N	2014-12-15 11:00:23	2014-12-15 11:15:10				{"preamble":"","body":""}
42	23	2	Julekalender	julekalender	\N	2014-12-15 10:44:26	2014-12-15 11:25:00				{"preamble":"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ","path_xmascal":23,"body":"<p><img alt=\\"christmas_illustration2\\" src=\\"\\/cms\\/images\\/13\\/454\\/278\\/christmas_illustration2.png?_dc=1418639086000\\" style=\\"float: right;\\" \\/>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.<\\/p>\\n\\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.<\\/p>\\n\\n<p>Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.<\\/p>\\n"}
55	36	2	Luke 16	luke-16	\N	2014-12-15 11:00:22	2014-12-15 11:25:41				{"preamble":"","body":""}
50	31	2	Luke 20	luke-20	\N	2014-12-15 11:00:15	2014-12-15 11:26:35				{"preamble":"","body":""}
87	68	2	Ølklubben	oelklubben	\N	2014-12-22 16:38:59	2014-12-22 16:38:59				{"preamble":"Lukket gruppe for \\u00f8lentusiaster","body":"","closed_group":1}
82	63	2	Calendar two	calendar-two	\N	2014-12-22 09:31:31	2014-12-23 09:00:50				{"publish_date":null,"end_date":null,"preamble":"Ikke forandre Subject (CSS)","content":""}
72	53	2	Cal one	cal-one	\N	2014-12-15 12:47:23	2014-12-23 09:02:16				{"publish_date":"2014-12-31T00:00:00","end_date":null,"preamble":"Calendar one","content":""}
77	58	2	Sykroken	sykroken	\N	2014-12-17 14:44:24	2014-12-23 12:04:00				{"preamble":"Her er det sying som Nonumy prompta omittantur pro ex, vel ipsum officiis philosophia eu. Mel et officiis efficiantur theophrastus, ex illum iusto dolorum usu, at ferri fabellas constituam usu. Molestiae consectetuer est te. Ne agam magna oportere per. Sumo consul in eum, nominavi torquatos maiestatis ad duo.\\n","body":"Her er det sying som Nonumy prompta omittantur pro ex, vel ipsum officiis philosophia eu. Mel et officiis efficiantur theophrastus, ex illum iusto dolorum usu, at ferri fabellas constituam usu. Molestiae consectetuer est te. Ne agam magna oportere per. Sumo consul in eum, nominavi torquatos maiestatis ad duo.<br \\/>\\nHer er det sying som Nonumy prompta omittantur pro ex, vel ipsum officiis philosophia eu. Mel et officiis efficiantur theophrastus, ex illum iusto dolorum usu, at ferri fabellas constituam usu. Molestiae consectetuer est te. Ne agam magna oportere per. Sumo consul in eum, nominavi torquatos maiestatis ad duo.<br \\/>\\n","closed_group":1,"path_groups":48}
68	49	2	Vi sykler	vi-sykler	\N	2014-12-15 12:13:42	2014-12-15 13:50:01				{"preamble":"Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ","body":"<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.<\\/p>\\n\\n<p>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.<\\/p>\\n\\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.<\\/p>\\n","closed_group":0,"path_groups":48}
62	43	2	Luke 2	luke2	\N	2014-12-15 11:00:34	2014-12-15 11:01:17				{"preamble":""}
66	47	2	Luke 5	luke-5	\N	2014-12-15 11:00:35	2014-12-15 11:01:55				{"preamble":""}
58	39	2	Luke 9	luke-9	\N	2014-12-15 11:00:23	2014-12-15 11:13:59				{"preamble":"","body":""}
57	38	2	Luke 13	luke-13	\N	2014-12-15 11:00:23	2014-12-15 11:15:24				{"preamble":"","body":""}
53	34	2	Luke 17	luke-17	\N	2014-12-15 11:00:22	2014-12-15 11:25:52				{"preamble":"","body":""}
78	59	2	Driftsmeldinger	driftsmeldinger	The list of alerts	2014-12-18 12:34:44	2014-12-18 12:34:56				{"preamble":"The list of alerts","content":""}
73	54	2	Cal two	cal-two	\N	2014-12-15 12:48:33	2014-12-23 10:22:07				{"publish_date":"2014-12-26T00:00:00","end_date":"2014-12-26T00:00:00","preamble":"Calendar Two","content":""}
108	89	2	Vanni fikk en gutt!!	vanni-fikk-en-gutt	Theodor heter han...	2014-12-23 16:05:14	2014-12-23 16:05:14				{"preamble":"Theodor heter han...","body":"","image1":14}
69	50	2	Strikkeklubben	strikkeklubben	\N	2014-12-15 12:14:17	2014-12-15 13:49:49				{"preamble":"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ","body":"<p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.<\\/p>\\n\\n<p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.<\\/p>\\n","closed_group":0,"path_groups":48}
79	60	2	Works in the datacenter	works-in-the-datacenter	There will be no Internet connection this Friday 12.30-13.00	2014-12-18 12:37:15	2014-12-18 12:37:15				{"preamble":"There will be no Internet connection this Friday 12.30-13.00","content":"<p>Due to maintenance works in our datacenter, there will be no Internet connection this Friday from 12.30 to 13.00<\\/p>\\n\\n<p>IT Operations<\\/p>\\n"}
84	65	2	Event two	event-two	\N	2014-12-22 09:33:10	2014-12-22 09:33:10				{"publish_date":"2014-12-25T00:00:00","end_date":null,"preamble":"Event two","content":""}
51	32	2	Luke 6	luke-6	\N	2014-12-15 11:00:22	2014-12-15 11:02:08				{"preamble":""}
63	44	2	Luke 7	luke-7	\N	2014-12-15 11:00:34	2014-12-15 11:02:18				{"preamble":""}
60	41	2	Luke 10	luke-10	\N	2014-12-15 11:00:34	2014-12-15 11:14:09				{"preamble":"","body":""}
54	35	2	Luke 14	luke-14	\N	2014-12-15 11:00:22	2014-12-15 11:15:44				{"preamble":"","body":""}
52	33	2	Luke 18	luke-18	\N	2014-12-15 11:00:22	2014-12-15 11:26:04				{"preamble":"","body":""}
89	70	2	Henriks gruppe	henriks-gruppe	\N	2014-12-23 10:45:50	2014-12-23 10:45:50	\N	\N	\N	{"preamble":"","body":"","closed_group":0}
104	85	2	Nyheter	nyheter	\N	2014-12-23 14:08:17	2014-12-23 16:38:40				{"preamble":"","body":"","pagepath_articles":85,"keys":"1"}
70	51	2	Cal One	cal-one	\N	2014-12-15 12:42:11	2014-12-15 12:42:11				{"publish_date":null,"end_date":null,"preamble":"Cal one","content":""}
64	45	2	Luke 4	luke-4	\N	2014-12-15 11:00:34	2014-12-15 11:01:40				{"preamble":""}
59	40	2	Luke 11	luke-11	\N	2014-12-15 11:00:34	2014-12-15 11:14:28				{"preamble":"","body":""}
49	30	2	Luke 15	luke-15	\N	2014-12-15 11:00:15	2014-12-15 11:15:57				{"preamble":"","body":""}
45	26	2	Luke 19	luke-19	\N	2014-12-15 11:00:10	2014-12-15 11:26:22				{"preamble":"","body":""}
80	61	2	Problemer med autentisering mot Active Directory [LØST]	problemer-med-autentisering-mot-active-directory-loest	Det var ikke mulig å bruke NTLM og LDAP mot ngi.no Active Directory mellom kl. 14.30 og 15.00 i dag. Dette er nå rettet og tillatt igjen.	2014-12-18 12:40:07	2014-12-18 12:40:11				{"preamble":"Det var ikke mulig \\u00e5 bruke NTLM og LDAP mot ngi.no Active Directory mellom kl. 14.30 og 15.00 i dag. Dette er n\\u00e5 rettet og tillatt igjen.","content":""}
75	56	2	Cal two new	cal-two-new	\N	2014-12-15 13:12:28	2014-12-22 12:17:23				{"publish_date":"2014-12-25T00:00:00","end_date":null,"preamble":"Cal two new","content":""}
85	66	2	Min informasjon	my-info	\N	2014-12-22 12:48:04	2014-12-23 12:06:45				{"preamble":"Nedenfor er inforamsjon om deg som du kan oppdatere om du skulle endre adresse e.l.","body":"","path_mymessages":16}
28	10	2	Sannkassen	sannkassen	\N	2014-12-11 14:51:54	2014-12-23 12:53:24				{"preamble":"Kort tekst","path_birthdays":12,"folder_gallery":2,"path_mymessages":16,"path_xmascal":23,"path_calendar":19,"path_groups":48,"path_driftsmeldinger":59}
105	86	2	Dette er en veldig viktig nyhet	dette-er-en-veldig-viktig-nyhet	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.	2014-12-23 16:02:46	2014-12-23 16:02:58				{"preamble":"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.","body":"","image1":12}
48	29	2	Luke 21	luke-21	\N	2014-12-15 11:00:15	2014-12-15 11:26:48				{"preamble":"","body":""}
25	9	1	RSS	rss		2014-12-10 12:48:19	2014-12-10 12:48:19	\N	\N	\N	{"feed_author":"Symfoni","feed_email":"contact@symfoni.com","feed_description":"The latest news from this site.","feed_format":"rss20"}
26	9	2	RSS	rss		2014-12-10 12:48:19	2014-12-10 12:48:19	\N	\N	\N	{"feed_author":"Symfoni","feed_email":"contact@symfoni.com","feed_description":"De siste nyhetene fra denne siden.","feed_format":"rss20"}
27	9	3	RSS	rss		2014-12-10 12:48:19	2014-12-10 12:48:19	\N	\N	\N	{"feed_author":"Symfoni","feed_email":"contact@symfoni.com","feed_description":"Senaste nytt fr\\u00e5n den h\\u00e4r webbplatsen.","feed_format":"rss20"}
47	28	2	Luke 22	luke-22	\N	2014-12-15 11:00:14	2014-12-15 11:27:53				{"preamble":"","body":""}
46	27	2	Luke 23	luke-23	\N	2014-12-15 11:00:10	2014-12-15 11:28:07				{"preamble":"","body":""}
44	25	2	Luke 24	luke-24	\N	2014-12-15 11:00:04	2014-12-15 11:28:17				{"preamble":"","body":""}
29	11	1	Google search	google-search	\N	2014-12-12 08:51:10	2014-12-12 08:58:59				{}
30	11	2	Google søk	google-soek	\N	2014-12-12 09:00:14	2014-12-12 09:00:14				{}
71	52	2	Cal Two	cal-two	\N	2014-12-15 12:42:39	2014-12-15 12:42:39				{"publish_date":null,"end_date":null,"preamble":"cal two","content":""}
31	12	2	Bursdagsbarn	bursdagsbarn	\N	2014-12-12 11:25:21	2014-12-12 11:28:03				{"preamble":"","body":""}
81	62	2	Calendar one	calendar-one	\N	2014-12-22 09:30:51	2014-12-23 09:00:38				{"publish_date":null,"end_date":null,"preamble":"Ikke forandre Subject (CSS)","content":""}
32	13	2	Anne Andreassen	anne-andreassen	Bursdag	2014-12-12 11:31:13	2014-12-12 11:33:34				{"preamble":"Bursdag","body":"","image1":2}
33	14	2	Red Robin	red-robin		2014-12-12 11:33:54	2014-12-12 11:33:54				{"preamble":"","body":"","image1":1}
41	22	2	Calendar	test-cal	\N	2014-12-15 09:30:52	2014-12-23 09:01:13				{"preamble":"Hello again","path_calendar":22}
43	24	2	Luke 1	luke-1	\N	2014-12-15 10:45:00	2014-12-15 10:45:09				{"preamble":""}
22	8	1	Search	search		2014-12-10 12:48:19	2014-12-12 14:24:12	\N	\N	\N	{}
23	8	2	Søk	soek		2014-12-10 12:48:19	2014-12-12 14:24:12	\N	\N	\N	{}
24	8	3	Sök	soek		2014-12-10 12:48:19	2014-12-12 14:24:12	\N	\N	\N	{}
34	15	2	Galleri	galleri	\N	2014-12-12 14:24:58	2014-12-12 14:24:58				{"preamble":"","body":"","folder_gallery":2}
35	16	2	Mine meldinger	mine-meldinger	\N	2014-12-12 15:17:55	2014-12-12 15:17:55				{"preamble":"","body":""}
36	17	2	Husk å vanne blomstene i jula	husk-aa-vanne-blomstene-i-jula		2014-12-12 15:18:21	2014-12-12 15:18:21				{"preamble":"","body":""}
37	18	2	Se over tilbudet ...	se-over-tilbudet		2014-12-12 15:18:46	2014-12-12 15:18:46				{"preamble":"","body":""}
38	19	2	Kalender	kalender	\N	2014-12-15 08:55:11	2014-12-15 08:55:11				{"publish_date":null,"end_date":null,"preamble":"Kalender","content":""}
39	20	2	Julaften	julaften	\N	2014-12-15 08:56:10	2014-12-22 09:35:42				{"publish_date":"2014-12-24T00:00:00","end_date":null,"preamble":"Vi feirer","content":"Jul"}
101	82	2	Bare min gruppe	bare-min-gruppe	\N	2014-12-23 11:35:34	2014-12-23 11:35:34	\N	\N	\N	{"preamble":"","body":null,"closed_group":1}
106	87	2	God jul & godt nytt år!	god-jul--godt-nytt-aar	Consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.	2014-12-23 16:03:17	2014-12-23 16:03:39				{"preamble":"Consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.","body":"","image1":12}
76	57	2	Jonatan 6 år	jonatan-6-aar	\N	2014-12-15 15:09:07	2014-12-22 09:37:09				{"publish_date":"2014-12-30T00:00:00","end_date":null,"preamble":"Hurra hurra hurra!!","content":""}
40	21	2	Hurra	hurra	\N	2014-12-15 08:57:28	2014-12-22 09:37:15				{"publish_date":"2014-12-26T00:00:00","end_date":null,"preamble":"God mat","content":""}
\.


--
-- Name: translations_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('translations_id_seq', 108, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY users (id, username, firstname, middlename, lastname, email, phone, password, deleted_at, created_at, updated_at) FROM stdin;
1	root	Symfoni	\N	Administrator	noreply@symfoni.com	\N	b91cd1a54781790beaa2baf741fa6789	\N	2014-12-10 12:48:19	2014-12-10 12:48:19
2	clund	Camilla		Lund	camilla.lund@symfoni.com	95853016	9cce6b5a45a8fe11a97ce4ed7438475c	\N	2014-12-11 14:34:33	2014-12-11 14:34:33
3	olemarius	Ole	Marius	Pentzen	ole.marius@symfoni.com		0ad0bcea46e013932bfc35fa1e07940a	\N	2014-12-11 14:35:49	2014-12-11 14:50:09
5	anne	Anne		Andreassen	anne.andreassen@symfoni.com		c7927b9ab0bd263ea33ed3f860eb8bcb	\N	2014-12-17 12:23:12	2014-12-17 12:23:12
6	tord	Tord	Olaf	Ripe	tord.olaf.ripe@symfoni.com		c7927b9ab0bd263ea33ed3f860eb8bcb	\N	2014-12-22 15:02:51	2014-12-22 15:02:51
4	redrobin	Red		Robin	redrobin@symfoni.com	99887744	c7927b9ab0bd263ea33ed3f860eb8bcb	\N	2014-12-15 18:08:07	2014-12-15 18:08:07
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('users_id_seq', 6, true);


--
-- Data for Name: variations; Type: TABLE DATA; Schema: cms; Owner: -
--

COPY variations (id, media_id, width, height, size, filename, created_at, updated_at) FROM stdin;
1	1	300	375	33277	original.jpg	2014-12-12 11:33:22	2014-12-12 11:33:22
2	1	160	200	13461	thumbnail.jpg	2014-12-12 11:33:22	2014-12-12 11:33:22
3	2	300	225	3850	original.jpg	2014-12-12 11:33:23	2014-12-12 11:33:23
4	2	200	150	1659	thumbnail.jpg	2014-12-12 11:33:23	2014-12-12 11:33:23
5	1	272	340	28628	272x340.jpg	2014-12-12 11:33:51	2014-12-12 11:33:51
6	1	200	250	18305	200x250.jpg	2014-12-12 12:26:59	2014-12-12 12:26:59
7	3	1024	768	561276	original.jpg	2014-12-12 14:03:11	2014-12-12 14:03:11
8	3	200	150	12289	thumbnail.jpg	2014-12-12 14:03:11	2014-12-12 14:03:11
9	4	1024	768	777835	original.jpg	2014-12-12 14:03:12	2014-12-12 14:03:12
10	4	200	150	14883	thumbnail.jpg	2014-12-12 14:03:12	2014-12-12 14:03:12
11	5	1024	768	620888	original.jpg	2014-12-12 14:03:13	2014-12-12 14:03:13
12	5	200	150	20662	thumbnail.jpg	2014-12-12 14:03:13	2014-12-12 14:03:13
13	6	1024	768	879394	original.jpg	2014-12-12 14:03:14	2014-12-12 14:03:14
14	6	200	150	23521	thumbnail.jpg	2014-12-12 14:03:14	2014-12-12 14:03:14
15	7	1024	768	845941	original.jpg	2014-12-12 14:03:14	2014-12-12 14:03:14
16	7	200	150	16038	thumbnail.jpg	2014-12-12 14:03:14	2014-12-12 14:03:14
17	8	1024	768	595284	original.jpg	2014-12-12 14:03:15	2014-12-12 14:03:15
18	8	200	150	16832	thumbnail.jpg	2014-12-12 14:03:15	2014-12-12 14:03:15
21	10	1024	768	775702	original.jpg	2014-12-12 14:03:17	2014-12-12 14:03:17
22	10	200	150	12376	thumbnail.jpg	2014-12-12 14:03:17	2014-12-12 14:03:17
23	11	1024	768	780831	original.jpg	2014-12-12 14:03:18	2014-12-12 14:03:18
24	11	200	150	16244	thumbnail.jpg	2014-12-12 14:03:18	2014-12-12 14:03:18
25	6	340	255	56780	340x255.jpg	2014-12-12 14:03:20	2014-12-12 14:03:20
26	11	100	75	5224	100x75.jpg	2014-12-12 14:28:40	2014-12-12 14:28:40
27	7	100	75	4970	100x75.jpg	2014-12-12 14:28:40	2014-12-12 14:28:40
29	5	100	75	7323	100x75.jpg	2014-12-12 14:28:41	2014-12-12 14:28:41
30	8	100	75	5554	100x75.jpg	2014-12-12 14:28:41	2014-12-12 14:28:41
31	10	100	75	4044	100x75.jpg	2014-12-12 14:28:41	2014-12-12 14:28:41
32	6	100	75	7725	100x75.jpg	2014-12-12 14:28:41	2014-12-12 14:28:41
33	4	100	75	5176	100x75.jpg	2014-12-12 14:28:41	2014-12-12 14:28:41
35	10	340	255	29896	340x255.jpg	2014-12-12 14:37:24	2014-12-12 14:37:24
36	3	100	75	4140	100x75.jpg	2014-12-12 14:37:33	2014-12-12 14:37:33
37	11	500	375	87617	500x375.jpg	2014-12-12 14:43:04	2014-12-12 14:43:04
38	7	500	375	96035	500x375.jpg	2014-12-12 14:43:05	2014-12-12 14:43:05
39	10	500	375	57079	500x375.jpg	2014-12-12 14:43:05	2014-12-12 14:43:05
40	8	500	375	75667	500x375.jpg	2014-12-12 14:43:05	2014-12-12 14:43:05
41	6	500	375	106563	500x375.jpg	2014-12-12 14:43:05	2014-12-12 14:43:05
42	4	500	375	78900	500x375.jpg	2014-12-12 14:43:05	2014-12-12 14:43:05
43	5	500	375	81735	500x375.jpg	2014-12-12 14:43:06	2014-12-12 14:43:06
44	3	500	375	63413	500x375.jpg	2014-12-12 14:43:06	2014-12-12 14:43:06
45	7	340	255	44560	340x255.jpg	2014-12-15 08:58:25	2014-12-15 08:58:25
46	11	340	255	42651	340x255.jpg	2014-12-15 08:58:27	2014-12-15 08:58:27
47	12	454	336	95987	original.png	2014-12-15 11:20:28	2014-12-15 11:20:28
48	12	200	148	42506	thumbnail.png	2014-12-15 11:20:28	2014-12-15 11:20:28
49	12	340	251	98514	340x251.png	2014-12-15 11:20:31	2014-12-15 11:20:31
50	13	454	278	77711	original.png	2014-12-15 11:24:46	2014-12-15 11:24:46
51	13	200	122	34428	thumbnail.png	2014-12-15 11:24:46	2014-12-15 11:24:46
52	13	340	208	78861	340x208.png	2014-12-15 11:24:47	2014-12-15 11:24:47
53	14	934	740	461192	original.png	2014-12-23 16:04:59	2014-12-23 16:04:59
54	14	200	158	57983	thumbnail.png	2014-12-23 16:04:59	2014-12-23 16:04:59
55	14	340	269	158899	340x269.png	2014-12-23 16:05:02	2014-12-23 16:05:02
56	14	400	316	218215	400x316.png	2014-12-23 16:07:02	2014-12-23 16:07:02
57	12	400	296	127970	400x296.png	2014-12-23 16:07:03	2014-12-23 16:07:03
58	14	800	633	846856	800x633.png	2014-12-23 16:10:32	2014-12-23 16:10:32
\.


--
-- Name: variations_id_seq; Type: SEQUENCE SET; Schema: cms; Owner: -
--

SELECT pg_catalog.setval('variations_id_seq', 58, true);


SET search_path = custom, pg_catalog;

--
-- Data for Name: employees; Type: TABLE DATA; Schema: custom; Owner: -
--

COPY employees (id, user_id, addresse, zipcode, city, "position", department, birthdate, info, img, created_at, updated_at) FROM stdin;
2	2	Knud Graahsgt  8	0481	Oslo	\N	\N			\N	2014-12-22 16:32:37	2014-12-22 16:32:37
1	4	Opp ei trapp og inn ei dør	666	Der	\N	\N	1982-07-25	Her kommer det mer info om meg etterhvert	\N	2014-12-22 13:58:50	2014-12-22 13:58:50
\.


--
-- Name: employees_id_seq; Type: SEQUENCE SET; Schema: custom; Owner: -
--

SELECT pg_catalog.setval('employees_id_seq', 2, true);


--
-- Data for Name: groupmembers; Type: TABLE DATA; Schema: custom; Owner: -
--

COPY groupmembers (id, page_id, user_id, admin, created_at, updated_at) FROM stdin;
10	49	4	0	2014-12-22 13:38:13	2014-12-22 13:38:13
12	50	4	0	2014-12-22 15:35:04	2014-12-22 15:35:04
13	58	4	0	2014-12-22 15:35:11	2014-12-22 15:35:11
17	58	3	0	2014-12-22 16:18:48	2014-12-22 16:18:48
18	58	5	1	2014-12-22 16:25:41	2014-12-22 16:25:41
19	68	6	1	2014-12-22 16:39:14	2014-12-22 16:39:14
21	68	3	0	2014-12-22 16:43:05	2014-12-22 16:43:05
25	68	2	0	2014-12-22 16:56:16	2014-12-22 16:56:16
29	82	2	1	2014-12-23 11:35:34	2014-12-23 11:35:34
32	49	2	0	2014-12-23 11:55:49	2014-12-23 11:55:49
33	70	2	0	2014-12-23 12:54:14	2014-12-23 12:54:14
34	82	5	0	2014-12-23 13:20:38	2014-12-23 13:20:38
35	82	3	0	2014-12-23 13:20:38	2014-12-23 13:20:38
\.


--
-- Name: groupmembers_id_seq; Type: SEQUENCE SET; Schema: custom; Owner: -
--

SELECT pg_catalog.setval('groupmembers_id_seq', 35, true);


SET search_path = public, pg_catalog;

--
-- Data for Name: cas_sessions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY cas_sessions (id, tgt, username, ip_address, open_date) FROM stdin;
7	TGT-7-yWxiIG66lySHUWks7HJtAmeBMb7MmqYdOHHxn66x6Rj92hxo3U-cas	olemarius	\N	2014-12-11 14:36:30.658+01
27	TGT-27-jR4ieGJJsubdPfT2a35yGxIPpz3wMfhH20pjN6BasUVfjsosEk-cas	tord	\N	2014-12-23 10:09:47.016+01
28	TGT-28-6dsc76KQNG3gdBRFhJW9WelN6guMa57R5kqv55IU6yywT2XdTM-cas	clund	\N	2014-12-23 10:11:50.534+01
29	TGT-29-BR1b0msmeLlpUUQX71McmVKilbWvg0og4VK7SdBtRFhUCaRcXF-cas	clund	\N	2014-12-23 11:26:21.055+01
30	TGT-30-fQbS1WdOPwdOb2duA4N7JApFGNKf9ULob7MQhIPlMTxDsUX9sh-cas	redrobin	\N	2014-12-23 14:26:14.474+01
\.


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY failed_jobs (id, connection, queue, payload, failed_at) FROM stdin;
\.


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('failed_jobs_id_seq', 1, false);


--
-- Data for Name: laravel_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY laravel_migrations (bundle, name, batch) FROM stdin;
cas	2013_05_15_000001_create_users_table	1
cas	2013_05_15_000002_create_person_table	1
cas	2013_05_15_000003_link_users_and_person_tables	1
cas	2013_05_15_000004_seed_users_and_person_tables	1
cas	2013_05_15_000005_constrain_person_owner_id	1
sites	2013_05_16_000000_create_cms_schemas	1
sites	2013_05_16_000001_create_layouts_table	1
sites	2013_05_16_000002_create_templates_table	1
sites	2013_05_16_000003_create_elements_table	1
sites	2013_05_16_000004_create_menus_table	1
sites	2013_05_16_000005_create_properties_table	1
sites	2013_05_16_000006_create_languages_table	1
sites	2013_05_16_000007_create_help_table	1
sites	2013_05_16_000008_create_pages_table	1
sites	2013_05_16_000009_create_translations_table	1
sites	2013_05_16_000010_create_media_table	1
sites	2013_05_16_000011_create_menu_page_table	1
sites	2013_05_16_000100_seed_default_languages	1
sites	2013_05_16_000101_seed_default_help	1
sites	2013_05_30_091059_create_parameters_table	1
sites	2013_05_30_100840_create_element_instances_table	1
sites	2013_05_30_100858_create_parameter_values_table	1
sites	2013_06_19_071953_create_folders_table	1
sites	2013_06_19_072831_modify_media_table	1
sites	2013_06_25_143348_create_variations_table	1
sites	2013_07_10_135941_create_roles_table	1
sites	2013_07_10_140333_seed_roles_table	1
sites	2013_07_10_140825_create_groups_table	1
sites	2013_07_10_141146_create_profiles_table	1
sites	2013_07_10_141148_create_group_profile_table	1
sites	2013_08_12_140506_create_configurations_table	1
sites	2013_08_12_140641_seed_configurations_table	1
sites	2013_08_13_142038_create_keywords_table	1
sites	2013_08_19_114747_create_media_exports_table	1
sites	2013_08_26_154619_modify_languages_table	1
sites	2013_08_28_092656_add_profile_if_missing	1
sites	2013_08_28_092657_add_author_to_pages_table	1
sites	2013_08_28_093958_add_author_to_media_table	1
sites	2013_09_03_115148_add_sort_column_to_pages	1
sites	2013_09_05_104135_change_summarizer_type	1
sites	2013_09_05_135127_add_default_layout_to_config	1
sites	2013_09_09_090207_add_sort_to_properties_table	1
sites	2013_09_09_130326_add_access_to_configurations_table	1
sites	2013_09_09_133919_add_access_to_pages_table	1
sites	2013_09_09_133940_add_access_to_templates_table	1
sites	2013_09_09_170118_create_media_translation_table	1
sites	2013_09_13_114447_create_likes_table	1
sites	2013_09_13_114452_create_comments_table	1
sites	2013_09_16_120636_add_parent_to_comments	1
sites	2013_09_16_120644_add_comment_to_likes	1
sites	2013_09_17_175336_remove_developer_from_help_menu	1
sites	2013_09_17_175339_add_icons_to_help_menu	1
sites	2013_09_17_175411_add_disabled_to_help_menu	1
sites	2013_09_17_175434_update_online_help	1
sites	2013_09_20_131442_add_keywords_description_to_translation	1
sites	2013_09_20_135205_fix_the_retarded_configuration_table	1
sites	2013_09_26_074947_add_author_hide_to_templates	1
sites	2013_10_08_130150_add_title_to_translations_table	1
sites	2013_10_16_094953_help_menu_change_ess_to_ssp	1
sites	2013_11_05_103455_change_media_description_to_text	1
sites	2013_11_05_122136_strip_extension_from_all_media_filenames	1
sites	2013_11_05_130747_make_media_filename_column_260_chars_long	1
sites	2013_12_02_103504_add_notes_column_to_pages	1
sites	2013_12_02_113948_create_related_pages_table	1
sites	2013_12_02_113952_create_related_media_table	1
sites	2013_12_17_160256_create_scheduler_table	1
sites	2014_01_19_123531_create_function_findpageidfrompath	1
sites	2014_01_27_124919_rename_author_to_author_id_on_pages	1
sites	2014_01_27_143058_rename_author_to_author_id_on_media	1
sites	2014_01_30_111202_create_related_links_table	1
sites	2014_01_30_143800_create_function_geturlforpage	1
sites	2014_01_30_143832_create_translated_pages_view	1
sites	2014_01_30_152133_create_users_view	1
sites	2014_01_30_152728_create_authors_view	1
sites	2014_02_06_095412_create_function_getmenustructure	1
sites	2014_02_11_125904_create_translated_pages_no_uri_view	1
sites	2014_03_24_142050_update_users_view	1
sites	2014_03_26_105804_replace_function_getmenustructure	1
cas	2014_03_27_080944_create_unlink_functions	1
cas	2014_03_27_080956_create_cas_sessions_table	1
cas	2014_03_27_080959_create_locks_table	1
cas	2014_03_27_081006_create_serviceticket_table	1
cas	2014_03_27_081018_create_session_sequence	1
cas	2014_03_27_081028_create_ticketgrantingticket_table	1
cas	2014_03_27_084541_establish_relationships	1
sites	2014_03_27_100609_create_keyword_collections_table	1
sites	2014_03_27_101317_add_collection_id_to_keywords_table	1
sites	2014_03_31_110713_create_keyword_page_table	1
sites	2014_03_31_132553_add_custom_url_to_pages	1
sites	2014_03_31_134021_add_custom_url_to_function_getmenustructure	1
cas	2014_04_30_075324_create_suite_properties_table	1
cas	2014_04_30_084751_seed_suite_properties_table	1
sites	2014_05_14_065105_add_content_column_to_translations	1
sites	2014_05_14_065705_convert_content_to_json	1
sites	2014_05_14_093125_add_content_to_translated_pages_view	1
sites	2014_05_15_092258_change_custom_url_to_redirect	1
sites	2014_05_19_135944_change_template_summarizer_to_text	1
sites	2014_05_26_173341_set_default_value_for_new_window_on_related_links	1
sites	2014_05_27_065251_change_status_enumeration_values_for_pages	1
sites	2014_05_27_082249_make_keyword_description_nullable	1
sites	2014_05_27_144351_make_element_query_nullable	1
sites	2014_05_27_144941_add_author_id_to_elements	1
sites	2014_05_27_144945_add_author_id_to_templates	1
sites	2014_05_27_144948_add_author_id_to_menus	1
sites	2014_05_27_144951_add_author_id_to_layouts	1
sites	2014_06_02_135643_change_translation_subject_and_slug_to_text	1
sites	2014_06_02_135912_create_filter_translation_trigger	1
sites	2014_06_02_140826_create_filter_pages_trigger	1
sites	2014_06_02_144640_drop_default_and_allow_null_on_pages_sort	1
cas	2014_06_03_211733_rename_person_e_mail_to_email	1
sites	2014_06_06_092945_convert_odd_jpg_media_types	1
sites	2014_06_10_110903_change_status_in_getmenustructure_function	1
sites	2014_06_10_131324_add_content_type_to_template	1
sites	2014_06_11_081738_add_dictionary_column_to_languages	1
sites	2014_06_11_113426_create_search_index_table	1
sites	2014_06_11_114750_create_function_convert_content_to_text	1
sites	2014_06_11_125529_create_search_index_for_current_content	1
sites	2014_06_11_132747_create_update_translation_index_trigger	1
sites	2014_06_26_142909_add_site_description_to_configurations	1
sites	2014_07_15_122336_change_guide_links_in_help_data	1
sites	2014_08_11_194617_create_data_definitions_table	1
sites	2014_08_11_194634_create_data_attributes_table	1
sites	2014_08_11_194842_create_data_items_table	1
sites	2014_08_14_105805_add_index_column_to_properties_table	1
sites	2014_08_14_105806_add_index_column_to_templates_table	1
sites	2014_08_14_105807_add_index_column_to_pages_table	1
sites	2014_08_14_105808_change_convert_content_to_text_function_to_convert_page_to_text	1
sites	2014_08_14_111530_use_convert_page_in_translation_index_trigger	1
sites	2014_08_14_120222_recreate_the_search_index_for_all_pages	1
sites	2014_08_18_141443_create_rename_property_in_content_function	1
sites	2014_08_18_141743_create_property_renamed_trigger	1
sites	2014_08_18_152927_change_properties_columns_from_string_to_text	1
sites	2014_08_20_095607_remove_query_column_from_elements_table	1
sites	2014_08_25_114005_create_users_table	1
sites	2014_08_25_121619_transfer_user_data_from_cas_tables_to_sites	1
sites	2014_08_25_125431_make_profiles_reference_new_users_table	1
sites	2014_08_25_130027_make_likes_reference_new_users_table	1
sites	2014_08_25_130034_make_comments_reference_new_users_table	1
cas	2014_08_27_111831_create_sites_to_cas_user_synchronization_trigger	1
cas	2014_08_27_111841_create_cas_to_sites_user_synchronization_trigger	1
cas	2014_08_27_111851_create_cas_to_sites_person_synchronization_trigger	1
sites	2014_08_28_132326_create_media_index_table	1
sites	2014_08_28_132845_add_language_column_to_media_table	1
sites	2014_08_28_132945_add_index_column_to_media_table	1
sites	2014_08_29_060845_create_search_index_for_current_media	1
sites	2014_08_29_063300_create_media_extracts_table	1
sites	2014_08_29_065833_create_update_media_index_trigger	1
sites	2014_08_29_115329_create_updated_media_index_extracts_trigger	1
sites	2014_09_01_122317_create_failed_jobs_table	1
sites	2014_09_08_104010_change_all_profiles_relationships_to_users	1
sites	2014_09_08_133358_create_role_user_table	1
sites	2014_09_08_134056_move_roles_from_profiles_into_pivot_table	1
sites	2014_09_08_141748_add_builtin_column_to_roles_table	1
sites	2014_09_08_142125_fix_the_default_roles	1
sites	2014_09_08_144848_create_permission_categories_table	1
sites	2014_09_08_145058_create_permissions_table	1
sites	2014_09_08_145406_create_permission_role_table	1
sites	2014_09_09_172432_drop_group_profile_table	1
sites	2014_09_09_172437_drop_groups_table	1
sites	2014_09_10_070809_seed_default_permissions	1
sites	2014_09_10_113702_add_user_role_to_all_users	1
cas	2014_09_12_093106_allow_person_owner_id_to_be_null	1
sites	2014_09_23_073312_create_channels_table	1
sites	2014_09_24_131426_add_root_to_findpageidfrompath	1
sites	2014_09_25_084327_add_root_to_geturlforpage	1
sites	2014_09_25_152926_use_session_settings_in_translated_pages_view	1
sites	2014_09_25_202629_cleaning_up_getmenustructure_function	1
sites	2014_09_29_074204_create_translated_pages_subtree_view	1
sites	2014_09_29_102318_create_pages_subtree_view	1
application	2014_12_17_143102_create_schema_custom	2
application	2014_12_17_143246_create_table_groupmembers	3
application	2014_12_22_122004_create_table_employees	4
\.


--
-- Data for Name: locks; Type: TABLE DATA; Schema: public; Owner: -
--

COPY locks (application_id, expiration_date, unique_id) FROM stdin;
cas	\N	\N
\.


--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: -
--

COPY person (id, department_id, company_id, firstname, middlename, lastname, job_title, persont_status_type_id, is_external, is_history, is_male, email, is_trash, is_restricted, owner_id, imported_responsible, import_id, contact_id, created_date, modified_date, old_id) FROM stdin;
1	\N	0	Symfoni	\N	Administrator	\N	1	f	f	\N	noreply@symfoni.com	f	f	\N	\N	\N	\N	2014-12-10 12:48:19+01	2014-12-10 12:48:19+01	\N
2	\N	0	Camilla		Lund	\N	1	f	f	\N	camilla.lund@symfoni.com	f	f	\N	\N	\N	\N	2014-12-11 14:34:33+01	2014-12-11 14:34:33+01	\N
3	\N	0	Ole	Marius	Pentzen	\N	1	f	f	\N	ole.marius@symfoni.com	f	f	\N	\N	\N	\N	2014-12-11 14:35:49+01	2014-12-11 14:50:09+01	\N
5	\N	0	Anne		Andreassen	\N	1	f	f	\N	anne.andreassen@symfoni.com	f	f	\N	\N	\N	\N	2014-12-17 12:23:12+01	2014-12-17 12:23:12+01	\N
6	\N	0	Tord	Olaf	Ripe	\N	1	f	f	\N	tord.olaf.ripe@symfoni.com	f	f	\N	\N	\N	\N	2014-12-22 15:02:51+01	2014-12-22 15:02:51+01	\N
4	\N	0	Red		Robin	\N	1	f	f	\N	redrobin@symfoni.com	f	f	\N	\N	\N	\N	2014-12-15 18:08:07+01	2014-12-15 18:08:07+01	\N
\.


--
-- Data for Name: serviceticket; Type: TABLE DATA; Schema: public; Owner: -
--

COPY serviceticket (id, number_of_times_used, creation_time, expiration_policy, last_time_used, previous_last_time_used, from_new_login, ticket_already_granted, service, ticketgrantingticket_id) FROM stdin;
\.


--
-- Name: session_sequence; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('session_sequence', 30, true);


--
-- Data for Name: suite_properties; Type: TABLE DATA; Schema: public; Owner: -
--

COPY suite_properties (id, property_name, property_value) FROM stdin;
1000001	com.symfoni.suite.config.settings.ConfigSettings	75182
\.


--
-- Data for Name: symfoni_scheduler; Type: TABLE DATA; Schema: public; Owner: -
--

COPY symfoni_scheduler (id, hash, "timestamp") FROM stdin;
\.


--
-- Name: symfoni_scheduler_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('symfoni_scheduler_id_seq', 1, false);


--
-- Data for Name: ticketgrantingticket; Type: TABLE DATA; Schema: public; Owner: -
--

COPY ticketgrantingticket (id, number_of_times_used, creation_time, expiration_policy, last_time_used, previous_last_time_used, authentication, expired, services_granted_access_to, ticketgrantingticket_id) FROM stdin;
TGT-7-yWxiIG66lySHUWks7HJtAmeBMb7MmqYdOHHxn66x6Rj92hxo3U-cas	14	1418304990657	76350	1419318566949	1419261149998	76351	f	76352	\N
TGT-27-jR4ieGJJsubdPfT2a35yGxIPpz3wMfhH20pjN6BasUVfjsosEk-cas	1	1419325787016	76385	1419325787044	1419325787016	76386	f	76387	\N
TGT-28-6dsc76KQNG3gdBRFhJW9WelN6guMa57R5kqv55IU6yywT2XdTM-cas	1	1419325910533	76395	1419325910559	1419325910533	76396	f	76397	\N
TGT-29-BR1b0msmeLlpUUQX71McmVKilbWvg0og4VK7SdBtRFhUCaRcXF-cas	1	1419330381055	76405	1419330381079	1419330381055	76406	f	76407	\N
TGT-30-fQbS1WdOPwdOb2duA4N7JApFGNKf9ULob7MQhIPlMTxDsUX9sh-cas	1	1419341174473	76435	1419341174492	1419341174473	76436	f	76437	\N
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY users (login, password, id, is_enabled, created_date, email, private_folder_id) FROM stdin;
root	b91cd1a54781790beaa2baf741fa6789	1	t	2014-12-10	noreply@symfoni.com	\N
clund	9cce6b5a45a8fe11a97ce4ed7438475c	2	t	2014-12-11	camilla.lund@symfoni.com	\N
olemarius	0ad0bcea46e013932bfc35fa1e07940a	3	t	2014-12-11	ole.marius@symfoni.com	\N
anne	c7927b9ab0bd263ea33ed3f860eb8bcb	5	t	2014-12-17	anne.andreassen@symfoni.com	\N
tord	c7927b9ab0bd263ea33ed3f860eb8bcb	6	t	2014-12-22	tord.olaf.ripe@symfoni.com	\N
redrobin	c7927b9ab0bd263ea33ed3f860eb8bcb	4	t	2014-12-15	redrobin@symfoni.com	\N
\.


--
-- Data for Name: BLOBS; Type: BLOBS; Schema: -; Owner: -
--

SET search_path = pg_catalog;

BEGIN;

SELECT pg_catalog.lo_open('75182', 131072);
SELECT pg_catalog.lowrite(0, '\x7b226d696e50617373776f72644c656e677468223a362c2264656661756c74416c6c6f776564416363657373223a747275652c226f726465725472657368686f6c64223a3130302c226c697374656e65724973416c69766544656c6179223a31302c2264656661756c745061676553697a65223a31352c2264656661756c7450617373776f7264223a226368616e67656974222c226c696d6974223a33313435373238302c22656d707479223a66616c73657d');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76350', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200416f72672e6a617369672e6361732e7469636b65742e737570706f72742e52656d656d6265724d6544656c65676174696e6745787069726174696f6e506f6c696379f804abe89ab282b30200024c001a72656d656d6265724d6545787069726174696f6e506f6c6963797400274c6f72672f6a617369672f6361732f7469636b65742f45787069726174696f6e506f6c6963793b4c001773657373696f6e45787069726174696f6e506f6c69637971007e00017870737200346f72672e6a617369672e6361732e7469636b65742e737570706f72742e54696d656f757445787069726174696f6e506f6c69637931343030333331370200014a001874696d65546f4b696c6c496e4d696c6c695365636f6e6473787000000000240c84007371007e00030000000005265c00');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76351', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200346f72672e6a617369672e6361732e61757468656e7469636174696f6e2e496d6d757461626c6541757468656e7469636174696f6e36373330393439330200014c001161757468656e74696361746564446174657400104c6a6176612f7574696c2f446174653b787200336f72672e6a617369672e6361732e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e5faac07dc8d4edbe0200024c000a6174747269627574657374000f4c6a6176612f7574696c2f4d61703b4c00097072696e636970616c7400324c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f5072696e636970616c3b7870737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00037870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f40000000000001770800000002000000027400326f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e52454d454d4245525f4d45737200116a6176612e6c616e672e426f6f6c65616ecd207280d59cfaee0200015a000576616c756578700174001461757468656e7469636174696f6e4d6574686f64740036636f6d2e73796d666f6e692e6361732e61646170746f72732e446174616261736541757468656e7469636174696f6e48616e646c657278737200366f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655072696e636970616cb6ecc25683597de50200024c000a6174747269627574657371007e00034c000269647400124c6a6176612f6c616e672f537472696e673b78707371007e00067371007e00083f400000000000037708000000040000000374000375696471007e001474000f67726f75704d656d6265727368697071007e0015740014656475506572736f6e416666696c696174696f6e71007e0016787400096f6c656d61726975737372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000014a3990adc178');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76352', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000187708000000200000000e74001e53542d34332d4a756e5a777a5a66364c65725a4c4832685264792d636173737200466f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655765624170706c69636174696f6e53657276696365496d706c73a88f8fa06df1da0200014c000c726573706f6e73655479706574003e4c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f526573706f6e736524526573706f6e7365547970653b787200446f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e41627374726163745765624170706c69636174696f6e53657276696365e9ded4f892672d590200065a00106c6f676765644f7574416c72656164794c000a617274696661637449647400124c6a6176612f6c616e672f537472696e673b4c000a68747470436c69656e7474001f4c6f72672f6a617369672f6361732f7574696c2f48747470436c69656e743b4c0002696471007e00064c000b6f726967696e616c55726c71007e00064c00097072696e636970616c7400324c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f5072696e636970616c3b787000707372001d6f72672e6a617369672e6361732e7574696c2e48747470436c69656e74b65aad47ecc46b14020004490011636f6e6e656374696f6e54696d656f75745a000f666f6c6c6f7752656469726563747349000b7265616454696d656f75745b000f61636365707461626c65436f6465737400025b497870000013880000001388757200025b494dba602676eab2a5020000787000000005000000c8000001300000012e0000012d000000ca74001c687474703a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f71007e000f737200366f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655072696e636970616cb6ecc25683597de50200024c000a6174747269627574657374000f4c6a6176612f7574696c2f4d61703b4c0002696471007e00067870737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e001178707371007e00003f400000000000037708000000040000000374000375696471007e001674000f67726f75704d656d6265727368697071007e0017740014656475506572736f6e416666696c696174696f6e71007e0018787400096f6c656d61726975737e72003c6f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e526573706f6e736524526573706f6e73655479706500000000000000001200007872000e6a6176612e6c616e672e456e756d00000000000000001200007870740008524544495245435474001e53542d33322d566d5070586576304267675442564d78737a32512d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74002768747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f636d732f61646d696e71007e00227371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e002674000f67726f75704d656d6265727368697071007e0027740014656475506572736f6e416666696c696174696f6e71007e0028787400096f6c656d617269757371007e001c74001e53542d33342d68707952694c4b69484876644944307761676b582d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74002768747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f636d732f61646d696e71007e002e7371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e003274000f67726f75704d656d6265727368697071007e0033740014656475506572736f6e416666696c696174696f6e71007e0034787400096f6c656d617269757371007e001c74001d53542d372d624965666539364659414d6175747762516b554c2d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74002768747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f636d732f61646d696e71007e003a7371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e003e74000f67726f75704d656d6265727368697071007e003f740014656475506572736f6e416666696c696174696f6e71007e0040787400096f6c656d617269757371007e001c74001d53542d392d526572714d42456d4168663967543142524647582d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74002768747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f636d732f61646d696e71007e00467371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e004a74000f67726f75704d656d6265727368697071007e004b740014656475506572736f6e416666696c696174696f6e71007e004c787400096f6c656d617269757371007e001c74001e53542d32382d78447779464c5a7132584331436457637a3148562d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74002768747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f636d732f61646d696e71007e00527371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e005674000f67726f75704d656d6265727368697071007e0057740014656475506572736f6e416666696c696174696f6e71007e0058787400096f6c656d617269757371007e001c74001e53542d35302d57665759684258705879423376756e53506566472d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca740025687474703a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f746573742d63616c71007e005e7371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e006274000f67726f75704d656d6265727368697071007e0063740014656475506572736f6e416666696c696174696f6e71007e0064787400096f6c656d617269757371007e001c74001e53542d32372d4e44466667575678686a7a44496c4d4d5a6c32562d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74002768747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f636d732f61646d696e71007e006a7371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e006e74000f67726f75704d656d6265727368697071007e006f740014656475506572736f6e416666696c696174696f6e71007e0070787400096f6c656d617269757371007e001c74001e53542d34372d356834316741726d625532574b504a6776616e482d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74001c687474703a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f71007e00767371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e007a74000f67726f75704d656d6265727368697071007e007b740014656475506572736f6e416666696c696174696f6e71007e007c787400096f6c656d617269757371007e001c74001e53542d31342d335a5165595864723239615a56615971344f4c412d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74002768747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f636d732f61646d696e71007e00827371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e008674000f67726f75704d656d6265727368697071007e0087740014656475506572736f6e416666696c696174696f6e71007e0088787400096f6c656d617269757371007e001c74001d53542d382d45705677315469357973783272717074314b55532d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74002768747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f636d732f61646d696e71007e008e7371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e009274000f67726f75704d656d6265727368697071007e0093740014656475506572736f6e416666696c696174696f6e71007e0094787400096f6c656d617269757371007e001c74001e53542d34322d41625967674748314f78566759587948305676642d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74002768747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f636d732f61646d696e71007e009a7371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e009e74000f67726f75704d656d6265727368697071007e009f740014656475506572736f6e416666696c696174696f6e71007e00a0787400096f6c656d617269757371007e001c74001e53542d31302d7a4448726a4574484153586553593149517175362d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74002768747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f636d732f61646d696e71007e00a67371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e00aa74000f67726f75704d656d6265727368697071007e00ab740014656475506572736f6e416666696c696174696f6e71007e00ac787400096f6c656d617269757371007e001c74001e53542d34392d54584c6b505a783948743941474c7a49363178312d6361737371007e000300707371007e000a0000138800000013887571007e000d00000005000000c8000001300000012e0000012d000000ca74001c687474703a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f71007e00b27371007e00107371007e00137371007e00003f400000000000037708000000040000000374000375696471007e00b674000f67726f75704d656d6265727368697071007e00b7740014656475506572736f6e416666696c696174696f6e71007e00b8787400096f6c656d617269757371007e001c78');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76395', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200416f72672e6a617369672e6361732e7469636b65742e737570706f72742e52656d656d6265724d6544656c65676174696e6745787069726174696f6e506f6c696379f804abe89ab282b30200024c001a72656d656d6265724d6545787069726174696f6e506f6c6963797400274c6f72672f6a617369672f6361732f7469636b65742f45787069726174696f6e506f6c6963793b4c001773657373696f6e45787069726174696f6e506f6c69637971007e00017870737200346f72672e6a617369672e6361732e7469636b65742e737570706f72742e54696d656f757445787069726174696f6e506f6c69637931343030333331370200014a001874696d65546f4b696c6c496e4d696c6c695365636f6e6473787000000000240c84007371007e00030000000005265c00');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76396', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200346f72672e6a617369672e6361732e61757468656e7469636174696f6e2e496d6d757461626c6541757468656e7469636174696f6e36373330393439330200014c001161757468656e74696361746564446174657400104c6a6176612f7574696c2f446174653b787200336f72672e6a617369672e6361732e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e5faac07dc8d4edbe0200024c000a6174747269627574657374000f4c6a6176612f7574696c2f4d61703b4c00097072696e636970616c7400324c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f5072696e636970616c3b7870737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00037870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000007708000000010000000174001461757468656e7469636174696f6e4d6574686f64740036636f6d2e73796d666f6e692e6361732e61646170746f72732e446174616261736541757468656e7469636174696f6e48616e646c657278737200366f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655072696e636970616cb6ecc25683597de50200024c000a6174747269627574657371007e00034c000269647400124c6a6176612f6c616e672f537472696e673b78707371007e00067371007e00083f400000000000037708000000040000000374000375696471007e0011740014656475506572736f6e416666696c696174696f6e71007e001274000f67726f75704d656d6265727368697071007e001378740005636c756e647372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000014a766aae0578');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76397', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000007708000000010000000174001e53542d35322d666b6366515036644c646733656c6b327673704b2d636173737200466f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655765624170706c69636174696f6e53657276696365496d706c73a88f8fa06df1da0200014c000c726573706f6e73655479706574003e4c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f526573706f6e736524526573706f6e7365547970653b787200446f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e41627374726163745765624170706c69636174696f6e53657276696365e9ded4f892672d590200065a00106c6f676765644f7574416c72656164794c000a617274696661637449647400124c6a6176612f6c616e672f537472696e673b4c000a68747470436c69656e7474001f4c6f72672f6a617369672f6361732f7574696c2f48747470436c69656e743b4c0002696471007e00064c000b6f726967696e616c55726c71007e00064c00097072696e636970616c7400324c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f5072696e636970616c3b787000707372001d6f72672e6a617369672e6361732e7574696c2e48747470436c69656e74b65aad47ecc46b14020004490011636f6e6e656374696f6e54696d656f75745a000f666f6c6c6f7752656469726563747349000b7265616454696d656f75745b000f61636365707461626c65436f6465737400025b497870000013880000001388757200025b494dba602676eab2a5020000787000000005000000c8000001300000012e0000012d000000ca74003b68747470733a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f2f73616e6e6b617373656e2f677275707065722f6f656c6b6c756262656e71007e000f737200366f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655072696e636970616cb6ecc25683597de50200024c000a6174747269627574657374000f4c6a6176612f7574696c2f4d61703b4c0002696471007e00067870737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e001178707371007e00003f400000000000037708000000040000000374000375696471007e0016740014656475506572736f6e416666696c696174696f6e71007e001774000f67726f75704d656d6265727368697071007e001878740005636c756e647e72003c6f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e526573706f6e736524526573706f6e73655479706500000000000000001200007872000e6a6176612e6c616e672e456e756d00000000000000001200007870740008524544495245435478');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76435', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200416f72672e6a617369672e6361732e7469636b65742e737570706f72742e52656d656d6265724d6544656c65676174696e6745787069726174696f6e506f6c696379f804abe89ab282b30200024c001a72656d656d6265724d6545787069726174696f6e506f6c6963797400274c6f72672f6a617369672f6361732f7469636b65742f45787069726174696f6e506f6c6963793b4c001773657373696f6e45787069726174696f6e506f6c69637971007e00017870737200346f72672e6a617369672e6361732e7469636b65742e737570706f72742e54696d656f757445787069726174696f6e506f6c69637931343030333331370200014a001874696d65546f4b696c6c496e4d696c6c695365636f6e6473787000000000240c84007371007e00030000000005265c00');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76436', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200346f72672e6a617369672e6361732e61757468656e7469636174696f6e2e496d6d757461626c6541757468656e7469636174696f6e36373330393439330200014c001161757468656e74696361746564446174657400104c6a6176612f7574696c2f446174653b787200336f72672e6a617369672e6361732e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e5faac07dc8d4edbe0200024c000a6174747269627574657374000f4c6a6176612f7574696c2f4d61703b4c00097072696e636970616c7400324c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f5072696e636970616c3b7870737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00037870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000007708000000010000000174001461757468656e7469636174696f6e4d6574686f64740036636f6d2e73796d666f6e692e6361732e61646170746f72732e446174616261736541757468656e7469636174696f6e48616e646c657278737200366f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655072696e636970616cb6ecc25683597de50200024c000a6174747269627574657371007e00034c000269647400124c6a6176612f6c616e672f537472696e673b78707371007e00067371007e00083f400000000000037708000000040000000374000375696471007e0011740014656475506572736f6e416666696c696174696f6e71007e001274000f67726f75704d656d6265727368697071007e001378740008726564726f62696e7372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000014a775396c978');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76437', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000007708000000010000000174001e53542d35342d41366f6544494972623156754e48694e42676d6a2d636173737200466f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655765624170706c69636174696f6e53657276696365496d706c73a88f8fa06df1da0200014c000c726573706f6e73655479706574003e4c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f526573706f6e736524526573706f6e7365547970653b787200446f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e41627374726163745765624170706c69636174696f6e53657276696365e9ded4f892672d590200065a00106c6f676765644f7574416c72656164794c000a617274696661637449647400124c6a6176612f6c616e672f537472696e673b4c000a68747470436c69656e7474001f4c6f72672f6a617369672f6361732f7574696c2f48747470436c69656e743b4c0002696471007e00064c000b6f726967696e616c55726c71007e00064c00097072696e636970616c7400324c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f5072696e636970616c3b787000707372001d6f72672e6a617369672e6361732e7574696c2e48747470436c69656e74b65aad47ecc46b14020004490011636f6e6e656374696f6e54696d656f75745a000f666f6c6c6f7752656469726563747349000b7265616454696d656f75745b000f61636365707461626c65436f6465737400025b497870000013880000001388757200025b494dba602676eab2a5020000787000000005000000c8000001300000012e0000012d000000ca74001c687474703a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f71007e000f737200366f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655072696e636970616cb6ecc25683597de50200024c000a6174747269627574657374000f4c6a6176612f7574696c2f4d61703b4c0002696471007e00067870737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e001178707371007e00003f400000000000037708000000040000000374000375696471007e0016740014656475506572736f6e416666696c696174696f6e71007e001774000f67726f75704d656d6265727368697071007e001878740008726564726f62696e7e72003c6f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e526573706f6e736524526573706f6e73655479706500000000000000001200007872000e6a6176612e6c616e672e456e756d00000000000000001200007870740008524544495245435478');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76385', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200416f72672e6a617369672e6361732e7469636b65742e737570706f72742e52656d656d6265724d6544656c65676174696e6745787069726174696f6e506f6c696379f804abe89ab282b30200024c001a72656d656d6265724d6545787069726174696f6e506f6c6963797400274c6f72672f6a617369672f6361732f7469636b65742f45787069726174696f6e506f6c6963793b4c001773657373696f6e45787069726174696f6e506f6c69637971007e00017870737200346f72672e6a617369672e6361732e7469636b65742e737570706f72742e54696d656f757445787069726174696f6e506f6c69637931343030333331370200014a001874696d65546f4b696c6c496e4d696c6c695365636f6e6473787000000000240c84007371007e00030000000005265c00');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76386', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200346f72672e6a617369672e6361732e61757468656e7469636174696f6e2e496d6d757461626c6541757468656e7469636174696f6e36373330393439330200014c001161757468656e74696361746564446174657400104c6a6176612f7574696c2f446174653b787200336f72672e6a617369672e6361732e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e5faac07dc8d4edbe0200024c000a6174747269627574657374000f4c6a6176612f7574696c2f4d61703b4c00097072696e636970616c7400324c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f5072696e636970616c3b7870737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00037870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000007708000000010000000174001461757468656e7469636174696f6e4d6574686f64740036636f6d2e73796d666f6e692e6361732e61646170746f72732e446174616261736541757468656e7469636174696f6e48616e646c657278737200366f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655072696e636970616cb6ecc25683597de50200024c000a6174747269627574657371007e00034c000269647400124c6a6176612f6c616e672f537472696e673b78707371007e00067371007e00083f400000000000037708000000040000000374000375696471007e0011740014656475506572736f6e416666696c696174696f6e71007e001274000f67726f75704d656d6265727368697071007e001378740004746f72647372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000014a7668cb8778');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76387', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000007708000000010000000174001e53542d35312d4774456a6b7968394b72566a57524d6654714c442d636173737200466f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655765624170706c69636174696f6e53657276696365496d706c73a88f8fa06df1da0200014c000c726573706f6e73655479706574003e4c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f526573706f6e736524526573706f6e7365547970653b787200446f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e41627374726163745765624170706c69636174696f6e53657276696365e9ded4f892672d590200065a00106c6f676765644f7574416c72656164794c000a617274696661637449647400124c6a6176612f6c616e672f537472696e673b4c000a68747470436c69656e7474001f4c6f72672f6a617369672f6361732f7574696c2f48747470436c69656e743b4c0002696471007e00064c000b6f726967696e616c55726c71007e00064c00097072696e636970616c7400324c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f5072696e636970616c3b787000707372001d6f72672e6a617369672e6361732e7574696c2e48747470436c69656e74b65aad47ecc46b14020004490011636f6e6e656374696f6e54696d656f75745a000f666f6c6c6f7752656469726563747349000b7265616454696d656f75745b000f61636365707461626c65436f6465737400025b497870000013880000001388757200025b494dba602676eab2a5020000787000000005000000c8000001300000012e0000012d000000ca74001c687474703a2f2f6465766e67692e73796d666f6e692e636f6d2f6e6f71007e000f737200366f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655072696e636970616cb6ecc25683597de50200024c000a6174747269627574657374000f4c6a6176612f7574696c2f4d61703b4c0002696471007e00067870737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e001178707371007e00003f400000000000037708000000040000000374000375696471007e0016740014656475506572736f6e416666696c696174696f6e71007e001774000f67726f75704d656d6265727368697071007e001878740004746f72647e72003c6f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e526573706f6e736524526573706f6e73655479706500000000000000001200007872000e6a6176612e6c616e672e456e756d00000000000000001200007870740008524544495245435478');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76405', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200416f72672e6a617369672e6361732e7469636b65742e737570706f72742e52656d656d6265724d6544656c65676174696e6745787069726174696f6e506f6c696379f804abe89ab282b30200024c001a72656d656d6265724d6545787069726174696f6e506f6c6963797400274c6f72672f6a617369672f6361732f7469636b65742f45787069726174696f6e506f6c6963793b4c001773657373696f6e45787069726174696f6e506f6c69637971007e00017870737200346f72672e6a617369672e6361732e7469636b65742e737570706f72742e54696d656f757445787069726174696f6e506f6c69637931343030333331370200014a001874696d65546f4b696c6c496e4d696c6c695365636f6e6473787000000000240c84007371007e00030000000005265c00');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76406', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200346f72672e6a617369672e6361732e61757468656e7469636174696f6e2e496d6d757461626c6541757468656e7469636174696f6e36373330393439330200014c001161757468656e74696361746564446174657400104c6a6176612f7574696c2f446174653b787200336f72672e6a617369672e6361732e61757468656e7469636174696f6e2e416273747261637441757468656e7469636174696f6e5faac07dc8d4edbe0200024c000a6174747269627574657374000f4c6a6176612f7574696c2f4d61703b4c00097072696e636970616c7400324c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f5072696e636970616c3b7870737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e00037870737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000007708000000010000000174001461757468656e7469636174696f6e4d6574686f64740036636f6d2e73796d666f6e692e6361732e61646170746f72732e446174616261736541757468656e7469636174696f6e48616e646c657278737200366f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655072696e636970616cb6ecc25683597de50200024c000a6174747269627574657371007e00034c000269647400124c6a6176612f6c616e672f537472696e673b78707371007e00067371007e00083f400000000000037708000000040000000374000375696471007e0011740014656475506572736f6e416666696c696174696f6e71007e001274000f67726f75704d656d6265727368697071007e001378740005636c756e647372000e6a6176612e7574696c2e44617465686a81014b597419030000787077080000014a76aee4fe78');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('76407', 131072);
SELECT pg_catalog.lowrite(0, '\xaced0005737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f400000000000007708000000010000000174001e53542d35332d654d4559326a41336e3961394f515067653178642d636173737200466f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655765624170706c69636174696f6e53657276696365496d706c73a88f8fa06df1da0200014c000c726573706f6e73655479706574003e4c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f526573706f6e736524526573706f6e7365547970653b787200446f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e41627374726163745765624170706c69636174696f6e53657276696365e9ded4f892672d590200065a00106c6f676765644f7574416c72656164794c000a617274696661637449647400124c6a6176612f6c616e672f537472696e673b4c000a68747470436c69656e7474001f4c6f72672f6a617369672f6361732f7574696c2f48747470436c69656e743b4c0002696471007e00064c000b6f726967696e616c55726c71007e00064c00097072696e636970616c7400324c6f72672f6a617369672f6361732f61757468656e7469636174696f6e2f7072696e636970616c2f5072696e636970616c3b787000707372001d6f72672e6a617369672e6361732e7574696c2e48747470436c69656e74b65aad47ecc46b14020004490011636f6e6e656374696f6e54696d656f75745a000f666f6c6c6f7752656469726563747349000b7265616454696d656f75745b000f61636365707461626c65436f6465737400025b497870000013880000001388757200025b494dba602676eab2a5020000787000000005000000c8000001300000012e0000012d000000ca740023687474703a2f2f6465766e67692e73796d666f6e692e636f6d2f636d732f61646d696e71007e000f737200366f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e53696d706c655072696e636970616cb6ecc25683597de50200024c000a6174747269627574657374000f4c6a6176612f7574696c2f4d61703b4c0002696471007e00067870737200256a6176612e7574696c2e436f6c6c656374696f6e7324556e6d6f6469666961626c654d6170f1a5a8fe74f507420200014c00016d71007e001178707371007e00003f400000000000037708000000040000000374000375696471007e0016740014656475506572736f6e416666696c696174696f6e71007e001774000f67726f75704d656d6265727368697071007e001878740005636c756e647e72003c6f72672e6a617369672e6361732e61757468656e7469636174696f6e2e7072696e636970616c2e526573706f6e736524526573706f6e73655479706500000000000000001200007872000e6a6176612e6c616e672e456e756d00000000000000001200007870740008524544495245435478');
SELECT pg_catalog.lo_close(0);

COMMIT;

SET search_path = cms, pg_catalog;

--
-- Name: channels_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (id);


--
-- Name: cms_data_definitions_keyword_unique; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY data_definitions
    ADD CONSTRAINT cms_data_definitions_keyword_unique UNIQUE (keyword);


--
-- Name: cms_elements_keyword_unique; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY elements
    ADD CONSTRAINT cms_elements_keyword_unique UNIQUE (keyword);


--
-- Name: cms_keyword_collections_name_unique; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY keyword_collections
    ADD CONSTRAINT cms_keyword_collections_name_unique UNIQUE (name);


--
-- Name: cms_layouts_keyword_unique; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY layouts
    ADD CONSTRAINT cms_layouts_keyword_unique UNIQUE (keyword);


--
-- Name: cms_menus_keyword_unique; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY menus
    ADD CONSTRAINT cms_menus_keyword_unique UNIQUE (keyword);


--
-- Name: cms_permission_categories_keyword_unique; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY permission_categories
    ADD CONSTRAINT cms_permission_categories_keyword_unique UNIQUE (keyword);


--
-- Name: cms_permissions_keyword_unique; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY permissions
    ADD CONSTRAINT cms_permissions_keyword_unique UNIQUE (keyword);


--
-- Name: cms_templates_keyword_unique; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY templates
    ADD CONSTRAINT cms_templates_keyword_unique UNIQUE (keyword);


--
-- Name: cms_users_username_unique; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT cms_users_username_unique UNIQUE (username);


--
-- Name: comments_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: configurations_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY configurations
    ADD CONSTRAINT configurations_pkey PRIMARY KEY (id);


--
-- Name: data_attributes_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY data_attributes
    ADD CONSTRAINT data_attributes_pkey PRIMARY KEY (id);


--
-- Name: data_definitions_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY data_definitions
    ADD CONSTRAINT data_definitions_pkey PRIMARY KEY (id);


--
-- Name: data_items_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY data_items
    ADD CONSTRAINT data_items_pkey PRIMARY KEY (id);


--
-- Name: element_instances_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY element_instances
    ADD CONSTRAINT element_instances_pkey PRIMARY KEY (id);


--
-- Name: elements_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY elements
    ADD CONSTRAINT elements_pkey PRIMARY KEY (id);


--
-- Name: folders_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY folders
    ADD CONSTRAINT folders_pkey PRIMARY KEY (id);


--
-- Name: help_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY help
    ADD CONSTRAINT help_pkey PRIMARY KEY (id);


--
-- Name: keyword_collections_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY keyword_collections
    ADD CONSTRAINT keyword_collections_pkey PRIMARY KEY (id);


--
-- Name: keyword_page_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY keyword_page
    ADD CONSTRAINT keyword_page_pkey PRIMARY KEY (id);


--
-- Name: keywords_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY keywords
    ADD CONSTRAINT keywords_pkey PRIMARY KEY (id);


--
-- Name: languages_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY languages
    ADD CONSTRAINT languages_pkey PRIMARY KEY (id);


--
-- Name: layouts_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY layouts
    ADD CONSTRAINT layouts_pkey PRIMARY KEY (id);


--
-- Name: likes_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY likes
    ADD CONSTRAINT likes_pkey PRIMARY KEY (id);


--
-- Name: media_exports_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY media_exports
    ADD CONSTRAINT media_exports_pkey PRIMARY KEY (id);


--
-- Name: media_extracts_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY media_extracts
    ADD CONSTRAINT media_extracts_pkey PRIMARY KEY (id);


--
-- Name: media_index_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY media_index
    ADD CONSTRAINT media_index_pkey PRIMARY KEY (id);


--
-- Name: media_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- Name: media_translation_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY media_translation
    ADD CONSTRAINT media_translation_pkey PRIMARY KEY (id);


--
-- Name: menu_page_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY menu_page
    ADD CONSTRAINT menu_page_pkey PRIMARY KEY (id);


--
-- Name: menus_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY menus
    ADD CONSTRAINT menus_pkey PRIMARY KEY (id);


--
-- Name: pages_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: parameter_values_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY parameter_values
    ADD CONSTRAINT parameter_values_pkey PRIMARY KEY (id);


--
-- Name: parameters_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY parameters
    ADD CONSTRAINT parameters_pkey PRIMARY KEY (id);


--
-- Name: permission_categories_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY permission_categories
    ADD CONSTRAINT permission_categories_pkey PRIMARY KEY (id);


--
-- Name: permission_role_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY permission_role
    ADD CONSTRAINT permission_role_pkey PRIMARY KEY (id);


--
-- Name: permissions_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: profiles_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (id);


--
-- Name: properties_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY properties
    ADD CONSTRAINT properties_pkey PRIMARY KEY (id);


--
-- Name: related_links_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY related_links
    ADD CONSTRAINT related_links_pkey PRIMARY KEY (id);


--
-- Name: related_media_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY related_media
    ADD CONSTRAINT related_media_pkey PRIMARY KEY (id);


--
-- Name: related_pages_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY related_pages
    ADD CONSTRAINT related_pages_pkey PRIMARY KEY (id);


--
-- Name: role_user_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT role_user_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: search_index_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY search_index
    ADD CONSTRAINT search_index_pkey PRIMARY KEY (id);


--
-- Name: templates_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY templates
    ADD CONSTRAINT templates_pkey PRIMARY KEY (id);


--
-- Name: translations_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY translations
    ADD CONSTRAINT translations_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: variations_pkey; Type: CONSTRAINT; Schema: cms; Owner: -; Tablespace: 
--

ALTER TABLE ONLY variations
    ADD CONSTRAINT variations_pkey PRIMARY KEY (id);


SET search_path = custom, pg_catalog;

--
-- Name: employees_pkey; Type: CONSTRAINT; Schema: custom; Owner: -; Tablespace: 
--

ALTER TABLE ONLY employees
    ADD CONSTRAINT employees_pkey PRIMARY KEY (id);


--
-- Name: groupmembers_pkey; Type: CONSTRAINT; Schema: custom; Owner: -; Tablespace: 
--

ALTER TABLE ONLY groupmembers
    ADD CONSTRAINT groupmembers_pkey PRIMARY KEY (id);


SET search_path = public, pg_catalog;

--
-- Name: failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: laravel_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY laravel_migrations
    ADD CONSTRAINT laravel_migrations_pkey PRIMARY KEY (bundle, name);


--
-- Name: locks_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY locks
    ADD CONSTRAINT locks_pkey PRIMARY KEY (application_id);


--
-- Name: pk_cas_session; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY cas_sessions
    ADD CONSTRAINT pk_cas_session PRIMARY KEY (id);


--
-- Name: pk_person_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY person
    ADD CONSTRAINT pk_person_id PRIMARY KEY (id);


--
-- Name: pk_suite_properties; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY suite_properties
    ADD CONSTRAINT pk_suite_properties PRIMARY KEY (id);


--
-- Name: pk_users; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT pk_users PRIMARY KEY (id);


--
-- Name: serviceticket_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY serviceticket
    ADD CONSTRAINT serviceticket_pkey PRIMARY KEY (id);


--
-- Name: symfoni_scheduler_hash_unique; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY symfoni_scheduler
    ADD CONSTRAINT symfoni_scheduler_hash_unique UNIQUE (hash);


--
-- Name: symfoni_scheduler_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY symfoni_scheduler
    ADD CONSTRAINT symfoni_scheduler_pkey PRIMARY KEY (id);


--
-- Name: ticketgrantingticket_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ticketgrantingticket
    ADD CONSTRAINT ticketgrantingticket_pkey PRIMARY KEY (id);


--
-- Name: unq_person_old_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY person
    ADD CONSTRAINT unq_person_old_id UNIQUE (old_id);


--
-- Name: uq_suite_property; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY suite_properties
    ADD CONSTRAINT uq_suite_property UNIQUE (property_name);


--
-- Name: uq_user_login; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT uq_user_login UNIQUE (login);


SET search_path = cms, pg_catalog;

--
-- Name: cms_keyword_collections_lower_case_name_unique; Type: INDEX; Schema: cms; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX cms_keyword_collections_lower_case_name_unique ON keyword_collections USING btree (lower((name)::text));


--
-- Name: idx_media_index_media_id; Type: INDEX; Schema: cms; Owner: -; Tablespace: 
--

CREATE INDEX idx_media_index_media_id ON media_index USING btree (media_id);


--
-- Name: idx_media_index_phrase; Type: INDEX; Schema: cms; Owner: -; Tablespace: 
--

CREATE INDEX idx_media_index_phrase ON media_index USING gin (phrase);


--
-- Name: idx_permission_categories_keyword; Type: INDEX; Schema: cms; Owner: -; Tablespace: 
--

CREATE INDEX idx_permission_categories_keyword ON permission_categories USING btree (keyword);


--
-- Name: idx_permissions_keyword; Type: INDEX; Schema: cms; Owner: -; Tablespace: 
--

CREATE INDEX idx_permissions_keyword ON permissions USING btree (keyword);


--
-- Name: idx_search_index_phrase; Type: INDEX; Schema: cms; Owner: -; Tablespace: 
--

CREATE INDEX idx_search_index_phrase ON search_index USING gin (phrase);


--
-- Name: idx_search_index_translation_id; Type: INDEX; Schema: cms; Owner: -; Tablespace: 
--

CREATE INDEX idx_search_index_translation_id ON search_index USING btree (translation_id);


SET search_path = public, pg_catalog;

--
-- Name: fki_person_company_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_person_company_id ON person USING btree (company_id);


--
-- Name: fki_person_department_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_person_department_id ON person USING btree (department_id);


--
-- Name: fki_person_import; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_person_import ON person USING btree (import_id);


--
-- Name: fki_person_user; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_person_user ON person USING btree (owner_id);


--
-- Name: fki_persont_status_type_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_persont_status_type_id ON person USING btree (persont_status_type_id);


--
-- Name: fki_session_tgt; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_session_tgt ON cas_sessions USING btree (tgt);


--
-- Name: fki_user_person; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX fki_user_person ON users USING btree (id);


--
-- Name: idx_person_external; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_person_external ON person USING btree (is_external);


--
-- Name: idx_person_history; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_person_history ON person USING btree (is_history);


--
-- Name: idx_person_restricted; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_person_restricted ON person USING btree (is_restricted);


--
-- Name: idx_user_enabled; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_user_enabled ON users USING btree (is_enabled);


--
-- Name: idx_user_login; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX idx_user_login ON users USING btree (login);


SET search_path = cms, pg_catalog;

--
-- Name: cms_filter_page_trigger; Type: TRIGGER; Schema: cms; Owner: -
--

CREATE TRIGGER cms_filter_page_trigger BEFORE INSERT OR UPDATE ON pages FOR EACH ROW EXECUTE PROCEDURE f_filter_page_trigger();


--
-- Name: cms_filter_translation_trigger; Type: TRIGGER; Schema: cms; Owner: -
--

CREATE TRIGGER cms_filter_translation_trigger BEFORE INSERT OR UPDATE ON translations FOR EACH ROW EXECUTE PROCEDURE f_filter_translation_trigger();


--
-- Name: cms_property_renamed_trigger; Type: TRIGGER; Schema: cms; Owner: -
--

CREATE TRIGGER cms_property_renamed_trigger AFTER UPDATE ON properties FOR EACH ROW EXECUTE PROCEDURE f_property_renamed_trigger();


--
-- Name: cms_update_media_index_extracts_trigger; Type: TRIGGER; Schema: cms; Owner: -
--

CREATE TRIGGER cms_update_media_index_extracts_trigger AFTER INSERT OR UPDATE ON media_extracts FOR EACH ROW EXECUTE PROCEDURE f_update_media_index_extracts_trigger();


--
-- Name: cms_update_media_index_trigger; Type: TRIGGER; Schema: cms; Owner: -
--

CREATE TRIGGER cms_update_media_index_trigger AFTER INSERT OR UPDATE ON media FOR EACH ROW EXECUTE PROCEDURE f_update_media_index_trigger();


--
-- Name: cms_update_translation_index_trigger; Type: TRIGGER; Schema: cms; Owner: -
--

CREATE TRIGGER cms_update_translation_index_trigger AFTER INSERT OR UPDATE ON translations FOR EACH ROW EXECUTE PROCEDURE f_update_translation_index_trigger();


--
-- Name: tg_sites_to_cas_user_synchronization_trigger; Type: TRIGGER; Schema: cms; Owner: -
--

CREATE TRIGGER tg_sites_to_cas_user_synchronization_trigger AFTER INSERT OR DELETE OR UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE public.f_sites_to_cas_user_synchronization_trigger();


SET search_path = public, pg_catalog;

--
-- Name: tg_unlink_st; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER tg_unlink_st AFTER DELETE OR UPDATE ON serviceticket FOR EACH ROW EXECUTE PROCEDURE f_unlink_st();


--
-- Name: tg_unlink_tgt; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER tg_unlink_tgt AFTER DELETE OR UPDATE ON ticketgrantingticket FOR EACH ROW EXECUTE PROCEDURE f_unlink_tgt();


SET search_path = cms, pg_catalog;

--
-- Name: cms_channels_language_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT cms_channels_language_id_foreign FOREIGN KEY (language_id) REFERENCES languages(id);


--
-- Name: cms_channels_layout_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT cms_channels_layout_id_foreign FOREIGN KEY (layout_id) REFERENCES layouts(id);


--
-- Name: cms_channels_root_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT cms_channels_root_id_foreign FOREIGN KEY (root_id) REFERENCES pages(id);


--
-- Name: cms_channels_start_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT cms_channels_start_id_foreign FOREIGN KEY (start_id) REFERENCES pages(id);


--
-- Name: cms_comments_language_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT cms_comments_language_id_foreign FOREIGN KEY (language_id) REFERENCES languages(id) ON DELETE CASCADE;


--
-- Name: cms_comments_page_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT cms_comments_page_id_foreign FOREIGN KEY (page_id) REFERENCES pages(id) ON DELETE CASCADE;


--
-- Name: cms_comments_parent_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT cms_comments_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES comments(id) ON DELETE CASCADE;


--
-- Name: cms_comments_user_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT cms_comments_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: cms_configurations_default_language_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY configurations
    ADD CONSTRAINT cms_configurations_default_language_foreign FOREIGN KEY (default_language) REFERENCES languages(id);


--
-- Name: cms_configurations_default_layout_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY configurations
    ADD CONSTRAINT cms_configurations_default_layout_foreign FOREIGN KEY (default_layout) REFERENCES layouts(id);


--
-- Name: cms_configurations_start_page_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY configurations
    ADD CONSTRAINT cms_configurations_start_page_foreign FOREIGN KEY (start_page) REFERENCES pages(id);


--
-- Name: cms_data_attributes_definition_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY data_attributes
    ADD CONSTRAINT cms_data_attributes_definition_id_foreign FOREIGN KEY (definition_id) REFERENCES data_definitions(id) ON DELETE CASCADE;


--
-- Name: cms_data_definitions_author_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY data_definitions
    ADD CONSTRAINT cms_data_definitions_author_id_foreign FOREIGN KEY (author_id) REFERENCES users(id);


--
-- Name: cms_data_items_definition_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY data_items
    ADD CONSTRAINT cms_data_items_definition_id_foreign FOREIGN KEY (definition_id) REFERENCES data_definitions(id) ON DELETE CASCADE;


--
-- Name: cms_element_instances_element_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY element_instances
    ADD CONSTRAINT cms_element_instances_element_id_foreign FOREIGN KEY (element_id) REFERENCES elements(id) ON DELETE CASCADE;


--
-- Name: cms_element_instances_layout_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY element_instances
    ADD CONSTRAINT cms_element_instances_layout_id_foreign FOREIGN KEY (layout_id) REFERENCES layouts(id) ON DELETE CASCADE;


--
-- Name: cms_element_instances_template_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY element_instances
    ADD CONSTRAINT cms_element_instances_template_id_foreign FOREIGN KEY (template_id) REFERENCES templates(id) ON DELETE CASCADE;


--
-- Name: cms_elements_author_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY elements
    ADD CONSTRAINT cms_elements_author_id_foreign FOREIGN KEY (author_id) REFERENCES users(id);


--
-- Name: cms_folders_parent_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY folders
    ADD CONSTRAINT cms_folders_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES folders(id) ON DELETE CASCADE;


--
-- Name: cms_help_parent_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY help
    ADD CONSTRAINT cms_help_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES help(id) ON DELETE CASCADE;


--
-- Name: cms_keyword_page_keyword_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY keyword_page
    ADD CONSTRAINT cms_keyword_page_keyword_id_foreign FOREIGN KEY (keyword_id) REFERENCES keywords(id) ON DELETE CASCADE;


--
-- Name: cms_keyword_page_page_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY keyword_page
    ADD CONSTRAINT cms_keyword_page_page_id_foreign FOREIGN KEY (page_id) REFERENCES pages(id) ON DELETE CASCADE;


--
-- Name: cms_keywords_collection_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY keywords
    ADD CONSTRAINT cms_keywords_collection_id_foreign FOREIGN KEY (collection_id) REFERENCES keyword_collections(id) ON DELETE CASCADE;


--
-- Name: cms_layouts_author_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY layouts
    ADD CONSTRAINT cms_layouts_author_id_foreign FOREIGN KEY (author_id) REFERENCES users(id);


--
-- Name: cms_likes_comment_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY likes
    ADD CONSTRAINT cms_likes_comment_id_foreign FOREIGN KEY (comment_id) REFERENCES comments(id) ON DELETE CASCADE;


--
-- Name: cms_likes_language_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY likes
    ADD CONSTRAINT cms_likes_language_id_foreign FOREIGN KEY (language_id) REFERENCES languages(id) ON DELETE CASCADE;


--
-- Name: cms_likes_page_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY likes
    ADD CONSTRAINT cms_likes_page_id_foreign FOREIGN KEY (page_id) REFERENCES pages(id) ON DELETE CASCADE;


--
-- Name: cms_likes_user_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY likes
    ADD CONSTRAINT cms_likes_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: cms_media_author_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media
    ADD CONSTRAINT cms_media_author_id_foreign FOREIGN KEY (author_id) REFERENCES users(id);


--
-- Name: cms_media_exports_media_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media_exports
    ADD CONSTRAINT cms_media_exports_media_id_foreign FOREIGN KEY (media_id) REFERENCES media(id) ON DELETE CASCADE;


--
-- Name: cms_media_extracts_media_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media_extracts
    ADD CONSTRAINT cms_media_extracts_media_id_foreign FOREIGN KEY (media_id) REFERENCES media(id) ON DELETE CASCADE;


--
-- Name: cms_media_folder_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media
    ADD CONSTRAINT cms_media_folder_id_foreign FOREIGN KEY (folder_id) REFERENCES folders(id) ON DELETE CASCADE;


--
-- Name: cms_media_language_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media
    ADD CONSTRAINT cms_media_language_id_foreign FOREIGN KEY (language_id) REFERENCES languages(id) ON DELETE SET NULL;


--
-- Name: cms_media_media_id; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media_index
    ADD CONSTRAINT cms_media_media_id FOREIGN KEY (media_id) REFERENCES media(id) ON DELETE CASCADE;


--
-- Name: cms_media_translation_media_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media_translation
    ADD CONSTRAINT cms_media_translation_media_id_foreign FOREIGN KEY (media_id) REFERENCES media(id) ON DELETE CASCADE;


--
-- Name: cms_media_translation_translation_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY media_translation
    ADD CONSTRAINT cms_media_translation_translation_id_foreign FOREIGN KEY (translation_id) REFERENCES translations(id) ON DELETE CASCADE;


--
-- Name: cms_menu_page_menu_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY menu_page
    ADD CONSTRAINT cms_menu_page_menu_id_foreign FOREIGN KEY (menu_id) REFERENCES menus(id) ON DELETE CASCADE;


--
-- Name: cms_menu_page_page_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY menu_page
    ADD CONSTRAINT cms_menu_page_page_id_foreign FOREIGN KEY (page_id) REFERENCES pages(id) ON DELETE CASCADE;


--
-- Name: cms_menus_author_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY menus
    ADD CONSTRAINT cms_menus_author_id_foreign FOREIGN KEY (author_id) REFERENCES users(id);


--
-- Name: cms_pages_author_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY pages
    ADD CONSTRAINT cms_pages_author_id_foreign FOREIGN KEY (author_id) REFERENCES users(id);


--
-- Name: cms_pages_parent_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY pages
    ADD CONSTRAINT cms_pages_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES pages(id);


--
-- Name: cms_pages_template_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY pages
    ADD CONSTRAINT cms_pages_template_id_foreign FOREIGN KEY (template_id) REFERENCES templates(id);


--
-- Name: cms_parameter_values_element_instance_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY parameter_values
    ADD CONSTRAINT cms_parameter_values_element_instance_id_foreign FOREIGN KEY (element_instance_id) REFERENCES element_instances(id) ON DELETE CASCADE;


--
-- Name: cms_parameter_values_parameter_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY parameter_values
    ADD CONSTRAINT cms_parameter_values_parameter_id_foreign FOREIGN KEY (parameter_id) REFERENCES parameters(id) ON DELETE CASCADE;


--
-- Name: cms_parameters_element_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY parameters
    ADD CONSTRAINT cms_parameters_element_id_foreign FOREIGN KEY (element_id) REFERENCES elements(id) ON DELETE CASCADE;


--
-- Name: cms_permission_role_permission_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY permission_role
    ADD CONSTRAINT cms_permission_role_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES permissions(id) ON DELETE CASCADE;


--
-- Name: cms_permission_role_role_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY permission_role
    ADD CONSTRAINT cms_permission_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;


--
-- Name: cms_permissions_category_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY permissions
    ADD CONSTRAINT cms_permissions_category_id_foreign FOREIGN KEY (category_id) REFERENCES permission_categories(id) ON DELETE CASCADE;


--
-- Name: cms_profiles_user_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY profiles
    ADD CONSTRAINT cms_profiles_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: cms_properties_template_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY properties
    ADD CONSTRAINT cms_properties_template_id_foreign FOREIGN KEY (template_id) REFERENCES templates(id) ON DELETE CASCADE;


--
-- Name: cms_related_links_page_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY related_links
    ADD CONSTRAINT cms_related_links_page_id_foreign FOREIGN KEY (page_id) REFERENCES pages(id) ON DELETE CASCADE;


--
-- Name: cms_related_media_media_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY related_media
    ADD CONSTRAINT cms_related_media_media_id_foreign FOREIGN KEY (media_id) REFERENCES media(id) ON DELETE CASCADE;


--
-- Name: cms_related_media_page_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY related_media
    ADD CONSTRAINT cms_related_media_page_id_foreign FOREIGN KEY (page_id) REFERENCES pages(id) ON DELETE CASCADE;


--
-- Name: cms_related_pages_page_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY related_pages
    ADD CONSTRAINT cms_related_pages_page_id_foreign FOREIGN KEY (page_id) REFERENCES pages(id) ON DELETE CASCADE;


--
-- Name: cms_related_pages_related_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY related_pages
    ADD CONSTRAINT cms_related_pages_related_id_foreign FOREIGN KEY (related_id) REFERENCES pages(id) ON DELETE CASCADE;


--
-- Name: cms_role_user_role_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT cms_role_user_role_id_foreign FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE;


--
-- Name: cms_role_user_user_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT cms_role_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: cms_templates_author_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY templates
    ADD CONSTRAINT cms_templates_author_id_foreign FOREIGN KEY (author_id) REFERENCES users(id);


--
-- Name: cms_translations_language_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY translations
    ADD CONSTRAINT cms_translations_language_id_foreign FOREIGN KEY (language_id) REFERENCES languages(id) ON DELETE CASCADE;


--
-- Name: cms_translations_page_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY translations
    ADD CONSTRAINT cms_translations_page_id_foreign FOREIGN KEY (page_id) REFERENCES pages(id) ON DELETE CASCADE;


--
-- Name: cms_translations_translations_id; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY search_index
    ADD CONSTRAINT cms_translations_translations_id FOREIGN KEY (translation_id) REFERENCES translations(id) ON DELETE CASCADE;


--
-- Name: cms_variations_media_id_foreign; Type: FK CONSTRAINT; Schema: cms; Owner: -
--

ALTER TABLE ONLY variations
    ADD CONSTRAINT cms_variations_media_id_foreign FOREIGN KEY (media_id) REFERENCES media(id) ON DELETE CASCADE;


SET search_path = custom, pg_catalog;

--
-- Name: custom_employees_user_id_foreign; Type: FK CONSTRAINT; Schema: custom; Owner: -
--

ALTER TABLE ONLY employees
    ADD CONSTRAINT custom_employees_user_id_foreign FOREIGN KEY (user_id) REFERENCES cms.users(id) ON DELETE CASCADE;


--
-- Name: custom_groupmembers_page_id_foreign; Type: FK CONSTRAINT; Schema: custom; Owner: -
--

ALTER TABLE ONLY groupmembers
    ADD CONSTRAINT custom_groupmembers_page_id_foreign FOREIGN KEY (page_id) REFERENCES cms.pages(id) ON DELETE CASCADE;


--
-- Name: custom_groupmembers_user_id_foreign; Type: FK CONSTRAINT; Schema: custom; Owner: -
--

ALTER TABLE ONLY groupmembers
    ADD CONSTRAINT custom_groupmembers_user_id_foreign FOREIGN KEY (user_id) REFERENCES cms.users(id) ON DELETE CASCADE;


SET search_path = public, pg_catalog;

--
-- Name: fk_person_user; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY person
    ADD CONSTRAINT fk_person_user FOREIGN KEY (owner_id) REFERENCES users(id);


--
-- Name: fk_session_tgt; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cas_sessions
    ADD CONSTRAINT fk_session_tgt FOREIGN KEY (tgt) REFERENCES ticketgrantingticket(id);


--
-- Name: fk_st_tgt; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY serviceticket
    ADD CONSTRAINT fk_st_tgt FOREIGN KEY (ticketgrantingticket_id) REFERENCES ticketgrantingticket(id);


--
-- Name: fk_tgt_tgt; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ticketgrantingticket
    ADD CONSTRAINT fk_tgt_tgt FOREIGN KEY (ticketgrantingticket_id) REFERENCES ticketgrantingticket(id);


--
-- Name: fk_user_person; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fk_user_person FOREIGN KEY (id) REFERENCES person(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM devngi;
GRANT ALL ON SCHEMA public TO devngi;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

